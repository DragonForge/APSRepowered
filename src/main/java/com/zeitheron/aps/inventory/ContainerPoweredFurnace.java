package com.zeitheron.aps.inventory;

import com.zeitheron.aps.blocks.tiles.TilePoweredFurnace;
import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncSyncableTile;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;

public class ContainerPoweredFurnace extends TransferableContainer<TilePoweredFurnace>
{
	public ContainerPoweredFurnace(EntityPlayer player, TilePoweredFurnace t)
	{
		super(player, t, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		addSlotToContainer(new Slot(t, 0, 52, 35));
		addSlotToContainer(new Slot(t, 1, 96, 35));
	}
	
	@Override
	protected void addTransfer()
	{
		// Shift-click
		transfer.addInTransferRule(0, stack -> t.isItemValidForSlot(0, stack));
		transfer.addOutTransferRule(0, i -> i > 1);
		transfer.addOutTransferRule(1, i -> i > 1);
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		if(player instanceof EntityPlayerMP)
			HCNet.INSTANCE.sendTo(new PacketSyncSyncableTile(t), (EntityPlayerMP) player);
	}
}