package com.zeitheron.aps.inventory;

import com.zeitheron.aps.blocks.tiles.TileFuelGenerator;
import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncSyncableTile;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.SlotFurnaceFuel;

public class ContainerFuelGenerator extends TransferableContainer<TileFuelGenerator>
{
	public ContainerFuelGenerator(EntityPlayer player, TileFuelGenerator tile)
	{
		super(player, tile, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		addSlotToContainer(new SlotFurnaceFuel(t, 0, 80, 40));
	}
	
	@Override
	protected void addTransfer()
	{
		// Shift-click
		transfer.addInTransferRule(0, stack -> t.isItemValidForSlot(0, stack));
		transfer.addOutTransferRule(0, i -> true);
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		if(player instanceof EntityPlayerMP)
			HCNet.INSTANCE.sendTo(new PacketSyncSyncableTile(t), (EntityPlayerMP) player);
	}
}