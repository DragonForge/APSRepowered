package com.zeitheron.aps.inventory;

import com.zeitheron.aps.blocks.tiles.TileLaserTable;
import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncSyncableTile;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceOutput;

public class ContainerLaserTable extends TransferableContainer<TileLaserTable>
{
	public ContainerLaserTable(EntityPlayer player, TileLaserTable t)
	{
		super(player, t, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		addSlotToContainer(new SlotFurnaceOutput(player, t.output, 0, 123, 34));
		for(int i = 0; i < 9; ++i)
			addSlotToContainer(new Slot(t.input, i, 30 + (i % 3) * 18, 17 + (i / 3) * 18));
	}
	
	@Override
	protected void addTransfer()
	{
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		if(player instanceof EntityPlayerMP)
			HCNet.INSTANCE.sendTo(new PacketSyncSyncableTile(t), (EntityPlayerMP) player);
	}
}