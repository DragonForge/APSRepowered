package com.zeitheron.aps.cfg;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyInt;

import net.minecraftforge.fluids.Fluid;

@HCModConfigurations(modid = InfoAR.MOD_ID, module = "machines")
public class MachineConfig implements IConfigReloadListener
{
	@ModConfigPropertyBool(name = "PoweredFurnace", category = "Machines", comment = "Should Powered Furnace be added to the game?", defaultValue = true)
	public static boolean FurnaceEnabled;
	
	@ModConfigPropertyBool(name = "Grinder", category = "Machines", comment = "Should Grinder be added to the game?", defaultValue = true)
	public static boolean GrinderEnabled;
	
	@ModConfigPropertyBool(name = "Pump", category = "Machines", comment = "Should Fluid Pump be added to the game?", defaultValue = true)
	public static boolean PumpEnabled;
	
	@ModConfigPropertyBool(name = "Magmafier", category = "Machines", comment = "Should Blast Furnace be added to the game?", defaultValue = true)
	public static boolean MagmafierEnabled;
	
	@ModConfigPropertyInt(name = "MagmaticGeneratorFluidConsumption", category = "Machines", comment = "How much mB of hot liquid will get consumed to start the generator?", defaultValue = 200, min = 1, max = 10_000)
	public static int MagmaticGeneratorFluidConsumption;
	
	@ModConfigPropertyInt(name = "MatterLimit", category = "Magmafier", comment = "How much matter can blast furnace hold in total?", defaultValue = 200, min = 1, max = Integer.MAX_VALUE)
	public static int MagmafierMatterLimit;
	
	@ModConfigPropertyInt(name = "EnergyPerOperation", category = "Magmafier", comment = "How much RF/FE will be consumed for single conversion?", defaultValue = 2000, min = 1, max = Integer.MAX_VALUE)
	public static int MagmafierEnergyPerOperation;
}