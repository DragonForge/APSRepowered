package com.zeitheron.aps.cfg;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyFloat;

@HCModConfigurations(modid = InfoAR.MOD_ID, module = "solar")
public class SolarConfig implements IConfigReloadListener
{
	@ModConfigPropertyFloat(name = "BaseReflectorGen", category = "Solar", comment = "How much RF/tick would", defaultValue = 8F, min = 0, max = 1000)
	public static float SolarBaseGen;
	
	@ModConfigPropertyBool(name = "Enabled", category = "Solar", comment = "Should Solar Reflectors and Collectors be added to the game?", defaultValue = true)
	public static boolean SolarEnabled;
}