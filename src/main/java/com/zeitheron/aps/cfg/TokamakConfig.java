package com.zeitheron.aps.cfg;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyInt;

import net.minecraftforge.common.config.Configuration;

@HCModConfigurations(modid = InfoAR.MOD_ID, module = "tokamak")
public class TokamakConfig implements IConfigReloadListener
{
	@ModConfigPropertyInt(name = "MaxTemperature", category = "Tokamak", comment = "What is the maximal temperature of TOKAMAK?", defaultValue = 10_000_000, min = 100_000, max = 1_000_000_000)
	public static int TokamakMaxTemp = 10_000_000;
	
	@ModConfigPropertyInt(name = "CoolRate", category = "Tokamak", comment = "What is the cool rate of TOKAMAK?", defaultValue = 1_000, min = 1, max = 1_000_000_000)
	public static int TokamakCoolRate = 1_000;
	
	@ModConfigPropertyInt(name = "MaxEnergyReceive", category = "Tokamak", comment = "What is the accept limit for TOKAMAK?", defaultValue = 4_000, min = 1, max = 1_000_000_000)
	public static int TokamakMaxEnergyReceive = 4_000;
	
	@ModConfigPropertyInt(name = "EnergyToHeatingScalar", category = "Tokamak", comment = "", defaultValue = 5, min = 1, max = 1_000)
	public static int TokamakEnergyToHeatingScalar = 5;
	
	@ModConfigPropertyInt(name = "WaterUsePercent", category = "Tokamak", comment = "How much water is going to be used? (10% = 100 mB)", defaultValue = 10, min = 1, max = 200)
	public static int TokamakWaterUse = 10;
	
	@ModConfigPropertyInt(name = "TokamakHeatGenLimit", category = "Tokamak", comment = "How much RF/tick will TOKAMAK need to heat up?", defaultValue = 2_000, min = 1, max = 1_000_000_000)
	public static int TokamakHeatGenLimit = 2_000;
	
	@ModConfigPropertyInt(name = "TokamakGenLimit", category = "Tokamak", comment = "How much RF/tick will TOKAMAK generate at full power?", defaultValue = 20_000, min = 1, max = 1_000_000_000)
	public static int TokamakGenLimit = 20_000;
	
	@ModConfigPropertyBool(name = "Enabled", category = "Tokamak", comment = "Should TOKAMAK be added to the game?", defaultValue = true)
	public static boolean TokamakEnabled;
	
	public static float TokamakFusionFraction;
	
	@Override
	public void reloadCustom(Configuration cfgs)
	{
		TokamakFusionFraction = cfgs.getInt("TokamakFusionFraction", "Tokamak", 80, 0, 100, "TOKAMAK Fusion Temperature Percentage") / 100F;
	}
}