package com.zeitheron.aps.intr.ic2;

import com.zeitheron.aps.items.ItemBaseAR;

public class ItemsARIC2
{
	public static final ItemBaseAR //
	IRIDIUM_CHIP = new ItemBaseAR("ic2_iridium_chip"), //
	        IRDIUM_POWER_CORE = new ItemBaseAR("ic2_iridium_power_core");
}