package com.zeitheron.aps.intr.ic2.inventory;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileICEngine;
import com.zeitheron.hammercore.client.gui.impl.container.ItemTransferHelper.TransferableContainer;

import ic2.api.item.ElectricItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerICEngine extends TransferableContainer<TileICEngine>
{
	public ContainerICEngine(EntityPlayer player, TileICEngine tile)
	{
		super(player, tile, 8, 84);
	}
	
	@Override
	protected void addCustomSlots()
	{
		addSlotToContainer(new Slot(t.items, 0, 80, 40)
		{
			@Override
			public boolean isItemValid(ItemStack stack)
			{
				return ElectricItem.manager.getCharge(stack) > 0;
			}
		});
	}
	
	@Override
	protected void addTransfer()
	{
		// Shift-click
		transfer.addInTransferRule(0, stack -> t.items.isItemValidForSlot(0, stack));
		transfer.addOutTransferRule(0, i -> true);
	}
}