package com.zeitheron.aps.intr.ic2.client.gui;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.ic2.blocks.tiles.TileAJEngine;
import com.zeitheron.aps.intr.ic2.blocks.tiles.TileICGenerator;
import com.zeitheron.aps.intr.ic2.inventory.ContainerAJEngine;
import com.zeitheron.aps.intr.ic2.net.PacketAlterAdjSpeed;
import com.zeitheron.aps.intr.ic2.net.PacketAlterAdjSpeed.EnumMathOperator;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiAJEngine
		extends GuiContainer
{
	int mouseX, mouseY;
	public TileAJEngine tile;

	public GuiAJEngine(EntityPlayer player, TileAJEngine tile)
	{
		super(new ContainerAJEngine(player, tile));
		this.tile = tile;

		xSize = 205;
		ySize = 204;
	}

	DynGuiTex tex;

	@Override
	public void initGui()
	{
		super.initGui();

		addButton(new GuiButton(305, guiLeft + 5, guiTop + 78, 32, 20, "-100"));
		addButton(new GuiButton(303, guiLeft + 41, guiTop + 78, 26, 20, "-10"));
		addButton(new GuiButton(301, guiLeft + 71, guiTop + 78, 20, 20, "-1"));
		addButton(new GuiButton(300, guiLeft + 113, guiTop + 78, 20, 20, "+1"));
		addButton(new GuiButton(302, guiLeft + 138, guiTop + 78, 26, 20, "+10"));
		addButton(new GuiButton(304, guiLeft + 168, guiTop + 78, 32, 20, "+100"));

		GuiTexBakery bak = GuiTexBakery.start();
		bak.body(0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			bak.slot(s.xPos - 1, s.yPos - 1);
		bak.slot(8, 121, 10, 76);
		tex = bak.bake();
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		int id = button.id - 300;

		if(id == 0)
			HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(tile, EnumMathOperator.ADD, 1 / 4D));
		if(id == 1)
			HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(tile, EnumMathOperator.SUBTRACT, 1 / 4D));

		if(id == 2)
			HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(tile, EnumMathOperator.ADD, 10 / 4D));
		if(id == 3)
			HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(tile, EnumMathOperator.SUBTRACT, 10 / 4D));

		if(id == 4)
			HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(tile, EnumMathOperator.ADD, 100 / 4D));
		if(id == 5)
			HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(tile, EnumMathOperator.SUBTRACT, 100 / 4D));
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;

		{
			IBlockState state = tile.getWorld().getBlockState(tile.getPos());
			String ln = I18n.format(state.getBlock().getTranslationKey() + ".name");
			fontRenderer.drawString(ln, (xSize - fontRenderer.getStringWidth(ln)) / 2, 6, shade);
		}

		String convert = I18n.format("gui." + InfoAR.MOD_ID + ".ic2_aj_engine_conv", TileICGenerator.df.format(tile.speed), (int) Math.floor(tile.euPerTick * 4D));

		int w = fontRenderer.getStringWidth(convert);
		float sf = w > 186 ? 186F / w : 1F;
		float nw = w * sf;

		GL11.glPushMatrix();
		GL11.glTranslated((xSize - nw) / 2 + 1, 31 + (16 - fontRenderer.FONT_HEIGHT * sf) / 2, 0);
		GL11.glScalef(sf, sf, 1);
		fontRenderer.drawString(convert, 0, 0, text);
		GL11.glPopMatrix();

		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 22, (ySize - 96) + 2, shade);

		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltips

		ItemStack mouse = HCNet.getMouseStack(mc.player);

		GlStateManager.enableBlend();

		if(mouseX > guiLeft + 8 && mouseY > guiTop + 121 && mouseX < guiLeft + 18 && mouseY < guiTop + 197)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 122, 8, 74, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				List<String> tip = TooltipHelper.generateEnergy(tile);
				tip.add(TextFormatting.DARK_GRAY + "Max. " + I18n.format("gui.hammercore.generation") + ": " + String.format("%,d", (int) (tile.euPerTick * 4)) + " " + I18n.format("gui.hammercore.fept"));
				drawHoveringText(tip, mouseX, mouseY);
			}
		} else if(mouseX > guiLeft + 97 && mouseY > guiTop + 62 && mouseX < guiLeft + 107 && mouseY < guiTop + 78)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 97, guiTop + 62, 9, 15, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				List<String> tip = TooltipHelper.emptyTooltipList();
				tip.add(String.format("%,d", Math.round(tile.sink.getEnergyStored())) + " / " + String.format("%,d", Math.round(tile.sink.getCapacity())) + " EU");
				drawHoveringText(tip, mouseX, mouseY);
			}
		}

		GL11.glPopMatrix();
	}

	@Override
	public void updateScreen()
	{
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TileAJEngine)
			this.tile = (TileAJEngine) tile;
		super.updateScreen();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mouseX = mouseX;
		this.mouseY = mouseY;
		tex.render(guiLeft, guiTop);
		GL11.glColor4f(1, 1, 1, 1);

		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/gui/widgets.png");

		RenderUtil.drawTexturedModalRect(guiLeft + 8, guiTop + 29, 0, 77, 190, 20);

		int col = GuiTheme.current().bodyLayerLU;
		GL11.glColor4f(ColorHelper.getRed(col), ColorHelper.getGreen(col), ColorHelper.getBlue(col), 1);
		RenderUtil.drawTexturedModalRect(guiLeft + 98, guiTop + 63, 0, 64, 7, 13);

		if(tile.sink.getEnergyStored() > 0)
		{
			double k = Math.min(1, tile.sink.getEnergyStored() / tile.sink.getCapacity()) * 13D;

			GL11.glColor4f(1, 1, 1, 1);
			RenderUtil.drawTexturedModalRect(guiLeft + 98, guiTop + 63 + 13 - k, 7, 64 + 13 - k, 7, k);
		}

		float energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 74F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 122 + 74 - energy, 8, energy, EnumPowerAnimation.UP);
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();

			// Show uses/recipes
			if(mouseX > guiLeft + 8 && mouseY > guiTop + 121 && mouseX < guiLeft + 18 && mouseY < guiTop + 197)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}

		super.keyTyped(typedChar, keyCode);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		GL11.glColor4f(1, 1, 1, 1);
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
}