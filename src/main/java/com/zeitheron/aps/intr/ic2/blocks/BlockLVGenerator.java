package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileICGenerator;
import com.zeitheron.aps.intr.ic2.blocks.tiles.TileLVGenerator;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockLVGenerator extends BlockBaseGenerator<TileLVGenerator>
{
	public BlockLVGenerator()
	{
		super(Material.IRON, TileLVGenerator.class, "ic2_pneumatic_gen_lv", 1);
		setSoundType(SoundType.METAL);
	}
}