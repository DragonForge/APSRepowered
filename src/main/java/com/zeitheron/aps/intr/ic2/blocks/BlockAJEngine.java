package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileAJEngine;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockAJEngine extends BlockBaseEngine<TileAJEngine>
{
	public BlockAJEngine()
	{
		super(Material.IRON, TileAJEngine.class, "ic2_aj_engine", 16);
		setSoundType(SoundType.METAL);
	}
}