package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileMVGenerator;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockMVGenerator extends BlockBaseGenerator<TileMVGenerator>
{
	public BlockMVGenerator()
	{
		super(Material.IRON, TileMVGenerator.class, "ic2_pneumatic_gen_mv", 2);
		setSoundType(SoundType.METAL);
	}
}