package com.zeitheron.aps.intr.ic2.blocks.tiles;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.ic2.client.gui.GuiAJEngine;
import com.zeitheron.aps.intr.ic2.inventory.ContainerAJEngine;
import com.zeitheron.aps.intr.ic2.net.PacketAlterAdjSpeed;
import com.zeitheron.aps.intr.ic2.net.PacketAlterAdjSpeed.EnumMathOperator;
import com.zeitheron.hammercore.net.HCNet;

import ic2.api.energy.EnergyNet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;

public class TileAJEngine extends TileICEngine
{
	public static final double DEF_SPEED = 8;
	
	public double speed;
	
	{
		baseTexture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_eadj.png");
		setSinkTier(16);
		setSpeed(DEF_SPEED, false);
	}
	
	public void setSpeed(double speed, boolean sync)
	{
		speed = Math.max(0, speed);
		this.speed = speed;
		
		this.maxChargeEU = Math.max(1000, speed * 1200);
		this.pistonSpeed = (float) Math.min(speed / 100F, 2);
		this.euPerTick = speed;
		sink.setCapacity(maxChargeEU);
		
		if(sync)
		{
			if(!world.isRemote)
				sendChangesToNearby();
			else
				HCNet.INSTANCE.sendToServer(new PacketAlterAdjSpeed(this, EnumMathOperator.SET, speed));
		}
	}
	
	@Override
	public void setSinkTier(int sinkTier)
	{
		this.maxVoltage = 4096 * 64;
		this.sinkTier = sinkTier;
		if(sink != null)
			sink.setSinkTier(sinkTier);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("Speed", speed);
		super.writeNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		setSpeed(nbt.getDouble("Speed"), false);
		super.readNBT(nbt);
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiAJEngine(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerAJEngine(player, this);
	}
}