package com.zeitheron.aps.intr.ic2.client;

import org.lwjgl.opengl.GL11;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.hammercore.client.model.ModelSimple;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.hammercore.utils.color.Rainbow;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

public enum EngineRender
{
	instance;
	
	private final ModelBase model = new ModelBase()
	{
	};
	
	private final ModelRenderer box;
	private final ModelRenderer trunk;
	private final ModelRenderer movingBox;
	private final ModelRenderer chamber;
	
	private EngineRender()
	{
		this.box = new ModelRenderer(model, 0, 0);
		this.box.addBox(-8.0f, -8.0f, -8.0f, 16, 4, 16);
		this.box.rotationPointX = 8;
		this.box.rotationPointY = 8;
		this.box.rotationPointZ = 8;
		
		this.trunk = new ModelRenderer(model, 0, 0);
		this.trunk.addBox(-4.0f, -4.0f, -4.0f, 8, 12, 8);
		this.trunk.rotationPointX = 8;
		this.trunk.rotationPointY = 8;
		this.trunk.rotationPointZ = 8;
		
		this.movingBox = new ModelRenderer(model, 0, 0);
		this.movingBox.addBox(-8.0f, -4.0f, -8.0f, 16, 4, 16);
		this.movingBox.rotationPointX = 8;
		this.movingBox.rotationPointY = 8;
		this.movingBox.rotationPointZ = 8;
		
		this.chamber = new ModelRenderer(model, 0, 0);
		this.chamber.addBox(-5.0f, -4.0f, -5.0f, 10, 2, 10);
		this.chamber.rotationPointX = 8;
		this.chamber.rotationPointY = 8;
		this.chamber.rotationPointZ = 8;
	}
	
	public void render(float progress, EnumFacing orientation, ResourceLocation baseTexture, double x, double y, double z)
	{
		GL11.glPushMatrix();
		GL11.glDisable(2896);
		GL11.glTranslated(x, y, z);
		
		float step = progress > .5F ? 7.99F - (progress - .5F) * 2F * 7.99F : progress * 2F * 7.99F;
		float[] angle = new float[] { 0, 0, 0 };
		float[] translate = new float[] { 0, 0, 0 };
		float translatefact = step / 16F + .001F;
		
		switch(orientation)
		{
		case EAST:
		{
			angle[2] = -1.5707964f;
			translate[0] = 1.0f;
			break;
		}
		case WEST:
		{
			angle[2] = 1.5707964f;
			translate[0] = -1.0f;
			break;
		}
		case UP:
		{
			translate[1] = 1.0f;
			break;
		}
		case DOWN:
		{
			angle[2] = 3.1415927f;
			translate[1] = -1.0f;
			break;
		}
		case SOUTH:
		{
			angle[0] = 1.5707964f;
			translate[2] = 1.0f;
			break;
		}
		case NORTH:
		{
			angle[0] = -1.5707964f;
			translate[2] = -1.0f;
		}
		}
		
		this.box.rotateAngleX = angle[0];
		this.box.rotateAngleY = angle[1];
		this.box.rotateAngleZ = angle[2];
		
		this.trunk.rotateAngleX = angle[0];
		this.trunk.rotateAngleY = angle[1];
		this.trunk.rotateAngleZ = angle[2];
		
		this.movingBox.rotateAngleX = angle[0];
		this.movingBox.rotateAngleY = angle[1];
		this.movingBox.rotateAngleZ = angle[2];
		
		this.chamber.rotateAngleX = angle[0];
		this.chamber.rotateAngleY = angle[1];
		this.chamber.rotateAngleZ = angle[2];
		
		float factor = 0.0625f;
		
		UtilsFX.bindTexture(baseTexture);
		this.box.render(factor);
		
		GL11.glTranslatef(translate[0] * translatefact, translate[1] * translatefact, translate[2] * translatefact);
		this.movingBox.render(factor);
		
		GL11.glTranslatef(-translate[0] * translatefact, -translate[1] * translatefact, -translate[2] * translatefact);
		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/models/engines/engine_chamber.png");
		
		float chamberf = 0.125f;
		
		int i = 0;
		while((float) i <= step + 2.0f)
		{
			this.chamber.render(factor);
			GL11.glTranslatef(translate[0] * chamberf, translate[1] * chamberf, translate[2] * chamberf);
			i += 2;
		}
		
		i = 0;
		while((float) i <= step + 2.0f)
		{
			GL11.glTranslatef(-translate[0] * chamberf, -translate[1] * chamberf, -translate[2] * chamberf);
			i += 2;
		}
		
		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/models/engines/engine_trunk.png");
		int pr = 205 + (int) ((1 - step / 8F) * 50F);
		ColorHelper.gl(255 << 24 | 255 << 16 | pr << 8 | pr);
		this.trunk.render(factor);
		
		GL11.glEnable(2896);
		GL11.glPopMatrix();
	}
}