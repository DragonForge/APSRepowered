package com.zeitheron.aps.intr.ic2.blocks.tiles;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.ic2.client.gui.GuiICEngine;
import com.zeitheron.aps.intr.ic2.inventory.ContainerICEngine;
import com.zeitheron.aps.util.MFEStorage;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.FrictionRotator;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import ic2.api.energy.EnergyNet;
import ic2.api.energy.prefab.BasicSink;
import ic2.api.item.ElectricItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TileICEngine extends TileSyncableTickable implements IEnergyStorage
{
	public int getFECap()
	{
		return 20_000;
	}
	
	public final MFEStorage energy = new MFEStorage(getFECap());
	private boolean setup = false;
	
	public double prevEU;
	
	public ResourceLocation baseTexture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_eadj.png");
	
	public double maxChargeEU = 0;
	public double maxVoltage;
	public int sinkTier;
	
	public boolean pistonActive = false;
	
	public double euPerTick = 8;
	public float pistonSpeed = .4F;
	
	public BasicSink sink = createSink();
	
	public InventoryDummy items = new InventoryDummy(1);
	{
		items.inventoryStackLimit = 1;
		items.validSlots = (index, stack) -> ElectricItem.manager.getTier(stack) <= sinkTier && ElectricItem.manager.getCharge(stack) > 0;
	}
	
	public EnumFacing getOrientation()
	{
		return WorldUtil.getFacing(world.getBlockState(pos));
	}
	
	public BasicSink createSink()
	{
		return new BasicSink(this, maxChargeEU, sinkTier)
		{
			@Override
			public double injectEnergy(EnumFacing directionFrom, double amount, double voltage)
			{
				sendChangesToNearby();
				return super.injectEnergy(directionFrom, amount, voltage);
			}
		};
	}
	
	public void setSinkTier(int sinkTier)
	{
		double euMax = EnergyNet.instance.getPowerFromTier(sinkTier);
		this.maxVoltage = euMax;
		this.euPerTick = euMax / 4;
		this.sinkTier = sinkTier;
		
		if(sink != null)
		{
			sink.setSinkTier(sinkTier);
			sink.setCapacity(maxChargeEU);
		}
	}
	
	public int getFECapacity()
	{
		return (int) (maxChargeEU * 4);
	}
	
	public FrictionRotator rotator = new FrictionRotator();
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	// TODO: Implement GUI stuff
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiICEngine(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerICEngine(player, this);
	}
	
	public void doSetup()
	{
		sink.onLoad();
		setup = true;
	}
	
	public boolean canWork()
	{
		return world.getRedstonePowerFromNeighbors(pos) > 0;
	}
	
	@Override
	public void tick()
	{
		if(!setup)
			doSetup();
		
		sink.setSinkTier(sinkTier);
		sink.setCapacity(maxChargeEU);
		
		if(!world.isRemote && atTickRate(2) && !items.getStackInSlot(0).isEmpty())
		{
			double itemCharge = Math.min(ElectricItem.manager.getCharge(items.getStackInSlot(0)), maxVoltage);
			if(itemCharge > 0)
			{
				sink.addEnergy(ElectricItem.manager.discharge(items.getStackInSlot(0), sink.getDemandedEnergy(), sinkTier, false, true, false));
				sendChangesToNearby();
			}
		}
		
		sink.update();
		
		rotator.update();
		
		if(pistonActive)
			rotator.speedup(pistonSpeed);
		
		if(!canWork())
		{
			rotator.friction = .5F;
			pistonActive = false;
			if(pistonActive)
				sendChangesToNearby();
			return;
		} else
			rotator.friction = .25F;
		
		doConversionLoop();
		
		EnumFacing face = getOrientation();
		TileEntity receptor = world.getTileEntity(pos.offset(face));
		if(receptor != null)
		{
			IEnergyStorage storage = receptor.hasCapability(CapabilityEnergy.ENERGY, face.getOpposite()) ? receptor.getCapability(CapabilityEnergy.ENERGY, face.getOpposite()) : null;
			
			if(storage != null && storage.canReceive())
			{
				int io = extractEnergy(storage.receiveEnergy(getEnergyStored(), false), false);
				if(io > 0)
					sendChangesToNearby();
			}
		}
		
		if(Math.abs(prevEU - sink.getEnergyStored()) > 1.0E-4)
		{
			sendChangesToNearby();
			prevEU = sink.getEnergyStored();
		}
	}
	
	public void doConversionLoop()
	{
		double eua = Math.min(euPerTick, sink.getEnergyStored());
		if(!world.isRemote && eua > 1E-4D && sink.useEnergy(eua))
		{
			energy.receiveEnergy((int) Math.floor(eua * 4), false);
			pistonActive = true;
			sendChangesToNearby();
		} else if(!world.isRemote && pistonActive)
		{
			pistonActive = false;
			sendChangesToNearby();
		}
	}
	
	@Override
	public void invalidate()
	{
		sink.invalidate();
		super.invalidate();
	}
	
	@Override
	public void onChunkUnload()
	{
		sink.onChunkUnload();
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return facing == getOrientation();
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		sink.writeToNBT(nbt);
		nbt.setTag("FE", energy.writeToNBT(new NBTTagCompound()));
		nbt.setFloat("PDegree", rotator.degree);
		nbt.setFloat("PSpeed", rotator.speed);
		nbt.setFloat("PCSpeed", rotator.currentSpeed);
		nbt.setBoolean("PistonActive", pistonActive);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		sink.readFromNBT(nbt);
		energy.readFromNBT(nbt.getCompoundTag("FE"));
		rotator.degree = nbt.getFloat("PDegree");
		rotator.speed = nbt.getFloat("PSpeed");
		rotator.currentSpeed = nbt.getFloat("PCSpeed");
		pistonActive = nbt.getBoolean("PistonActive");
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		int e = energy.extractEnergy(maxExtract, simulate);
		if(e > 0)
			sendChangesToNearby();
		return e;
	}
	
	@Override
	public int getEnergyStored()
	{
		return energy.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return energy.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return true;
	}
	
	@Override
	public boolean canReceive()
	{
		return false;
	}
}