package com.zeitheron.aps.intr.ic2;

import com.zeitheron.aps.intr.ic2.blocks.BlockAJEngine;
import com.zeitheron.aps.intr.ic2.blocks.BlockEVEngine;
import com.zeitheron.aps.intr.ic2.blocks.BlockEVGenerator;
import com.zeitheron.aps.intr.ic2.blocks.BlockHVEngine;
import com.zeitheron.aps.intr.ic2.blocks.BlockHVGenerator;
import com.zeitheron.aps.intr.ic2.blocks.BlockLVEngine;
import com.zeitheron.aps.intr.ic2.blocks.BlockLVGenerator;
import com.zeitheron.aps.intr.ic2.blocks.BlockMVEngine;
import com.zeitheron.aps.intr.ic2.blocks.BlockMVGenerator;

public class BlocksARIC2
{
	public static final BlockLVEngine LV_ENGINE = new BlockLVEngine();
	public static final BlockMVEngine MV_ENGINE = new BlockMVEngine();
	public static final BlockHVEngine HV_ENGINE = new BlockHVEngine();
	public static final BlockEVEngine EV_ENGINE = new BlockEVEngine();
	public static final BlockAJEngine AJ_ENGINE = new BlockAJEngine();
	
	public static final BlockLVGenerator LV_GENERATOR = new BlockLVGenerator();
	public static final BlockMVGenerator MV_GENERATOR = new BlockMVGenerator();
	public static final BlockHVGenerator HV_GENERATOR = new BlockHVGenerator();
	public static final BlockEVGenerator EV_GENERATOR = new BlockEVGenerator();
}