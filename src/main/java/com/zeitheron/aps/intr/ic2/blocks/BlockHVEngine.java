package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileHVEngine;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockHVEngine extends BlockBaseEngine<TileHVEngine>
{
	public BlockHVEngine()
	{
		super(Material.IRON, TileHVEngine.class, "ic2_hv_engine", 3);
		setSoundType(SoundType.METAL);
	}
}