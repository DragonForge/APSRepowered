package com.zeitheron.aps.intr.ic2.client.tesr;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileICEngine;
import com.zeitheron.aps.intr.ic2.client.EngineRender;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;

@AtTESR(TileICEngine.class)
public class TESRICEngine extends TESR<TileICEngine>
{
	public ResourceLocation itemTex;
	
	public TESRICEngine(ResourceLocation itemTex)
	{
		this.itemTex = itemTex;
	}
	
	public TESRICEngine()
	{
	}
	
	@Override
	public void renderTileEntityAt(TileICEngine te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		EngineRender.instance.render(te.rotator.getActualRotation(partialTicks) / 360F, te.getOrientation(), te.baseTexture, x, y, z);
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		EngineRender.instance.render(.25F, EnumFacing.UP, itemTex, 0, 0, 0);
	}
}