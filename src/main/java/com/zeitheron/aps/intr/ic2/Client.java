package com.zeitheron.aps.intr.ic2;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.ic2.client.tesr.TESRICEngine;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class Client extends Common
{
	@Override
	public void init()
	{
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksARIC2.LV_ENGINE), new TESRICEngine(new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_elv.png")));
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksARIC2.MV_ENGINE), new TESRICEngine(new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_emv.png")));
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksARIC2.HV_ENGINE), new TESRICEngine(new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_ehv.png")));
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksARIC2.EV_ENGINE), new TESRICEngine(new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_eev.png")));
		ItemRenderingHandler.INSTANCE.bindItemRender(Item.getItemFromBlock(BlocksARIC2.AJ_ENGINE), new TESRICEngine(new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_eadj.png")));
	}
}