package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileHVGenerator;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockHVGenerator extends BlockBaseGenerator<TileHVGenerator>
{
	public BlockHVGenerator()
	{
		super(Material.IRON, TileHVGenerator.class, "ic2_pneumatic_gen_hv", 3);
		setSoundType(SoundType.METAL);
	}
}