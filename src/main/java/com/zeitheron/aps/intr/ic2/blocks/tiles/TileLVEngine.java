package com.zeitheron.aps.intr.ic2.blocks.tiles;

import com.zeitheron.aps.InfoAR;

import net.minecraft.util.ResourceLocation;

public class TileLVEngine extends TileICEngine
{
	{
		baseTexture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_elv.png");
		setSinkTier(1);
		maxChargeEU = 1000;
		pistonSpeed = .4F;
	}
}