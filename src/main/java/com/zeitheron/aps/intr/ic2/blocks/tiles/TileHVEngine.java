package com.zeitheron.aps.intr.ic2.blocks.tiles;

import com.zeitheron.aps.InfoAR;

import net.minecraft.util.ResourceLocation;

public class TileHVEngine extends TileICEngine
{
	{
		baseTexture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_ehv.png");
		setSinkTier(3);
		maxChargeEU = 4000;
		pistonSpeed = .8F;
	}
}