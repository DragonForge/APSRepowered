package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileLVEngine;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockLVEngine extends BlockBaseEngine<TileLVEngine>
{
	public BlockLVEngine()
	{
		super(Material.IRON, TileLVEngine.class, "ic2_lv_engine", 1);
		setSoundType(SoundType.METAL);
	}
}