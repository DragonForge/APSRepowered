package com.zeitheron.aps.intr.ic2.blocks.tiles;

import com.zeitheron.aps.InfoAR;

import net.minecraft.util.ResourceLocation;

public class TileEVEngine extends TileICEngine
{
	{
		baseTexture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_eev.png");
		setSinkTier(4);
		maxChargeEU = 8000;
		pistonSpeed = 1F;
	}
}