package com.zeitheron.aps.intr.ic2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.aps.APSRepowered;
import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.api.ILoadableLoadModule;
import com.zeitheron.aps.init.RecipesAR;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.mod.ModuleLoader;

@ModuleLoader(requiredModid = "ic2", value = "apsmodule")
public class ARModuleIC2 implements ILoadableLoadModule
{
	public static ARModuleIC2 instance;
	{
		instance = this;
	}
	
	public static final Logger LOG = LogManager.getLogger("APSR: IC2");
	
	public final Common proxy = (Common) HammerCore.pipelineProxy.createAndPipeDependingOnSide("com.zeitheron.aps.intr.ic2.Client", "com.zeitheron.aps.intr.ic2.Common");
	
	RecipesARIC2 recipes;
	
	@Override
	public void preInit()
	{
		LOG.info("PreInit");
		RecipesAR.subs.add(recipes = new RecipesARIC2());
		SimpleRegistration.registerFieldBlocksFrom(BlocksARIC2.class, InfoAR.MOD_ID, APSRepowered.tab);
		SimpleRegistration.registerFieldItemsFrom(ItemsARIC2.class, InfoAR.MOD_ID, APSRepowered.tab);
	}
	
	@Override
	public void init()
	{
		LOG.info("Init");
		proxy.init();
	}
}