package com.zeitheron.aps.intr.ic2.blocks;

import java.util.List;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileICGenerator;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBaseGenerator<T extends TileICGenerator> extends BlockDeviceHC<T>
{
	public final int tier;
	
	public BlockBaseGenerator(Material mat, Class<T> tc, String name, int tier)
	{
		super(mat, tc, name);
		this.tier = tier;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn)
	{
		tooltip.add("Power Tier: " + tier);
	}
}