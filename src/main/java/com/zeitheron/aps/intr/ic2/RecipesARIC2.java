package com.zeitheron.aps.intr.ic2;

import com.zeitheron.aps.api.laser_table.ILaserTableRegisterHook;
import com.zeitheron.aps.api.laser_table.RecipesLaserTable;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.init.ItemsAR;
import com.zeitheron.hammercore.utils.ConsumableItem;
import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class RecipesARIC2 extends RecipeRegistry implements ILaserTableRegisterHook
{
	@Override
	public void registerLaserTable()
	{
		Item crafting = ForgeRegistries.ITEMS.getValue(new ResourceLocation("ic2", "crafting"));
		RecipesLaserTable.add(ItemsARIC2.IRIDIUM_CHIP, 64_000, ConsumableItem.of(1, ItemsAR.EMERALD_POWER_CORE), ConsumableItem.of(2, new ItemStack(crafting, 1, 4)));
	}
	
	public static ItemStack cable(int type, boolean... insulation)
	{
		ItemStack c = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation("ic2", "cable")), 1, type);
		c.setTagCompound(new NBTTagCompound());
		c.getTagCompound().setByte("type", (byte) type);
		c.getTagCompound().setByte("insulation", (byte) insulation.length);
		return c;
	}
	
	@Override
	public void crafting()
	{
		Item te = ForgeRegistries.ITEMS.getValue(new ResourceLocation("ic2", "te"));
		Item crafting = ForgeRegistries.ITEMS.getValue(new ResourceLocation("ic2", "crafting"));
		
		shaped(ItemsARIC2.IRDIUM_POWER_CORE, "iri", "gcg", "iri", 'i', new ItemStack(crafting, 1, 4), 'r', "gemLapis", 'g', "circuitAdvanced", 'c', ItemsARIC2.IRIDIUM_CHIP);
		
		shaped(BlocksARIC2.LV_ENGINE, "xxx", " h ", "gtg", 'x', "plateIron", 'h', "blockGlassHardened", 'g', "gearIron", 't', new ItemStack(te, 1, 77));
		shaped(BlocksARIC2.MV_ENGINE, "xxx", " h ", "gtg", 'x', "plateBronze", 'h', "blockGlassHardened", 'g', "gearBronze", 't', new ItemStack(te, 1, 78));
		shaped(BlocksARIC2.HV_ENGINE, "xxx", " h ", "gtg", 'x', "plateSteel", 'h', "blockGlassHardened", 'g', "gearSteel", 't', new ItemStack(te, 1, 79));
		shaped(BlocksARIC2.EV_ENGINE, "xxx", " h ", "gtg", 'x', "plateIridium", 'h', "blockGlassHardened", 'g', "gearIridium", 't', new ItemStack(te, 1, 80));
		shaped(BlocksARIC2.AJ_ENGINE, " p ", "eee", "gcg", 'p', BlocksAR.PIPE_ENERGY_PLATINIRIDIUM, 'e', BlocksARIC2.EV_ENGINE, 'g', "gearPlatiniridium", 'c', ItemsARIC2.IRDIUM_POWER_CORE);
		
		shaped(BlocksARIC2.LV_GENERATOR, "mpm", "isi", "mcm", 'p', cable(4, true), 'm', "plateIron", 'i', "plateIron", 's', new ItemStack(te, 1, 72), 'c', "circuitBasic");
		shaped(BlocksARIC2.MV_GENERATOR, "mpm", "isi", "mcm", 'p', cable(0, true), 'm', "plateIron", 'i', "plateBronze", 's', new ItemStack(te, 1, 73), 'c', "circuitBasic");
		shaped(BlocksARIC2.HV_GENERATOR, "mpm", "isi", "mcm", 'p', cable(2, true, true), 'm', "plateIron", 'i', "plateSteel", 's', new ItemStack(te, 1, 74), 'c', "circuitAdvanced");
		shaped(BlocksARIC2.EV_GENERATOR, "mpm", "isi", "mcm", 'p', cable(1, true), 'm', "plateIron", 'i', new ItemStack(crafting, 1, 4), 's', new ItemStack(te, 1, 75), 'c', "circuitAdvanced");
	}
}