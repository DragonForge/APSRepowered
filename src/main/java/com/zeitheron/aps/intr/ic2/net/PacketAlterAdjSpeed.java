package com.zeitheron.aps.intr.ic2.net;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileAJEngine;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;

public class PacketAlterAdjSpeed implements IPacket
{
	public int world;
	public long pos;
	public double speed;
	public EnumMathOperator operator;
	
	static
	{
		IPacket.handle(PacketAlterAdjSpeed.class, PacketAlterAdjSpeed::new);
	}
	
	public PacketAlterAdjSpeed()
	{
		
	}
	
	public PacketAlterAdjSpeed(TileAJEngine engine, EnumMathOperator operator, double speed)
	{
		this.operator = operator;
		this.speed = speed;
		if(engine != null)
		{
			this.world = engine.getWorld().provider.getDimension();
			this.pos = engine.getPos().toLong();
		}
	}
	
	@Override
	public IPacket executeOnServer(PacketContext net)
	{
		EntityPlayerMP mp = net.getSender();
		if(mp != null)
		{
			MinecraftServer server = mp.getServer();
			WorldServer world = server.getWorld(this.world);
			TileAJEngine tile = WorldUtil.cast(world.getTileEntity(BlockPos.fromLong(pos)), TileAJEngine.class);
			if(tile != null)
			{
				if(this.operator == EnumMathOperator.ADD)
					tile.setSpeed(tile.speed + this.speed, true);
				if(this.operator == EnumMathOperator.SUBTRACT)
					tile.setSpeed(tile.speed - this.speed, true);
				if(this.operator == EnumMathOperator.SET)
					tile.setSpeed(this.speed, true);
				if(this.operator == EnumMathOperator.RESET)
					tile.setSpeed(TileAJEngine.DEF_SPEED, true);
			}
		}
		return null;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setDouble("Speed", speed);
		nbt.setInteger("Dim", world);
		nbt.setLong("Pos", pos);
		nbt.setByte("Op", (byte) operator.ordinal());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		speed = nbt.getDouble("Speed");
		world = nbt.getInteger("Dim");
		pos = nbt.getLong("Pos");
		operator = EnumMathOperator.values()[nbt.getByte("Op")];
	}
	
	public enum EnumMathOperator
	{
		ADD, //
		SUBTRACT, //
		SET, //
		RESET;
	}
}