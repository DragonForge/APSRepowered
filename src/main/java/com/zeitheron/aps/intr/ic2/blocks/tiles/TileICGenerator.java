package com.zeitheron.aps.intr.ic2.blocks.tiles;

import java.text.DecimalFormat;

import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;

import ic2.api.energy.EnergyNet;
import ic2.api.energy.prefab.BasicSource;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileICGenerator extends TileSyncableTickable implements IEnergyStorage, ITooltipProviderHC
{
	public static final DecimalFormat df = new DecimalFormat("#0.0");
	public FEEnergyStorage energy = new FEEnergyStorage(20_000);
	private boolean setup = false;
	public int rfPerTick = 128;
	
	public int sourceTier = 1;
	public double voltage;
	
	public int usage;
	
	public BasicSource source = new BasicSource(this, 4000, sourceTier);
	
	public void setSourceTier(int sourceTier)
	{
		double euMax = EnergyNet.instance.getPowerFromTier(sourceTier);
		this.voltage = euMax;
		this.rfPerTick = (int) euMax * 4;
		this.sourceTier = sourceTier;
		
		if(source != null)
			source.setSourceTier(sourceTier);
	}
	
	public void doSetup()
	{
		source.onLoad();
		setup = true;
	}
	
	@Override
	public void tick()
	{
		if(!setup)
			doSetup();
		
		int usage = energy.extractEnergy((int) Math.ceil(source.addEnergy(energy.getEnergyStored() / 4D) * 4D), false);
		if(this.usage > 0 && usage == 0)
			sendChangesToNearby();
		if(!world.isRemote)
			this.usage = usage;
		if(usage > 0 && atTickRate(20))
			sendChangesToNearby();
	}
	
	@Override
	public void invalidate()
	{
		source.invalidate();
		super.invalidate();
	}
	
	@Override
	public void onChunkUnload()
	{
		source.onChunkUnload();
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Usage", usage);
		source.writeToNBT(nbt);
		energy.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		usage = nbt.getInteger("Usage");
		source.readFromNBT(nbt);
		energy.readFromNBT(nbt);
		setTooltipDirty(true);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return energy.receiveEnergy(maxReceive, simulate);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return energy.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return energy.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	private boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("gui.hammercore.consumption"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", usage)).appendColor(TextFormatting.BLUE));
		tip.append(new TranslationTooltipInfo("gui.hammercore.fept"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("gui.hammercore.generation"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(df.format(usage / 4D) + " ").appendColor(TextFormatting.GOLD));
		tip.append(new StringTooltipInfo("EU/t"));
	}
}