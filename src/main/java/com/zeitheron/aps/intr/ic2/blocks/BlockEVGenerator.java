package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileEVGenerator;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockEVGenerator extends BlockBaseGenerator<TileEVGenerator>
{
	public BlockEVGenerator()
	{
		super(Material.IRON, TileEVGenerator.class, "ic2_pneumatic_gen_ev", 4);
		setSoundType(SoundType.METAL);
	}
}