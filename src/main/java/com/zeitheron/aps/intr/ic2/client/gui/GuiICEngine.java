package com.zeitheron.aps.intr.ic2.client.gui;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.ic2.blocks.tiles.TileICEngine;
import com.zeitheron.aps.intr.ic2.inventory.ContainerICEngine;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiICEngine
		extends GuiContainer
{
	int mouseX, mouseY;
	public TileICEngine tile;

	public GuiICEngine(EntityPlayer player, TileICEngine tile)
	{
		super(new ContainerICEngine(player, tile));
		this.tile = tile;
	}

	public GuiICEngine(ContainerICEngine container, TileICEngine tile)
	{
		super(container);
		this.tile = tile;
	}

	DynGuiTex tex;

	@Override
	public void initGui()
	{
		super.initGui();

		GuiTexBakery bak = GuiTexBakery.start();
		bak.body(0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			bak.slot(s.xPos - 1, s.yPos - 1);
		bak.slot(8, 10, 10, 60);
		tex = bak.bake();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;

		{
			IBlockState state = tile.getWorld().getBlockState(tile.getPos());
			String ln = I18n.format(state.getBlock().getTranslationKey() + ".name");
			fontRenderer.drawString(ln, (xSize - fontRenderer.getStringWidth(ln)) / 2, 6, shade);
		}

		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);

		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltips

		ItemStack mouse = HCNet.getMouseStack(mc.player);

		GlStateManager.enableBlend();

		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				List<String> tip = TooltipHelper.generateEnergy(tile);
				tip.add(TextFormatting.DARK_GRAY + "Max. " + I18n.format("gui.hammercore.generation") + ": " + String.format("%,d", (int) (tile.euPerTick * 4)) + " " + I18n.format("gui.hammercore.fept"));
				drawHoveringText(tip, mouseX, mouseY);
			}
		} else if(mouseX > guiLeft + 83 && mouseY > guiTop + 24 && mouseX < guiLeft + 92 && mouseY < guiTop + 39)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 83, guiTop + 24, 9.5, 14.5, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				List<String> tip = TooltipHelper.emptyTooltipList();
				tip.add(String.format("%,d", Math.round(tile.sink.getEnergyStored())) + " / " + String.format("%,d", Math.round(tile.sink.getCapacity())) + " EU");
				drawHoveringText(tip, mouseX, mouseY);
			}
		}

		GL11.glPopMatrix();
	}

	@Override
	public void updateScreen()
	{
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TileICEngine)
			this.tile = (TileICEngine) tile;
		super.updateScreen();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mouseX = mouseX;
		this.mouseY = mouseY;
		tex.render(guiLeft, guiTop);
		GL11.glColor4f(1, 1, 1, 1);

		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/gui/widgets.png");
		int col = GuiTheme.current().bodyLayerLU;
		GL11.glColor4f(ColorHelper.getRed(col), ColorHelper.getGreen(col), ColorHelper.getBlue(col), 1);
		RenderUtil.drawTexturedModalRect(guiLeft + 84, guiTop + 25, 0, 64, 7, 13);

		if(tile.sink.getEnergyStored() > 0)
		{
			double k = Math.min(1, tile.sink.getEnergyStored() / tile.sink.getCapacity()) * 13D;

			GL11.glColor4f(1, 1, 1, 1);
			RenderUtil.drawTexturedModalRect(guiLeft + 84, guiTop + 25 + 13 - k, 7, 64 + 13 - k, 7, k);
		}

		float energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();

			// Show uses/recipes
			if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}

		super.keyTyped(typedChar, keyCode);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawDefaultBackground();
		GL11.glColor4f(1, 1, 1, 1);
		super.drawScreen(mouseX, mouseY, partialTicks);
		renderHoveredToolTip(mouseX, mouseY);
	}
}