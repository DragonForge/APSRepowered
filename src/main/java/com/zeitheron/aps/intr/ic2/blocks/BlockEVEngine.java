package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileEVEngine;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockEVEngine extends BlockBaseEngine<TileEVEngine>
{
	public BlockEVEngine()
	{
		super(Material.IRON, TileEVEngine.class, "ic2_ev_engine", 4);
		setSoundType(SoundType.METAL);
	}
}