package com.zeitheron.aps.intr.ic2.blocks;

import com.zeitheron.aps.intr.ic2.blocks.tiles.TileMVEngine;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockMVEngine extends BlockBaseEngine<TileMVEngine>
{
	public BlockMVEngine()
	{
		super(Material.IRON, TileMVEngine.class, "ic2_mv_engine", 2);
		setSoundType(SoundType.METAL);
	}
}