package com.zeitheron.aps.intr.ic2.blocks.tiles;

import com.zeitheron.aps.InfoAR;

import net.minecraft.util.ResourceLocation;

public class TileMVEngine extends TileICEngine
{
	{
		baseTexture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/engines/base_emv.png");
		setSinkTier(2);
		maxChargeEU = 2000;
		pistonSpeed = .6F;
	}
}