package com.zeitheron.aps.intr.crafttweaker;

import java.util.LinkedList;

import com.zeitheron.aps.intr.crafttweaker.core.ICTCompat;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;

public class CTCompatImpl implements ICTCompat
{
	private static final LinkedList<IAction> lateActions = new LinkedList<>();
	
	@Override
	public void onLoadComplete()
	{
		lateActions.forEach(CraftTweakerAPI::apply);
		lateActions.clear();
	}
	
	@Override
	public void init()
	{
		lateActions.forEach(CraftTweakerAPI::apply);
		lateActions.clear();
	}
	
	@Override
	public void addLateAction(Object action)
	{
		lateActions.addLast((IAction) action);
	}
}