package com.zeitheron.aps.intr.crafttweaker.modules;

import com.zeitheron.aps.api.TokamakFluidRegistry;
import com.zeitheron.aps.intr.crafttweaker.CraftTweakerCompat;
import com.zeitheron.aps.intr.crafttweaker.core.BaseAction;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.liquid.ILiquidStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.apsrepowered.Tokamak")
@ZenRegister
public class Tokamak
{
	@ZenMethod
	public static void addFuel(ILiquidStack input, int burnTime)
	{
		Object intern = input.getInternal();
		if(intern instanceof FluidStack)
			CraftTweakerCompat.compat().addLateAction(new Add(((FluidStack) intern).getFluid(), burnTime));
	}
	
	@ZenMethod
	public static void removeFuel(ILiquidStack input)
	{
		Object intern = input.getInternal();
		if(intern instanceof FluidStack)
			CraftTweakerCompat.compat().addLateAction(new Remove(((FluidStack) intern).getFluid()));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(Fluid fl, int bt)
		{
			super("Tokamak", () -> TokamakFluidRegistry.addTokamakFuel(fl, bt));
		}
	}
	
	private static final class Remove extends BaseAction
	{
		private Remove(Fluid fl)
		{
			super("Tokamak", () -> TokamakFluidRegistry.TMAP.remove(fl));
		}
	}
}