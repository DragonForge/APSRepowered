package com.zeitheron.aps.intr.crafttweaker.modules;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.aps.api.laser_table.LaserTableRecipe;
import com.zeitheron.aps.api.laser_table.RecipesLaserTable;
import com.zeitheron.aps.intr.crafttweaker.CraftTweakerCompat;
import com.zeitheron.aps.intr.crafttweaker.core.BaseAction;
import com.zeitheron.aps.intr.crafttweaker.core.InputHelper;
import com.zeitheron.hammercore.utils.ConsumableItem;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.apsrepowered.LaserTable")
@ZenRegister
public class LaserTable
{
	@ZenMethod
	public static void add(IItemStack output, int fe, IIngredient... ingredients)
	{
		ConsumableItem[] cis = new ConsumableItem[ingredients.length];
		for(int i = 0; i < cis.length; ++i)
		{
			Ingredient s = InputHelper.toIngredient(ingredients[i]);
			ingredients[i].getAmount();
			cis[i] = ConsumableItem.of(ingredients[i].getAmount(), s);
		}
		CraftTweakerCompat.compat().addLateAction(new Add(InputHelper.toStack(output), fe, cis));
	}
	
	@ZenMethod
	public static void remove(IItemStack output)
	{
		CraftTweakerCompat.compat().addLateAction(new Remove(InputHelper.toStack(output)));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(ItemStack out, int FE, ConsumableItem... items)
		{
			super("LaserTable", () -> RecipesLaserTable.add(out, FE, items));
		}
	}
	
	private static final class Remove extends BaseAction
	{
		private Remove(ItemStack out)
		{
			super("LaserTable", () ->
			{
				List<LaserTableRecipe> rem = new ArrayList<>();
				for(LaserTableRecipe rlt : RecipesLaserTable.RECIPES)
					if(out.isItemEqual(rlt.getOutput()))
						rem.add(rlt);
				RecipesLaserTable.RECIPES.removeAll(rem);
			});
		}
	}
}