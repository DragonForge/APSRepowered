package com.zeitheron.aps.intr.crafttweaker.modules;

import com.zeitheron.aps.api.MagmafierMatterRegistry;
import com.zeitheron.aps.intr.crafttweaker.CraftTweakerCompat;
import com.zeitheron.aps.intr.crafttweaker.core.BaseAction;
import com.zeitheron.aps.intr.crafttweaker.core.InputHelper;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.apsrepowered.Magmafier")
@ZenRegister
public class Magmafier
{
	@ZenMethod
	public static void add(IIngredient input, int matter)
	{
		CraftTweakerCompat.compat().addLateAction(new Add(InputHelper.toIngredient(input), matter));
	}
	
	@ZenMethod
	public static void remove(IItemStack input)
	{
		CraftTweakerCompat.compat().addLateAction(new Remove(InputHelper.toStack(input)));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(Ingredient in, int matter)
		{
			super("Magmafier", () -> MagmafierMatterRegistry.addMatter(in, matter));
		}
	}
	
	private static final class Remove extends BaseAction
	{
		private Remove(ItemStack in)
		{
			super("Magmafier", () -> MagmafierMatterRegistry.removeMatter(in));
		}
	}
}