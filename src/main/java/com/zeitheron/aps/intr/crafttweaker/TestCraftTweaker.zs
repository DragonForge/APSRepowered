
mods.apsrepowered.LaserTable.add(<minecraft:diamond>, 20000, <ore:coal> * 192);

mods.apsrepowered.Grinder.add(<ore:coal>, 2400, [0.1, 10, 100], [<minecraft:diamond>, <minecraft:torch>, <hammermetals:coal_dust>]);

mods.apsrepowered.Tokamak.addFuel(<liquid:lava>, 50);

mods.apsrepowered.Tokamak.removeFuel(<liquid:water>);