package com.zeitheron.aps.intr.crafttweaker.modules;

import java.util.Optional;

import com.zeitheron.aps.api.GrinderRecipe;
import com.zeitheron.aps.intr.crafttweaker.CraftTweakerCompat;
import com.zeitheron.aps.intr.crafttweaker.core.BaseAction;
import com.zeitheron.aps.intr.crafttweaker.core.InputHelper;

import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenClass("mods.apsrepowered.Grinder")
@ZenRegister
public class Grinder
{
	@ZenMethod
	public static void add(IIngredient input, int fe, float[] chances, IItemStack... outputs)
	{
		ItemStack[] os = new ItemStack[outputs.length];
		for(int i = 0; i < os.length; ++i)
			os[i] = InputHelper.toStack(outputs[i]);
		CraftTweakerCompat.compat().addLateAction(new Add(new GrinderRecipe(InputHelper.toIngredient(input), input.getAmount(), os, chances, fe)));
	}
	
	@ZenMethod
	public static void add(IIngredient input, int fe, float chance, IItemStack output)
	{
		CraftTweakerCompat.compat().addLateAction(new Add(new GrinderRecipe(InputHelper.toIngredient(input), input.getAmount(), InputHelper.toStack(output), chance, fe)));
	}
	
	@ZenMethod
	public static void add(IIngredient input, int fe, IItemStack output)
	{
		CraftTweakerCompat.compat().addLateAction(new Add(new GrinderRecipe(InputHelper.toIngredient(input), input.getAmount(), InputHelper.toStack(output), 100F, fe)));
	}
	
	@ZenMethod
	public static void add(IIngredient input, int fe, float chance, IItemStack... outputs)
	{
		ItemStack[] os = new ItemStack[outputs.length];
		for(int i = 0; i < os.length; ++i)
			os[i] = InputHelper.toStack(outputs[i]);
		CraftTweakerCompat.compat().addLateAction(new Add(new GrinderRecipe(InputHelper.toIngredient(input), input.getAmount(), os, chance, fe)));
	}
	
	@ZenMethod
	public static void remove(IItemStack input)
	{
		Optional<GrinderRecipe> rec = GrinderRecipe.GRINDER_RECIPES.stream().filter(g -> g.isIngredient(InputHelper.toStack(input))).findAny();
		if(rec.isPresent())
			CraftTweakerCompat.compat().addLateAction(new Remove(rec.get()));
	}
	
	private static final class Add extends BaseAction
	{
		private Add(GrinderRecipe rec)
		{
			super("Grinder", rec::add);
		}
	}
	
	private static final class Remove extends BaseAction
	{
		private Remove(GrinderRecipe rec)
		{
			super("Grinder", rec::remove);
		}
	}
}