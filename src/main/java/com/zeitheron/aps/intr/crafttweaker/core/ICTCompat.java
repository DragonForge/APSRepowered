package com.zeitheron.aps.intr.crafttweaker.core;

public interface ICTCompat
{
	void onLoadComplete();
	
	void init();
	
	void addLateAction(Object action);
}