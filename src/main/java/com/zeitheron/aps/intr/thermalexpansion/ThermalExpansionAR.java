package com.zeitheron.aps.intr.thermalexpansion;

import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.init.FluidsAR;

import cofh.thermalexpansion.util.managers.machine.RefineryManager;
import cofh.thermalexpansion.util.managers.machine.TransposerManager;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

public class ThermalExpansionAR
{
	public static void register()
	{
		RefineryManager.addRecipe(1000, new FluidStack(FluidRegistry.WATER, Fluid.BUCKET_VOLUME), new FluidStack(FluidsAR.HEAVY_WATER, Fluid.BUCKET_VOLUME));
		TransposerManager.addFillRecipe(500, new ItemStack(BlocksAR.ROCK_GLASS, 4), new ItemStack(BlocksAR.BURNT_GLASS, 4), new FluidStack(FluidRegistry.LAVA, Fluid.BUCKET_VOLUME / 4), false);
	}
}