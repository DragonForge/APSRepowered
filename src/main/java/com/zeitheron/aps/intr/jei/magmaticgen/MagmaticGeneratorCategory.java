package com.zeitheron.aps.intr.jei.magmaticgen;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.ingredient.RenderEnergyBar;
import com.zeitheron.hammercore.client.gui.GuiTankTexture;
import com.zeitheron.hammercore.compat.jei.TankOverlayDrawable;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.*;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;

public class MagmaticGeneratorCategory
		implements IRecipeCategory<MagmaticGeneratorWrapper>
{
	IDrawable draw;

	public MagmaticGeneratorCategory(IJeiHelpers helper)
	{
		draw = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/jei_magmatic_gen_fuel.png"), 0, 0, 84, 64);
	}

	@Override
	public String getUid()
	{
		return APSRepoweredJEI.MAGMATIC_GENERATOR;
	}

	@Override
	public String getTitle()
	{
		return I18n.format("jei." + getUid());
	}

	@Override
	public String getModName()
	{
		return InfoAR.MOD_NAME;
	}

	@Override
	public IDrawable getBackground()
	{
		return draw;
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, MagmaticGeneratorWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup items = recipeLayout.getItemStacks();
		IGuiFluidStackGroup fluids = recipeLayout.getFluidStacks();
		IGuiIngredientGroup<ICIngr> custom = recipeLayout.getIngredientsGroup(ICIngr.class);

		items.init(0, true, 61, 22);
		items.set(0, FluidUtil.getFilledBucket(recipeWrapper.getInput()));

		FluidStack fs = recipeWrapper.getInput();
		fluids.init(1, true, 3, 3, 16, 58, fs.amount, true, new TankOverlayDrawable(new GuiTankTexture(14, 58)));
		fluids.set(1, fs);

		int energy = Math.round(recipeWrapper.getBurnTime() * 160);

		custom.init(1, false, new RenderEnergyBar(8, 58, energy, 0), 27, 3, 8, 58, 0, 0);
		custom.set(1, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}