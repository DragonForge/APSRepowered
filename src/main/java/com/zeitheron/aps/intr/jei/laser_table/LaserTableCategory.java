package com.zeitheron.aps.intr.jei.laser_table;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.cfg.TokamakConfig;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.ingredient.RenderEnergyBar;
import com.zeitheron.hammercore.utils.ConsumableItem;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiIngredientGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

public class LaserTableCategory implements IRecipeCategory<LaserTableWrapper>
{
	IDrawable draw;
	
	public LaserTableCategory(IJeiHelpers helper)
	{
		draw = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/jei_laser_table.png"), 0, 0, 118, 62);
	}
	
	@Override
	public String getUid()
	{
		return APSRepoweredJEI.LASER_TABLE;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format("jei." + getUid());
	}
	
	@Override
	public String getModName()
	{
		return InfoAR.MOD_NAME;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return draw;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, LaserTableWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup g = recipeLayout.getItemStacks();
		IGuiIngredientGroup<ICIngr> custom = recipeLayout.getIngredientsGroup(ICIngr.class);
		
		g.init(0, false, 95, 22);
		g.set(0, recipeWrapper.r.getOutput().copy());
		
		ConsumableItem[] cos = recipeWrapper.r.getInputs();
		for(int x = 0; x < 3; ++x)
			for(int y = 0; y < 3; ++y)
			{
				int s = y + x * 3;
				if(s >= cos.length)
					continue;
				
				g.init(s + 1, true, 2 + (x * 18), 5 + (y * 18));
				
				NonNullList<ItemStack> items = NonNullList.from(ItemStack.EMPTY, cos[s].ingr.getMatchingStacks());
				for(int i = 0; i < items.size(); ++i)
				{
					ItemStack is = items.get(i).copy();
					is.setCount(cos[s].amount);
					items.set(i, is);
				}
				g.set(s + 1, items);
			}
		
		custom.init(1, true, new RenderEnergyBar(8, 58, recipeWrapper.r.FE, 0), 70, 3, 8, 58, 0, 0);
		custom.set(1, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}