package com.zeitheron.aps.intr.jei.magmaticgen;

import java.util.Collections;
import java.util.List;

import com.zeitheron.aps.blocks.tiles.TileMagmaticGenerator;
import com.zeitheron.aps.cfg.MachineConfig;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;

public class MagmaticGeneratorWrapper implements IRecipeWrapper
{
	public final Fluid fluid;
	
	public MagmaticGeneratorWrapper(Fluid fluid)
	{
		this.fluid = fluid;
	}
	
	public FluidStack getInput()
	{
		return new FluidStack(fluid, MachineConfig.MagmaticGeneratorFluidConsumption);
	}
	
	int time;
	
	public int getBurnTime()
	{
		if(time > 0)
			return time;
		time = TileMagmaticGenerator.getBurnTime(getInput());
		return time;
	}
	
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY)
	{
		// float times = Fluid.BUCKET_VOLUME / (float)
		// MachineConfig.MagmaticGeneratorFluidConsumption;
		// int energy = Math.round(getBurnTime() * 160 * times);
		//
		// if(mouseX >= 27 && mouseY >= 3 && mouseX < 35 && mouseY < 61)
		// return TooltipHelper.generate("energy", energy, energy, "fe");
		return Collections.emptyList();
	}
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
		// float energy = 58F;
		// GuiWidgets.drawEnergy(27, 61 - energy, 8, energy,
		// EnumPowerAnimation.UP);
		//
		// GlStateManager.enableBlend();
		//
		// if(mouseX >= 27 && mouseY >= 3 && mouseX < 35 && mouseY < 61)
		// {
		// GL11.glDisable(GL11.GL_TEXTURE_2D);
		// RenderUtil.drawColoredModalRect(27, 3, 8, 58, 0xAAFFFFFF);
		// GL11.glEnable(GL11.GL_TEXTURE_2D);
		// }
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInput(FluidStack.class, getInput());
		ingredients.setInput(ItemStack.class, FluidUtil.getFilledBucket(getInput()));
		ingredients.setOutput(ICIngr.class, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}