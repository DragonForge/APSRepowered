package com.zeitheron.aps.intr.jei.tokamak;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileTokamak;
import com.zeitheron.aps.cfg.TokamakConfig;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.ingredient.RenderEnergyBar;
import com.zeitheron.hammercore.client.gui.GuiTankTexture;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.compat.jei.TankOverlayDrawable;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiFluidStackGroup;
import mezz.jei.api.gui.IGuiIngredientGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

public class TokamakFuelCategory
		implements IRecipeCategory<TokamakFuelWrapper>
{
	IDrawable draw, tank;

	public TokamakFuelCategory(IJeiHelpers helper)
	{
		draw = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/jei_tokamak_fuel.png"), 0, 0, 106, 64);
		tank = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/widgets.png"), 1, 17, 16, 58);
	}

	@Override
	public String getUid()
	{
		return APSRepoweredJEI.TOKAMAK_FUEL;
	}

	@Override
	public String getTitle()
	{
		return I18n.format("jei." + getUid());
	}

	@Override
	public String getModName()
	{
		return InfoAR.MOD_NAME;
	}

	@Override
	public IDrawable getBackground()
	{
		return draw;
	}

	@Override
	public void drawExtras(Minecraft minecraft)
	{
		double speed = 4F;
		double rad = Math.toRadians((System.currentTimeMillis() % (3600L * speed))) / speed;
		float green = ((float) Math.sin(rad) + 1) / 2F;
		green = .7F + green * .3F;
		GuiWidgets.drawSquare(45, 5, 1, green, .2F);

		GuiWidgets.drawEnergy(27, 3, 8, 58, EnumPowerAnimation.UP);

		GuiWidgets.drawLine(27, 61 - (((TokamakConfig.TokamakMaxTemp * TokamakConfig.TokamakFusionFraction) / (float) TokamakConfig.TokamakMaxTemp) * 58));
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, TokamakFuelWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiFluidStackGroup i = recipeLayout.getFluidStacks();
		IGuiIngredientGroup<ICIngr> custom = recipeLayout.getIngredientsGroup(ICIngr.class);

		i.init(0, true, 3, 3, 16, 58, Fluid.BUCKET_VOLUME, true, new TankOverlayDrawable(new GuiTankTexture(14, 64)));
		i.set(0, new FluidStack(recipeWrapper.fuel, Fluid.BUCKET_VOLUME));

		custom.init(1, false, new RenderEnergyBar(58, 8, TileTokamak.getFEProduced(100, recipeWrapper.fuel, Fluid.BUCKET_VOLUME), 0), 45, 42, 58, 8, 0, 0);
		custom.set(1, APSRepoweredJEI.IG_FORGE_ENERGY);

		custom.init(2, true, new RenderEnergyBar(58, 8, TokamakConfig.TokamakMaxEnergyReceive, 0), 45, 29, 58, 8, 0, 0);
		custom.set(2, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}