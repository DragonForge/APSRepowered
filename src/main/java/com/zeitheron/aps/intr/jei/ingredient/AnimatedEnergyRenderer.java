package com.zeitheron.aps.intr.jei.ingredient;

import java.util.List;

import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.TooltipHelper;

import mezz.jei.api.ingredients.IIngredientRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;

public class AnimatedEnergyRenderer implements IIngredientRenderer<ICIngr>
{
	public final int w, h, fe, mfe;
	
	public AnimatedEnergyRenderer(int w, int h, int energy, int maxEnergy)
	{
		this.w = w;
		this.h = h;
		this.fe = energy;
		this.mfe = maxEnergy;
	}
	
	@Override
	public void render(Minecraft minecraft, int xPosition, int yPosition, ICIngr ingredient)
	{
		GuiWidgets.drawEnergy(xPosition, yPosition, w, h, EnumPowerAnimation.UP);
	}
	
	@Override
	public List<String> getTooltip(Minecraft minecraft, ICIngr ingredient, ITooltipFlag tooltipFlag)
	{
		return TooltipHelper.generate("energy", fe, mfe, "fe");
	}
}