package com.zeitheron.aps.intr.jei.laser_table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.aps.api.laser_table.LaserTableRecipe;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.hammercore.utils.ConsumableItem;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

public class LaserTableWrapper implements IRecipeWrapper
{
	public final LaserTableRecipe r;
	
	public LaserTableWrapper(LaserTableRecipe r)
	{
		this.r = r;
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		List<List<ItemStack>> inputs = new ArrayList<>();
		for(ConsumableItem ci : r.getInputs())
			inputs.add(Arrays.asList(ci.ingr.getMatchingStacks()));
		ingredients.setInputLists(ItemStack.class, inputs);
		ingredients.setInput(ICIngr.class, APSRepoweredJEI.IG_FORGE_ENERGY);
		ingredients.setOutput(ItemStack.class, r.getOutput().copy());
	}
}