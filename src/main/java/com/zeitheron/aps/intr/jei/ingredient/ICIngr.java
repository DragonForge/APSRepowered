package com.zeitheron.aps.intr.jei.ingredient;

import java.util.HashSet;
import java.util.Set;

import com.zeitheron.aps.InfoAR;

import net.minecraft.item.ItemStack;

public interface ICIngr
{
	Set<ICIngr> INGREDIENTS = new HashSet<>();
	
	String id();
	
	default ICIngr implement()
	{
		INGREDIENTS.add(this);
		return this;
	}
	
	default ItemStack cheat()
	{
		return ItemStack.EMPTY;
	}
	
	default String icon()
	{
		return InfoAR.MOD_ID + ":textures/ingredients/" + id() + ".png";
	}
	
	default String modid()
	{
		return InfoAR.MOD_ID;
	}
}