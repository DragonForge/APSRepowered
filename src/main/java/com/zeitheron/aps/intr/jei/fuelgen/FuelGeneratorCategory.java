package com.zeitheron.aps.intr.jei.fuelgen;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.ingredient.RenderEnergyBar;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiIngredientGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredientRenderer;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;

public class FuelGeneratorCategory implements IRecipeCategory<FuelGeneratorWrapper>
{
	IDrawable draw;
	
	public FuelGeneratorCategory(IJeiHelpers helper)
	{
		draw = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/jei_solid_gen_fuel.png"), 0, 0, 84, 64);
	}
	
	@Override
	public String getUid()
	{
		return APSRepoweredJEI.FUEL_GENERATOR;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format("jei." + getUid());
	}
	
	@Override
	public String getModName()
	{
		return InfoAR.MOD_NAME;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return draw;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, FuelGeneratorWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup items = recipeLayout.getItemStacks();
		IGuiIngredientGroup<ICIngr> custom = recipeLayout.getIngredientsGroup(ICIngr.class);
		
		items.init(0, true, 61, 22);
		items.set(0, recipeWrapper.getInput());
		
		custom.init(1, false, new RenderEnergyBar(8, 58, recipeWrapper.getBurnTime() * 40, 0), 27, 3, 8, 58, 0, 0);
		custom.set(1, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}