package com.zeitheron.aps.intr.jei;

import java.util.stream.Collectors;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.api.GrinderRecipe;
import com.zeitheron.aps.api.MagmafierMatterRegistry;
import com.zeitheron.aps.api.TokamakFluidRegistry;
import com.zeitheron.aps.api.laser_table.LaserTableRecipe;
import com.zeitheron.aps.api.laser_table.RecipesLaserTable;
import com.zeitheron.aps.blocks.tiles.TileMagmaticGenerator;
import com.zeitheron.aps.client.gui.GuiLaserTable;
import com.zeitheron.aps.client.gui.GuiTokamak;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.intr.jei.fuelgen.FuelGeneratorCategory;
import com.zeitheron.aps.intr.jei.fuelgen.FuelGeneratorWrapper;
import com.zeitheron.aps.intr.jei.grinder.GrinderCategory;
import com.zeitheron.aps.intr.jei.grinder.GrinderWrapper;
import com.zeitheron.aps.intr.jei.ingredient.CustomIngredientManager;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.laser_table.LaserTableCategory;
import com.zeitheron.aps.intr.jei.laser_table.LaserTableWrapper;
import com.zeitheron.aps.intr.jei.magmafier.MagmafierCategory;
import com.zeitheron.aps.intr.jei.magmafier.MagmafierWrapper;
import com.zeitheron.aps.intr.jei.magmaticgen.MagmaticGeneratorCategory;
import com.zeitheron.aps.intr.jei.magmaticgen.MagmaticGeneratorWrapper;
import com.zeitheron.aps.intr.jei.tokamak.TokamakFuelCategory;
import com.zeitheron.aps.intr.jei.tokamak.TokamakFuelWrapper;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.ingredients.IModIngredientRegistration;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

@JEIPlugin
public class JEIAPSR implements IModPlugin
{
	@Override
	public void register(IModRegistry registry)
	{
		// All items and sub-items in the game.
		NonNullList<ItemStack> allSubs = NonNullList.create();
		ForgeRegistries.ITEMS.getValuesCollection().stream().forEach(i ->
		{
			try
			{
				for(CreativeTabs tab : i.getCreativeTabs())
					i.getSubItems(tab, allSubs);
			} catch(Throwable err)
			{
			}
		});
		
		registry.handleRecipes(LaserTableRecipe.class, LaserTableWrapper::new, APSRepoweredJEI.LASER_TABLE);
		registry.handleRecipes(FluidStack.class, f -> new TokamakFuelWrapper(f.getFluid()), APSRepoweredJEI.TOKAMAK_FUEL);
		registry.handleRecipes(GrinderRecipe.class, GrinderWrapper::new, APSRepoweredJEI.GRINDER);
		registry.handleRecipes(MagmafierRecipe.class, t -> new MagmafierWrapper(t.value), APSRepoweredJEI.MAGMAFIER);
		registry.handleRecipes(MagmaticGeneratorRecipe.class, r -> new MagmaticGeneratorWrapper(r.fluid), APSRepoweredJEI.MAGMATIC_GENERATOR);
		registry.handleRecipes(FuelGeneratorRecipe.class, r -> new FuelGeneratorWrapper(r.stack), APSRepoweredJEI.FUEL_GENERATOR);
		
		registry.addRecipes(RecipesLaserTable.RECIPES, APSRepoweredJEI.LASER_TABLE);
		registry.addRecipeCatalyst(new ItemStack(BlocksAR.LASER_TABLE), APSRepoweredJEI.LASER_TABLE);
		
		if(BlocksAR.TOKAMAK != null)
		{
			registry.addRecipes(TokamakFluidRegistry.TMAP.keySet().stream().map(f -> new FluidStack(f, Fluid.BUCKET_VOLUME)).collect(Collectors.toList()), APSRepoweredJEI.TOKAMAK_FUEL);
			registry.addRecipeCatalyst(new ItemStack(BlocksAR.TOKAMAK), APSRepoweredJEI.TOKAMAK_FUEL);
		}
		
		if(BlocksAR.GRINDER != null)
		{
			registry.addRecipes(GrinderRecipe.GRINDER_RECIPES, APSRepoweredJEI.GRINDER);
			registry.addRecipeCatalyst(new ItemStack(BlocksAR.GRINDER), APSRepoweredJEI.GRINDER);
		}
		
		if(BlocksAR.MAGMAFIER != null)
		{
			registry.addRecipes(MagmafierMatterRegistry.recipes.stream().map(MagmafierRecipe::new).collect(Collectors.toList()), APSRepoweredJEI.MAGMAFIER);
			registry.addRecipeCatalyst(new ItemStack(BlocksAR.MAGMAFIER), APSRepoweredJEI.MAGMAFIER);
		}
		
		registry.addRecipes(FluidRegistry.getRegisteredFluids().values().stream().filter(f -> TileMagmaticGenerator.getBurnTime(new FluidStack(f, Fluid.BUCKET_VOLUME)) > 0).map(MagmaticGeneratorRecipe::new).collect(Collectors.toList()), APSRepoweredJEI.MAGMATIC_GENERATOR);
		registry.addRecipeCatalyst(new ItemStack(BlocksAR.MAGMATIC_GENERATOR), APSRepoweredJEI.MAGMATIC_GENERATOR);
		
		registry.addRecipeCatalyst(new ItemStack(BlocksAR.FUEL_GENERATOR), APSRepoweredJEI.FUEL_GENERATOR);
		registry.addRecipes(allSubs.stream().filter(stack -> TileEntityFurnace.isItemFuel(stack) && FluidUtil.getFluidHandler(stack) == null).map(FuelGeneratorRecipe::new).collect(Collectors.toList()), APSRepoweredJEI.FUEL_GENERATOR);
		
		if(BlocksAR.POWERED_FURNACE != null)
			registry.addRecipeCatalyst(new ItemStack(BlocksAR.POWERED_FURNACE), VanillaRecipeCategoryUid.SMELTING);
		
		registry.addIngredientInfo(new ItemStack(BlocksAR.BURNT_GLASS), ItemStack.class, "jei.desc." + InfoAR.MOD_ID + ".burnt_glass");
		
		registry.addRecipeClickArea(GuiTokamak.class, 33, 40, 22, 15, APSRepoweredJEI.TOKAMAK_FUEL);
		registry.addRecipeClickArea(GuiLaserTable.class, 90, 34, 22, 14, APSRepoweredJEI.LASER_TABLE);
	}
	
	@Override
	public void registerIngredients(IModIngredientRegistration registry)
	{
		APSRepoweredJEI.init();
		
		CustomIngredientManager cim = new CustomIngredientManager();
		registry.register(ICIngr.class, ICIngr.INGREDIENTS, cim, cim);
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registry)
	{
		registry.addRecipeCategories(//
		        new LaserTableCategory(registry.getJeiHelpers()), //
		        new TokamakFuelCategory(registry.getJeiHelpers()), //
		        new GrinderCategory(registry.getJeiHelpers()), //
		        new MagmafierCategory(registry.getJeiHelpers()), //
		        new MagmaticGeneratorCategory(registry.getJeiHelpers()), //
		        new FuelGeneratorCategory(registry.getJeiHelpers()));
	}
	
	public static class MagmafierRecipe
	{
		public final TwoTuple<Ingredient, Integer> value;
		
		public MagmafierRecipe(TwoTuple<Ingredient, Integer> value)
		{
			this.value = value;
		}
	}
	
	public static class MagmaticGeneratorRecipe
	{
		public final Fluid fluid;
		
		public MagmaticGeneratorRecipe(Fluid fluid)
		{
			this.fluid = fluid;
		}
	}
	
	public static class FuelGeneratorRecipe
	{
		public final ItemStack stack;
		
		public FuelGeneratorRecipe(ItemStack stack)
		{
			this.stack = stack;
		}
	}
}