package com.zeitheron.aps.intr.jei.fuelgen;

import java.util.Collections;
import java.util.List;

import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;

public class FuelGeneratorWrapper implements IRecipeWrapper
{
	public final ItemStack stack;
	
	public FuelGeneratorWrapper(ItemStack stack)
	{
		this.stack = stack;
	}
	
	public ItemStack getInput()
	{
		return stack.copy();
	}
	
	int time;
	
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY)
	{
		return Collections.emptyList();
	}
	
	public int getBurnTime()
	{
		if(time > 0)
			return time;
		time = TileEntityFurnace.getItemBurnTime(stack);
		return time;
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInput(ItemStack.class, getInput());
		ingredients.setOutput(ICIngr.class, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}