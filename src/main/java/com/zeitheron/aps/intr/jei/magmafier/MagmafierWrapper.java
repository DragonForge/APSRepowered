package com.zeitheron.aps.intr.jei.magmafier;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

public class MagmafierWrapper implements IRecipeWrapper
{
	public final TwoTuple<Ingredient, Integer> value;
	
	public MagmafierWrapper(TwoTuple<Ingredient, Integer> value)
	{
		this.value = value;
	}
	
	public Ingredient getInput()
	{
		return value.get1();
	}
	
	public FluidStack getOutput()
	{
		return new FluidStack(FluidRegistry.LAVA, 50 * value.get2());
	}
	
	public FluidStack getOutput(int max)
	{
		FluidStack st = getOutput();
		st.amount = Math.min(max, st.amount);
		return st;
	}
	
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY)
	{
		return Collections.emptyList();
	}
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInputs(ItemStack.class, Arrays.asList(getInput().getMatchingStacks()));
		ingredients.setInputs(ICIngr.class, Arrays.asList(APSRepoweredJEI.IG_FORGE_ENERGY, APSRepoweredJEI.IG_MATTER_UNIT));
		ingredients.setOutput(ICIngr.class, APSRepoweredJEI.IG_MATTER_UNIT);
		ingredients.setOutput(FluidStack.class, getOutput());
	}
}