package com.zeitheron.aps.intr.jei.ingredient;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.TextureAtlasSpriteFull;

import mezz.jei.api.ingredients.IIngredientHelper;
import mezz.jei.api.ingredients.IIngredientRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;

public class CustomIngredientManager implements IIngredientHelper<ICIngr>, IIngredientRenderer<ICIngr>
{
	@Override
	public void render(Minecraft minecraft, int xPosition, int yPosition, ICIngr ingredient)
	{
		String[] mod = ingredient.icon().split(":", 2);
		UtilsFX.bindTexture(mod.length == 1 ? InfoAR.MOD_ID : mod[0], mod[mod.length - 1]);
		GlStateManager.enableBlend();
		RenderUtil.drawTexturedModalRect(xPosition, yPosition, TextureAtlasSpriteFull.sprite, 16, 16);
	}
	
	@Override
	public ItemStack getCheatItemStack(ICIngr ingredient)
	{
		return ingredient.cheat();
	}
	
	@Override
	public FontRenderer getFontRenderer(Minecraft minecraft, ICIngr ingredient)
	{
		return minecraft.fontRenderer;
	}
	
	@Override
	public List<String> getTooltip(Minecraft minecraft, ICIngr ingredient, ITooltipFlag tooltipFlag)
	{
		return Arrays.asList(I18n.format("ingredient." + InfoAR.MOD_ID + ":" + ingredient.id() + ".desc").split("<br>"));
	}
	
	@Override
	public List<ICIngr> expandSubtypes(List<ICIngr> ingredients)
	{
		return ingredients;
	}
	
	@Override
	public ICIngr getMatch(Iterable<ICIngr> ingredients, ICIngr ingredientToMatch)
	{
		List<ICIngr> li = new ArrayList<>();
		ingredients.forEach(li::add);
		return li.contains(ingredientToMatch) ? ingredientToMatch : null;
	}
	
	@Override
	public String getDisplayName(ICIngr ingredient)
	{
		return I18n.format("ingredient." + InfoAR.MOD_ID + ":" + ingredient.id() + ".name");
	}
	
	@Override
	public String getUniqueId(ICIngr ingredient)
	{
		return ingredient.id();
	}
	
	@Override
	public String getWildcardId(ICIngr ingredient)
	{
		return "/";
	}
	
	@Override
	public String getModId(ICIngr ingredient)
	{
		return ingredient.modid();
	}
	
	@Override
	public Iterable<Color> getColors(ICIngr ingredient)
	{
		return Arrays.asList(Color.WHITE);
	}
	
	@Override
	public String getResourceId(ICIngr ingredient)
	{
		return getUniqueId(ingredient);
	}
	
	@Override
	public ICIngr copyIngredient(ICIngr ingredient)
	{
		return ingredient;
	}
	
	@Override
	public String getErrorInfo(ICIngr ingredient)
	{
		return "";
	}
}