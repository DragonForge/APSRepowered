package com.zeitheron.aps.intr.jei.magmafier;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.cfg.MachineConfig;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.ingredient.RenderEnergyBar;
import com.zeitheron.aps.intr.jei.ingredient.RenderMatterBar;
import com.zeitheron.hammercore.client.gui.GuiTankTexture;
import com.zeitheron.hammercore.compat.jei.TankOverlayDrawable;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.*;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;

import java.util.Arrays;

public class MagmafierCategory implements IRecipeCategory<MagmafierWrapper>
{
	IDrawable draw;
	
	public MagmafierCategory(IJeiHelpers helper)
	{
		draw = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/jei_magmafier_fuel.png"), 0, 0, 84, 64);
	}
	
	@Override
	public String getUid()
	{
		return APSRepoweredJEI.MAGMAFIER;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format("jei." + getUid());
	}
	
	@Override
	public String getModName()
	{
		return InfoAR.MOD_NAME;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return draw;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, MagmafierWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup items = recipeLayout.getItemStacks();
		IGuiFluidStackGroup fluids = recipeLayout.getFluidStacks();
		IGuiIngredientGroup<ICIngr> custom = recipeLayout.getIngredientsGroup(ICIngr.class);
		
		items.init(0, true, 61, 22);
		items.set(0, Arrays.asList(recipeWrapper.getInput().getMatchingStacks()));
		
		fluids.init(1, false, 3, 3, 16, 58, Fluid.BUCKET_VOLUME * 10, false, new TankOverlayDrawable(new GuiTankTexture(14, 58)));
		fluids.set(1, recipeWrapper.getOutput(Fluid.BUCKET_VOLUME * 10));
		
		int maxEnergy = recipeWrapper.value.get2() * MachineConfig.MagmafierEnergyPerOperation;
		
		custom.init(1, true, new RenderEnergyBar(8, 58, maxEnergy, 0), 27, 3, 8, 58, 0, 0);
		custom.set(1, APSRepoweredJEI.IG_FORGE_ENERGY);
		
		custom.init(2, true, new RenderMatterBar(8, 58, recipeWrapper.value.get2(), 0), 43, 3, 8, 58, 0, 0);
		custom.set(2, APSRepoweredJEI.IG_MATTER_UNIT);
	}
}