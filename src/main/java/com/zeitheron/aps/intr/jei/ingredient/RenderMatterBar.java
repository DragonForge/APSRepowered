package com.zeitheron.aps.intr.jei.ingredient;

import java.util.List;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;

import mezz.jei.api.ingredients.IIngredientRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;

public class RenderMatterBar implements IIngredientRenderer<ICIngr>
{
	public final int w, h, fe, mfe;
	
	public RenderMatterBar(int w, int h, int energy, int maxEnergy)
	{
		this.w = w;
		this.h = h;
		this.fe = energy;
		this.mfe = maxEnergy;
	}
	
	@Override
	public void render(Minecraft minecraft, int xPosition, int yPosition, ICIngr ingredient)
	{
		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/gui/widgets.png");
		RenderUtil.drawTexturedModalRect(xPosition, yPosition, 0, 0, w, h);
	}
	
	@Override
	public List<String> getTooltip(Minecraft minecraft, ICIngr ingredient, ITooltipFlag tooltipFlag)
	{
		return TooltipHelper.generate("matter", fe, mfe, "mu");
	}
}