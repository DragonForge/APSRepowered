package com.zeitheron.aps.intr.jei.grinder;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.api.GrinderRecipe;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag.TooltipFlags;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GrinderWrapper
		implements IRecipeWrapper
{
	public final GrinderRecipe recipe;

	public GrinderWrapper(GrinderRecipe recipe)
	{
		this.recipe = recipe;
	}

	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInputs(ItemStack.class, Arrays.asList(recipe.getIngredient().getMatchingStacks()));
		ingredients.setInput(ICIngr.class, APSRepoweredJEI.IG_FORGE_ENERGY);

		ItemStack[] os = recipe.getAllOutputs();
		for(int i = 0; i < os.length; ++i)
			os[i] = os[i].copy();
		ingredients.setOutputs(ItemStack.class, Arrays.asList(os));
	}

	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
		int bx = 37;
		int by = 2;

		FontRenderer fr = minecraft.fontRenderer;

		String txt = I18n.format("gui." + InfoAR.MOD_ID + ".grinder_traces") + ":";
		fr.drawString(txt, bx, by, 0x404040, false);

		ItemStack[] outs = recipe.getAllOutputs();
		int hover = getHoveredItem(mouseX, mouseY);

		for(int i = 0; i < outs.length; ++i)
		{
			by += fr.FONT_HEIGHT + 1;
			ItemStack out = outs[i];
			String sub = out.getCount() + "x";
			String s = sub + "   (" + recipe.chances[i] + "%)";
			fr.drawString(s, bx, by, 0x404040, false);

			GlStateManager.pushMatrix();
			RenderHelper.enableGUIStandardItemLighting();
			int x = bx + fr.getStringWidth(sub) + 2, y = by;
			GlStateManager.translate(x, y, 0);
			GlStateManager.scale(.5F, .5F, 1);
			minecraft.getRenderItem().renderItemIntoGUI(out, 0, 0);
			GlStateManager.popMatrix();

			if(mouseX >= x && mouseY >= y && mouseX < x + 8 && mouseY < y + 8)
			{
				GL11.glColor4f(1, 1, 1, 1);
				RenderUtil.drawGradientRect(x, y, 8, 8, 0x99FFFFFF, 0x99FFFFFF, 200);
			}

			GL11.glColor4f(1, 1, 1, 1);
		}

		GL11.glColor4f(1, 1, 1, 1);
	}

	List<String> tooltip = new ArrayList<>();

	public int getHoveredItem(int mouseX, int mouseY)
	{
		int bx = 37;
		int by = 2;
		FontRenderer fr = Minecraft.getMinecraft().fontRenderer;
		ItemStack[] outs = recipe.getAllOutputs();
		for(int i = 0; i < outs.length; ++i)
		{
			by += fr.FONT_HEIGHT + 1;
			ItemStack out = outs[i];
			String sub = out.getCount() + "x";
			int x = bx + fr.getStringWidth(sub) + 2, y = by;
			if(mouseX >= x && mouseY >= y && mouseX < x + 8 && mouseY < y + 8)
				return i;
		}
		return -1;
	}

	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY)
	{
		tooltip.clear();

		int hover = getHoveredItem(mouseX, mouseY);

		if(hover != -1)
		{
			ItemStack hs = recipe.getAllOutputs()[hover];
			if(hs.isEmpty())
				return tooltip;
			tooltip.addAll(hs.getTooltip(Minecraft.getMinecraft().player, Minecraft.getMinecraft().gameSettings.advancedItemTooltips ? TooltipFlags.ADVANCED : TooltipFlags.NORMAL));
			for(int i = 1; i < tooltip.size(); ++i)
				tooltip.set(i, TextFormatting.GRAY + tooltip.get(i));
			ModContainer mc = Loader.instance().getIndexedModList().get(hs.getItem().getRegistryName().getNamespace());
			tooltip.add(TextFormatting.BLUE.toString() + TextFormatting.ITALIC + (mc != null ? mc.getName() : "???"));
			return tooltip;
		}

		return tooltip;
	}

	@Override
	public boolean handleClick(Minecraft minecraft, int mouseX, int mouseY, int mouseButton)
	{
		int hover = getHoveredItem(mouseX, mouseY);

		if(hover != -1)
		{
			ItemStack hs = recipe.getAllOutputs()[hover];
			if(mouseButton == 0)
			{
				IJeiHelper.Instance.getJEIModifier().showRecipes(hs);
				return true;
			} else if(mouseButton == 1)
			{
				IJeiHelper.Instance.getJEIModifier().showUses(hs);
				return true;
			}
		}

		return false;
	}
}