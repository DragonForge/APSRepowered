package com.zeitheron.aps.intr.jei.grinder;

import java.util.Arrays;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;
import com.zeitheron.aps.intr.jei.ingredient.RenderEnergyBar;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiIngredientGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;

public class GrinderCategory implements IRecipeCategory<GrinderWrapper>
{
	IDrawable draw;
	
	public GrinderCategory(IJeiHelpers helper)
	{
		draw = helper.getGuiHelper().createDrawable(new ResourceLocation(InfoAR.MOD_ID, "textures/gui/jei_grinder.png"), 0, 0, 150, 70);
	}
	
	@Override
	public String getUid()
	{
		return APSRepoweredJEI.GRINDER;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format("jei." + getUid());
	}
	
	@Override
	public String getModName()
	{
		return InfoAR.MOD_NAME;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return draw;
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, GrinderWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup i = recipeLayout.getItemStacks();
		IGuiIngredientGroup<ICIngr> custom = recipeLayout.getIngredientsGroup(ICIngr.class);
		
		i.init(0, true, 2, 26);
		i.set(0, Arrays.asList(recipeWrapper.recipe.getIngredient().getMatchingStacks()));
		
		custom.init(2, true, new RenderEnergyBar(8, 64, recipeWrapper.recipe.energy, 0), 25, 3, 8, 64, 0, 0);
		custom.set(2, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
}