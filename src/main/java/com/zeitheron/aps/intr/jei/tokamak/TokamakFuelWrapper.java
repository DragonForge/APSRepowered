package com.zeitheron.aps.intr.jei.tokamak;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

public class TokamakFuelWrapper implements IRecipeWrapper
{
	public final Fluid fuel;
	
	public TokamakFuelWrapper(Fluid fuel)
	{
		this.fuel = fuel;
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInput(FluidStack.class, new FluidStack(fuel, Fluid.BUCKET_VOLUME));
		ingredients.setInput(ICIngr.class, APSRepoweredJEI.IG_FORGE_ENERGY);
		ingredients.setOutput(ICIngr.class, APSRepoweredJEI.IG_FORGE_ENERGY);
	}
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
		int bx = 44;
		int by = 5;
		
		String txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.4");
		minecraft.fontRenderer.drawString(txt, bx + 12, by, 0x404040, false);
		
		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.6");
		minecraft.fontRenderer.drawString(txt, bx, by + 48, 0x404040, false);
		
		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.5");
		minecraft.fontRenderer.drawString(txt, bx, by + 14, 0x404040, false);
	}
}