package com.zeitheron.aps.intr.jei;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.intr.jei.ingredient.ICIngr;

public class APSRepoweredJEI
{
	public static final String LASER_TABLE = InfoAR.MOD_ID + ".laser_table";
	public static final String TOKAMAK_FUEL = InfoAR.MOD_ID + ".tokamak_fuel";
	public static final String GRINDER = InfoAR.MOD_ID + ".grinder";
	public static final String MAGMAFIER = InfoAR.MOD_ID + ".magmafier";
	public static final String MAGMATIC_GENERATOR = InfoAR.MOD_ID + ".magmatic_generator";
	public static final String FUEL_GENERATOR = InfoAR.MOD_ID + ".fuel_generator";
	
	public static final ICIngr IG_FORGE_ENERGY = () -> "fe";
	public static final ICIngr IG_MATTER_UNIT = () -> "mu";
	
	static
	{
		IG_FORGE_ENERGY.implement();
		IG_MATTER_UNIT.implement();
	}

	public static void init()
	{
	}
}