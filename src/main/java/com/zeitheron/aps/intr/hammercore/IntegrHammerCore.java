package com.zeitheron.aps.intr.hammercore;

import com.zeitheron.aps.pipes.IPipe;
import com.zeitheron.hammercore.api.mhb.BlockTraceable;
import com.zeitheron.hammercore.api.mhb.ICubeManager;
import com.zeitheron.hammercore.api.mhb.IRayCubeRegistry;
import com.zeitheron.hammercore.api.mhb.IRayRegistry;
import com.zeitheron.hammercore.api.mhb.RaytracePlugin;

import net.minecraft.block.Block;
import net.minecraftforge.fml.common.registry.GameRegistry;

@RaytracePlugin
public class IntegrHammerCore implements IRayRegistry
{
	IRayCubeRegistry cubeRegistry;
	
	@Override
	public void registerCubes(IRayCubeRegistry cube)
	{
		cubeRegistry = cube;
		GameRegistry.findRegistry(Block.class).getValuesCollection().forEach(this::register);
		cubeRegistry = null;
	}
	
	private void register(Block bl)
	{
		if(bl instanceof BlockTraceable && bl instanceof ICubeManager && bl instanceof IPipe)
			cubeRegistry.bindBlockCubeManager((BlockTraceable) bl, (ICubeManager) bl);
	}
}