package com.zeitheron.aps.client;

import net.minecraft.client.renderer.texture.TextureAtlasSprite;

public class TextureAtlasSpriteUV extends TextureAtlasSprite
{
	public TextureAtlasSpriteUV(int x, int y, int w, int h, int texSizeX, int texSizeY)
	{
		super("uv");
		width = texSizeX;
		height = texSizeY;
		initSprite(w, h, x, y, false);
	}
}