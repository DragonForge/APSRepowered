package com.zeitheron.aps.client.tesr;

import org.lwjgl.opengl.GL11;

import com.zeitheron.aps.blocks.tiles.TileLaserTable;
import com.zeitheron.aps.client.models.ModelLaserHead;
import com.zeitheron.aps.client.models.ModelLaserTableBase;
import com.zeitheron.aps.util.RectangleAnimation2D;
import com.zeitheron.aps.util.LyingItems.LocatableStack;
import com.zeitheron.hammercore.client.render.tesr.TESR;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.util.ResourceLocation;

public class TESRLaserTable extends TESR<TileLaserTable>
{
	public ModelLaserTableBase modelBase = new ModelLaserTableBase();
	public ModelLaserHead modelHead = new ModelLaserHead();
	
	@Override
	public void renderTileEntityAt(TileLaserTable te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		RenderHelper.disableStandardItemLighting();
		GL11.glPushMatrix();
		GL11.glTranslated(x + .5, y + 1.5, z + .5);
		GL11.glScalef(1 / 16F, 1 / 16F, 1 / 16F);
		GL11.glRotatef(180F, 0F, 0F, 1F);
		bindTexture(ModelLaserTableBase.texture);
		modelBase.render(null, 0F, 0F, -.1F, 0F, 0F, 1F);
		
		RectangleAnimation2D r = te.laserHeadPos;
		
		float ox = r.getInterpX(partialTicks) - r.bounds.x;
		float oy = r.getInterpY(partialTicks) - r.bounds.y;
		
		GL11.glTranslated(4 - ox, -6, -5.5 + oy);
		GL11.glScaled(.75, 1, .75);
		bindTexture(ModelLaserHead.texture);
		modelHead.render(null, 0F, 0F, -.1F, 0F, 0F, 1F);
		GL11.glPopMatrix();
		
		RenderItem ri = Minecraft.getMinecraft().getRenderItem();
		
		GL11.glPushMatrix();
		GL11.glTranslated(x, y + 1, z);
		GL11.glRotated(90, 1, 0, 0);
		int i = 0;
		for(LocatableStack l : te.renderable.merging)
		{
			GL11.glPushMatrix();
			
			GL11.glTranslatef(.25F + l.getX(partialTicks) * .5F, .25F + l.getY(partialTicks) * .5F, i / -1000F);
			
			GL11.glRotated(l.prrY + (l.rY - l.prrY) * partialTicks, 0, 0, 1);
			
			GL11.glScaled(.5, .5, .5);
			ri.renderItem(l.stack, TransformType.GROUND);
			GL11.glPopMatrix();
			++i;
		}
		i += 30;
		LocatableStack l = te.renderable.mergeTar;
		if(l != null)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(.25F + l.getX(partialTicks) * .5F, .25F + l.getY(partialTicks) * .5F, i / -1000F);
			float s = l.getScale(partialTicks);
			GL11.glScaled(s, s, s);
			GL11.glScaled(.5, .5, .5);
			ri.renderItem(l.stack, TransformType.GROUND);
			GL11.glPopMatrix();
		}
		GL11.glPopMatrix();
		
		if(te.crafting)
		{
			GL11.glPushMatrix();
			GlStateManager.disableLighting();
			GL11.glTranslated(x - 11.6 / 16 + ox / 16, y + 1, z + 4.4 / 16 + oy / 16);
			GlStateManager.disableTexture2D();
			GL11.glColor4f(1, 0, 0, 1);
			GL11.glBegin(GL11.GL_LINE_STRIP);
			{
				GL11.glVertex3f(1, 0, 0);
				GL11.glVertex3f(1, 6 / 16F, 0);
			}
			GL11.glEnd();
			GL11.glColor4f(1, 1, 1, 1);
			GlStateManager.enableTexture2D();
			GL11.glPopMatrix();
			GlStateManager.enableLighting();
		}
	}
}