package com.zeitheron.aps.client.tesr;

import org.lwjgl.opengl.GL11;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileFluidPump;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.RenderBlocks;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidTank;

public class TESRFluidPump extends TESR<TileFluidPump>
{
	@Override
	public void renderTileEntityAt(TileFluidPump te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		Fluid render = te.lastPumpedFluid;
		FluidTank tank = te.internalTank;
		if(render == null)
			render = tank.getFluid() != null ? tank.getFluid().getFluid() : FluidRegistry.WATER;
		TextureAtlasSprite sprite = mc.getTextureMapBlocks().getAtlasSprite(render.getStill().toString());
		
		GL11.glColor3f(1F, 1F, 1F);
		GlStateManager.enableAlpha();
		GlStateManager.disableBlend();
		GL11.glDisable(2884);
		GL11.glDisable(GL11.GL_LIGHTING);
		
		mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		
		RenderBlocks rb = RenderBlocks.forMod(InfoAR.MOD_ID);
		int br = getBrightnessForRB(te, rb);
		
		rb.setRenderBounds(.01, .01, .01, .99, .99, .99);
		
		Tessellator.getInstance().getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
		
		rb.renderFace(te.getFront(), x, y, z, sprite, 1, 1, 1, br);
		
		Tessellator.getInstance().draw();
		GL11.glEnable(GL11.GL_LIGHTING);
	}
	
	@Override
	public void renderItem(ItemStack item)
	{
		TextureAtlasSprite sprite = mc.getTextureMapBlocks().getAtlasSprite(FluidRegistry.WATER.getStill().toString());
		
		mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		
		RenderBlocks rb = RenderBlocks.forMod(InfoAR.MOD_ID);
		int br = getBrightnessForRB(null, rb);
		
		GL11.glColor3f(1F, 1F, 1F);
		
		rb.setRenderBounds(.01, .01, .01, .99, .99, .99);
		
		Tessellator.getInstance().getBuffer().begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX_LMAP_COLOR);
		
		rb.renderFace(EnumFacing.NORTH, 0, 0, 0, sprite, 1, 1, 1, br);
		
		GlStateManager.disableLighting();
		GlStateManager.enableAlpha();
		GlStateManager.disableBlend();
		Tessellator.getInstance().draw();
		GlStateManager.enableBlend();
		GlStateManager.enableLighting();
	}
}