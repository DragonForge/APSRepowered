package com.zeitheron.aps.client.tesr;

import org.lwjgl.opengl.GL11;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileSolarCollector;
import com.zeitheron.aps.blocks.tiles.TileSolarReflector;
import com.zeitheron.hammercore.annotations.AtTESR;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.client.utils.UtilsFX;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;

@AtTESR(TileSolarCollector.class)
public class TESRSolarCollector extends TESR<TileSolarCollector>
{
	@Override
	public void renderTileEntityAt(TileSolarCollector te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/models/solar_laser.png");
		GlStateManager.pushMatrix();
		
		GlStateManager.disableLighting();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 255, 15);
		
		GlStateManager.color(1, 1, 1, alpha);
		GlStateManager.translate(x + .5, y + .5, z + .5);
		// GlStateManager.disableTexture2D();
		float time = (te.ticksExisted + partialTicks) % 160 / 160F;
		if(te.Manager != null)
			for(int i = 0; i < te.Manager.Reflectors.size(); ++i)
			{
				TileSolarReflector ref = te.Manager.Reflectors.get(i);
				if(ref != null && ref.beamFocus != null)
				{
					double srcX = te.getPos().getX();
					double srcY = te.getPos().getY();
					double srcZ = te.getPos().getZ();
					
					double dstX = ref.getPos().getX();
					double dstY = ref.getPos().getY() + .45;
					double dstZ = ref.getPos().getZ();
					
					double dist = Math.sqrt(ref.getPos().distanceSq(te.getPos()));
					
					GL11.glBegin(GL11.GL_LINE_STRIP);
					{
						GL11.glVertex3d(dstX - srcX, dstY - srcY, dstZ - srcZ);
						GL11.glTexCoord2f(time, 0);
						
						GL11.glVertex3d(0, .49, 0);
						GL11.glTexCoord2f(time + (float) Math.sqrt(dist), 0);
					}
					GL11.glEnd();
				}
			}
		// GlStateManager.enableTexture2D();
		
		GlStateManager.enableLighting();
		GlStateManager.popMatrix();
	}
}