package com.zeitheron.aps.client.models;

import com.zeitheron.aps.InfoAR;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

/**
 * Laser Head - Borindeus
 */
public class ModelLaserHead extends ModelBase
{
	public static final ResourceLocation texture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/laser_head.png");
	
	public ModelRenderer shape1;
	public ModelRenderer shape2;
	public ModelRenderer shape2_1;
	public ModelRenderer shape5;
	public ModelRenderer shape5_1;
	public ModelRenderer shape7;
	
	public ModelLaserHead()
	{
		this.textureWidth = 64;
		this.textureHeight = 32;
		this.shape5_1 = new ModelRenderer(this, 31, 0);
		this.shape5_1.setRotationPoint(0.0F, 4.0F, 1.0F);
		this.shape5_1.addBox(0.0F, 0.0F, 0.0F, 3, 2, 1, 0.0F);
		this.setRotateAngle(shape5_1, 0.0F, -1.5707963267948966F, 0.0F);
		this.shape2 = new ModelRenderer(this, 20, 0);
		this.shape2.setRotationPoint(-2.0F, 1.0F, 0.0F);
		this.shape2.addBox(0.0F, 0.0F, 0.0F, 3, 3, 5, 0.0F);
		this.shape5 = new ModelRenderer(this, 15, 0);
		this.shape5.setRotationPoint(-2.0F, 4.0F, 2.0F);
		this.shape5.addBox(0.0F, 0.0F, 0.0F, 3, 2, 1, 0.0F);
		this.shape1 = new ModelRenderer(this, 0, 0);
		this.shape1.setRotationPoint(-3.0F, 0.0F, 0.0F);
		this.shape1.addBox(0.0F, 0.0F, 0.0F, 5, 1, 5, 0.0F);
		this.shape2_1 = new ModelRenderer(this, 36, 0);
		this.shape2_1.setRotationPoint(-3.0F, 1.0F, 1.0F);
		this.shape2_1.addBox(0.0F, 0.0F, 0.0F, 5, 3, 3, 0.0F);
		this.shape7 = new ModelRenderer(this, 0, 0);
		this.shape7.setRotationPoint(-1.0F, 6.0F, 2.0F);
		this.shape7.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1, 0.0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		this.shape5_1.render(f5);
		this.shape2.render(f5);
		this.shape5.render(f5);
		this.shape1.render(f5);
		this.shape2_1.render(f5);
		this.shape7.render(f5);
	}
	
	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}