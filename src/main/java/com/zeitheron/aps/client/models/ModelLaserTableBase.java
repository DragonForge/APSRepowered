package com.zeitheron.aps.client.models;

import com.zeitheron.aps.InfoAR;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

/**
 * Laser Table - Borindeus
 */
public class ModelLaserTableBase extends ModelBase
{
	public static final ResourceLocation texture = new ResourceLocation(InfoAR.MOD_ID, "textures/models/laser_table.png");
	
	public ModelRenderer shape2;
	public ModelRenderer shape5;
	public ModelRenderer shape6;
	public ModelRenderer shape7;
	public ModelRenderer shape8;
	
	public ModelLaserTableBase()
	{
		this.textureWidth = 128;
		this.textureHeight = 64;

		this.shape6 = new ModelRenderer(this, 80, 0);
		this.shape6.setRotationPoint(-8.0F, -7.0F, -8.0F);
		this.shape6.addBox(0.0F, 1.0F, 0.0F, 2, 14, 2, 0.0F);

		this.shape5 = new ModelRenderer(this, 72, 0);
		this.shape5.setRotationPoint(-8.0F, -7.0F, 6.0F);
		this.shape5.addBox(0.0F, 1.0F, 0.0F, 2, 14, 2, 0.0F);

		this.shape7 = new ModelRenderer(this, 88, 0);
		this.shape7.setRotationPoint(6.0F, -7.0F, -8.0F);
		this.shape7.addBox(0.0F, 1.0F, 0.0F, 2, 14, 2, 0.0F);

		this.shape8 = new ModelRenderer(this, 48, 17);
		this.shape8.setRotationPoint(-8.0F, -7.0F, -8.0F);
		this.shape8.addBox(0.0F, 0.0F, 0.0F, 16, 1, 16, 0.0F);

		this.shape2 = new ModelRenderer(this, 64, 0);
		this.shape2.setRotationPoint(6.0F, -7.0F, 6.0F);
		this.shape2.addBox(0.0F, 1.0F, 0.0F, 2, 14, 2, 0.0F);
	}
	
	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale)
	{
		this.shape6.render(scale);
		this.shape5.render(scale);
		this.shape7.render(scale);
		this.shape2.render(scale);
		this.shape8.render(scale);
	}
	
	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z)
	{
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}