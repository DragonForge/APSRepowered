package com.zeitheron.aps.client.models;

import com.google.common.base.Predicates;
import com.zeitheron.aps.pipes.IPipe;
import com.zeitheron.hammercore.utils.PositionedStateImplementation;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.*;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.util.vector.Vector3f;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class BakedPipeModel<T extends Block & IPipe>
		implements IBakedModel
{
	public static final FaceBakery $ = new FaceBakery();

	T pipe;
	boolean item;

	public BakedPipeModel(T pipe, boolean b)
	{
		this.pipe = pipe;
		this.item = b;
	}

	@Override
	public List<BakedQuad> getQuads(@Nullable IBlockState state, @Nullable EnumFacing side, long rand)
	{
		try
		{
			s:
			if(!item && side != null)
			{
				PositionedStateImplementation extendedState = Cast.cast(state, PositionedStateImplementation.class);
				if(extendedState == null) break s;
				IBlockAccess world = extendedState.getWorld();
				BlockPos pos = extendedState.getPos();
				IPipe pipe = IPipe.get(world, pos);
				Predicate<EnumFacing> ctp = pipe == null ? Predicates.alwaysFalse() : pipe::isConnectedTo;
				List<BakedQuad> quads = new ArrayList<>();
				TextureAtlasSprite tx = getParticleTexture();
				if(pipe != null)
					pipe.generateBakedQuads(quads, side, rand, world, pos, extendedState, ctp, tx);
				else
					generateQuads(quads, side, rand, world, pos, extendedState, ctp, tx);
				return quads;
			}

			List<BakedQuad> quads = new ArrayList<>();
			TextureAtlasSprite tx = getParticleTexture();

			if(item && side != null)
			{
				{
					quads.add($.makeBakedQuad(
							new Vector3f(5, 5, 5), new Vector3f(11, 11, 11),
							new BlockPartFace(side, 0, "0",
									new BlockFaceUV(new float[]
											{
													0,
													0,
													6,
													6
											}, 0)),
							tx,
							side,
							ModelRotation.X0_Y0,
							null,
							true,
							true));

					quads.add($.makeBakedQuad(
							new Vector3f(5.5F, 0, 5.5F), new Vector3f(10.5F, 5, 10.5F),
							new BlockPartFace(side, 0, "0",
									new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
											{
													6,
													6,
													11,
													11
											} : new float[]
											{
													0,
													6,
													5,
													11
											}, 0)),
							tx,
							side,
							ModelRotation.X0_Y0,
							null,
							true,
							true));

					quads.add($.makeBakedQuad(
							new Vector3f(5.5F, 11, 5.5F), new Vector3f(10.5F, 16, 10.5F),
							new BlockPartFace(side, 0, "0",
									new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
											{
													6,
													6,
													11,
													11
											} : new float[]
											{
													0,
													6,
													5,
													11
											}, 0)),
							tx,
							side,
							ModelRotation.X0_Y0,
							null,
							true,
							true));
				}
			}

			return quads;
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		return Collections.emptyList();
	}

	public static void generateQuads(List<BakedQuad> quads, EnumFacing side, long rand, IBlockAccess world, BlockPos pos, IBlockState state, Predicate<EnumFacing> ctp, TextureAtlasSprite tx)
	{
		List<EnumFacing> connected = new ArrayList<>();
		for(EnumFacing f : EnumFacing.VALUES)
			if(ctp.test(f))
				connected.add(f);

		if(connected.size() == 2 && connected.get(0).getOpposite() == connected.get(1) && world.getBlockState(pos.offset(connected.get(0))).getBlock() == state.getBlock() && world.getBlockState(pos.offset(connected.get(1))).getBlock() == state.getBlock())
		{
			EnumFacing.Axis axial = connected.get(0).getAxis();

			if(axial == EnumFacing.Axis.X && side.getAxis() != EnumFacing.Axis.X)
				quads.add($.makeBakedQuad(
						new Vector3f(5, 5.5F, 5.5F), new Vector3f(11, 10.5F, 10.5F),
						new BlockPartFace(side, 0, "0",
								new BlockFaceUV(new float[]
										{
												6,
												0,
												12,
												5
										}, 0)),
						tx,
						side,
						ModelRotation.X0_Y0,
						null,
						true,
						true));

			if(axial == EnumFacing.Axis.Y && side.getAxis() != EnumFacing.Axis.Y)
				quads.add($.makeBakedQuad(
						new Vector3f(5.5F, 5, 5.5F), new Vector3f(10.5F, 11, 10.5F),
						new BlockPartFace(side, 0, "0",
								new BlockFaceUV(new float[]
										{
												0,
												6,
												5,
												12
										}, 0)),
						tx,
						side,
						ModelRotation.X0_Y0,
						null,
						true,
						true));

			if(axial == EnumFacing.Axis.Z && side.getAxis() != EnumFacing.Axis.Z)
				quads.add($.makeBakedQuad(
						new Vector3f(5.5F, 5.5F, 5), new Vector3f(10.5F, 10.5F, 11),
						new BlockPartFace(side, 0, "0",
								new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
										{
												0,
												6,
												5,
												12
										} : new float[]
										{
												6,
												0,
												12,
												5
										}, 0)),
						tx,
						side,
						ModelRotation.X0_Y0,
						null,
						true,
						true));
		} else
			quads.add($.makeBakedQuad(
					new Vector3f(5, 5, 5), new Vector3f(11, 11, 11),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											0,
											0,
											6,
											6
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));

		if(connected.contains(EnumFacing.WEST) && side.getAxis() != EnumFacing.Axis.X)
			quads.add($.makeBakedQuad(
					new Vector3f(0, 5.5F, 5.5F), new Vector3f(5, 10.5F, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));

		if(connected.contains(EnumFacing.EAST) && side.getAxis() != EnumFacing.Axis.X)
			quads.add($.makeBakedQuad(
					new Vector3f(11, 5.5F, 5.5F), new Vector3f(16, 10.5F, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));

		if(connected.contains(EnumFacing.DOWN) && side.getAxis() != EnumFacing.Axis.Y)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 0, 5.5F), new Vector3f(10.5F, 5, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											0,
											6,
											5,
											11
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));

		if(connected.contains(EnumFacing.UP) && side.getAxis() != EnumFacing.Axis.Y)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 11, 5.5F), new Vector3f(10.5F, 16, 10.5F),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(new float[]
									{
											0,
											6,
											5,
											11
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));

		if(connected.contains(EnumFacing.NORTH) && side.getAxis() != EnumFacing.Axis.Z)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 5.5F, 0), new Vector3f(10.5F, 10.5F, 5),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
									{
											0,
											6,
											5,
											11
									} : new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));

		if(connected.contains(EnumFacing.SOUTH) && side.getAxis() != EnumFacing.Axis.Z)
			quads.add($.makeBakedQuad(
					new Vector3f(5.5F, 5.5F, 11), new Vector3f(10.5F, 10.5F, 16),
					new BlockPartFace(side, 0, "0",
							new BlockFaceUV(side.getAxis() == EnumFacing.Axis.Y ? new float[]
									{
											0,
											6,
											5,
											11
									} : new float[]
									{
											6,
											0,
											11,
											5
									}, 0)),
					tx,
					side,
					ModelRotation.X0_Y0,
					null,
					true,
					true));
	}

	@Override
	public boolean isAmbientOcclusion()
	{
		return false;
	}

	@Override
	public boolean isGui3d()
	{
		return false;
	}

	@Override
	public boolean isBuiltInRenderer()
	{
		return false;
	}

	@Override
	public TextureAtlasSprite getParticleTexture()
	{
		return Minecraft.getMinecraft().getTextureMapBlocks().getAtlasSprite(pipe.getTex().toString());
	}

	@Override
	public ItemOverrideList getOverrides()
	{
		return ItemOverrideList.NONE;
	}

	@Override
	public ItemCameraTransforms getItemCameraTransforms()
	{
		return new ItemCameraTransforms(
				getTransform(ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND),
				getTransform(ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND),
				getTransform(ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND),
				getTransform(ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND),
				getTransform(ItemCameraTransforms.TransformType.HEAD),
				getTransform(ItemCameraTransforms.TransformType.GUI),
				getTransform(ItemCameraTransforms.TransformType.GROUND),
				getTransform(ItemCameraTransforms.TransformType.FIXED));
	}

	public ItemTransformVec3f getTransform(ItemCameraTransforms.TransformType type)
	{
		switch(type)
		{
			case GUI:
				return new ItemTransformVec3f(new Vector3f(30, 45, 0), new Vector3f(0, 0, 0), new Vector3f(0.625F, 0.625F, 0.625F));
			case GROUND:
				return new ItemTransformVec3f(new Vector3f(0, 0, 0), new Vector3f(0, 3, 0), new Vector3f(0.25F, 0.25F, 0.25F));
			case FIXED:
				return new ItemTransformVec3f(new Vector3f(0, 0, 0), new Vector3f(0, 3, 0), new Vector3f(0.5F, 0.5F, 0.5F));
			case THIRD_PERSON_RIGHT_HAND:
				return new ItemTransformVec3f(new Vector3f(75, 45, 0), new Vector3f(0, 0F, 0), new Vector3f(0.375F, 0.375F, 0.375F));
			case THIRD_PERSON_LEFT_HAND:
				return new ItemTransformVec3f(new Vector3f(75, 45, 0), new Vector3f(0, 0F, 0), new Vector3f(0.375F, 0.375F, 0.375F));
			case FIRST_PERSON_RIGHT_HAND:
				return new ItemTransformVec3f(new Vector3f(0, 45, 0), new Vector3f(0, 0, 0), new Vector3f(0.4F, 0.4F, 0.4F));
			case FIRST_PERSON_LEFT_HAND:
				return new ItemTransformVec3f(new Vector3f(0, 225, 0), new Vector3f(0, 0, 0), new Vector3f(0.4F, 0.4F, 0.4F));
			default:
				break;
		}
		return ItemTransformVec3f.DEFAULT;
	}
}