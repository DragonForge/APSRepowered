package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileTokamak;
import com.zeitheron.aps.cfg.TokamakConfig;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerTokamak;
import com.zeitheron.aps.util.DecimalAnimation;
import com.zeitheron.hammercore.client.gui.GuiFluidTank;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class GuiTokamak
		extends GuiWTFMojang<ContainerTokamak>
{
	int mouseX, mouseY;

	public static final ResourceLocation tex = new ResourceLocation(InfoAR.MOD_ID, "textures/gui/tokamak.png");
	private TileTokamak tokamak;

	public GuiFluidTank tank;

	public DecimalAnimation quant = new DecimalAnimation(10F, false);

	public GuiTokamak(EntityPlayer player, TileTokamak tokamak)
	{
		super(new ContainerTokamak(player, tokamak));
		this.tokamak = tokamak;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;

		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);
		String s1 = String.format("%,d", (int) tokamak.PowerIn);
		fontRenderer.drawString(s1, 160 - fontRenderer.getStringWidth(s1), 44, text);
		String s2 = String.format("%,d", (int) tokamak.PowerOut);
		fontRenderer.drawString(s2, 160 - fontRenderer.getStringWidth(s2), 57, text);

		String txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.1");
		fontRenderer.drawString(txt, (52 - fontRenderer.getStringWidth(txt)) / 2, 6, shade, false);

		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.2");
		fontRenderer.drawString(txt, (54 - fontRenderer.getStringWidth(txt)) / 2, 15, shade, false);

		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.3");
		fontRenderer.drawString(txt, (54 - fontRenderer.getStringWidth(txt)) / 2, 24, shade, false);

		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.4");
		fontRenderer.drawString(txt, 114, 20, shade, false);

		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.6");
		fontRenderer.drawString(txt, 102, 68, shade, false);

		txt = I18n.format("gui." + InfoAR.MOD_ID + ".tokamak.5");
		fontRenderer.drawString(txt, 102, 34, shade, false);

		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltip
		ItemStack mouse = HCNet.getMouseStack(mc.player);

		GlStateManager.enableBlend();

		if(tank.isHovered(mouseX, mouseY))
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(tank.x, tank.y, tank.width, tank.height, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
				drawHoveringText(tank.getTooltip(mouseX, mouseY), mouseX, mouseY);
		}

		if(mouseX > guiLeft + 102 && mouseY > guiTop + 56 && mouseX < guiLeft + 162 && mouseY < guiTop + 66)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 103, guiTop + 57, 58, 8, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generateCustom("gui.hammercore.generation", (int) tokamak.PowerOut, TokamakConfig.TokamakGenLimit, "fe"), mouseX, mouseY);
		}

		if(mouseX > guiLeft + 102 && mouseY > guiTop + 43 && mouseX < guiLeft + 162 && mouseY < guiTop + 53)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 103, guiTop + 44, 58, 8, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generateCustom("gui.hammercore.consumption", (int) tokamak.PowerIn, TokamakConfig.TokamakMaxEnergyReceive, "fe"), mouseX, mouseY);
		}
		GL11.glPopMatrix();
	}

	@Override
	public void updateScreen()
	{
		tank.tank.setFluid(tokamak.fuelTank.getFluid());
		TileEntity tile = this.tokamak.getWorld().getTileEntity(this.tokamak.getPos());
		if(tile instanceof TileTokamak)
			this.tokamak = (TileTokamak) tile;
		super.updateScreen();
	}

	public DynGuiTex tx;

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		mouseX = i;
		mouseY = j;

		tx.render(guiLeft, guiTop);
		int text = GuiTheme.CURRENT_THEME.textShadeColor;
		GL11.glColor4f(ColorHelper.getRed(text), ColorHelper.getGreen(text), ColorHelper.getBlue(text), 1F);
		mc.renderEngine.bindTexture(tex);
		int topLeftX = (width - xSize) / 2;
		int topLeftY = (height - ySize) / 2;
		drawTexturedModalRect(topLeftX, topLeftY, 0, 0, xSize, ySize);

		UtilsFX.bindTexture("textures/gui/def_widgets.png");
		int col = GuiTheme.CURRENT_THEME.slotColor;
		GL11.glColor4f(ColorHelper.getRed(col), ColorHelper.getGreen(col), ColorHelper.getBlue(col), 1);
		RenderUtil.drawTexturedModalRect(guiLeft + 33, guiTop + 40, 0, 14, 22, 16);

		GL11.glColor4f(1, 1, 1, 1);

		// Fusing Indicator
		if(tokamak.isFusing())
		{
			double speed = 4F;
			double rad = Math.toRadians((System.currentTimeMillis() % (3600L * speed))) / speed;
			float green = ((float) Math.sin(rad) + 1) / 2F;
			green = .7F + green * .3F;
			GuiWidgets.drawSquare(topLeftX + 103, topLeftY + 20, 1, green, .2F);
		} else
			GuiWidgets.drawSquare(topLeftX + 103, topLeftY + 20, .5F, .5F, .5F);

		// Water Level Gauge
		if(tank.tank.getFluid() != null)
			tank.tank.getFluid().amount = tokamak.fuelTank.getFluidAmount();
		tank.render(mouseX, mouseY);

		// Temperature Gauge
		float temp = tokamak.getScaledTemp(58);
		GuiWidgets.drawEnergy(topLeftX + 85, topLeftY + 18 + 58 - temp, 8, temp, EnumPowerAnimation.UP);
		GuiWidgets.drawLine(topLeftX + 85, topLeftY + 18 + 58 - tokamak.getScaledFusionTemp(58));

		// Power Input Gauge
		GuiWidgets.drawEnergy(topLeftX + 103, topLeftY + 44, tokamak.getScaledPower(false, 58), 8, EnumPowerAnimation.RIGHT);

		// Power Output Gauge
		GuiWidgets.drawEnergy(topLeftX + 103, topLeftY + 57, tokamak.getScaledPower(true, 58), 8, EnumPowerAnimation.LEFT);
	}

	@Override
	public void initGui()
	{
		super.initGui();

		quant.val = quant.prevVal = tokamak.fuelTank.getFluidAmount();

		tank = new GuiFluidTank(guiLeft + 61, guiTop + 18, 16, 58, new FluidTank(tokamak.fuelTank.getCapacity()).readFromNBT(tokamak.fuelTank.writeToNBT(new NBTTagCompound())));
		GuiTexBakery b = GuiTexBakery.start();
		b.body(0, 0, xSize, ySize);
		for(Slot slot : inventorySlots.inventorySlots)
			b.slot(slot.xPos - 1, slot.yPos - 1);
		b.slot(60, 17, 18, 60).slot(84, 17, 10, 60).slot(102, 19, 10, 10).slot(102, 43, 60, 10).slot(102, 56, 60, 10);
		tx = b.bake();
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			int topLeftX = (width - xSize) / 2;
			int topLeftY = (height - ySize) / 2;

			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();

			// Show uses/recipes
			if(tank.isHovered(mouseX, mouseY))
			{
				FluidStack fs = tokamak.fuelTank.getFluid();

				if(fs != null)
				{
					FluidStack nfs = new FluidStack(fs.getFluid(), Fluid.BUCKET_VOLUME);
					if(showRecipe.getKeyCode() == keyCode)
						helper.showRecipes(nfs);
					else if(showUses.getKeyCode() == keyCode)
						helper.showUses(nfs);
				}
			} else if((mouseX > guiLeft + 102 && mouseY > guiTop + 56 && mouseX < guiLeft + 162 && mouseY < guiTop + 66) || (mouseX > guiLeft + 102 && mouseY > guiTop + 43 && mouseX < guiLeft + 162 && mouseY < guiTop + 53))
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}

		super.keyTyped(typedChar, keyCode);
	}
}