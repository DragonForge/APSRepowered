package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.blocks.tiles.TileLaserTable;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerLaserTable;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;

import java.io.IOException;

public class GuiLaserTable
		extends GuiWTFMojang<ContainerLaserTable>
{
	int mouseX, mouseY;
	public TileLaserTable t;
	public final EntityPlayer player;

	public DynGuiTex tex;

	public GuiLaserTable(EntityPlayer player, TileLaserTable t)
	{
		super(new ContainerLaserTable(player, t));
		this.t = t;
		this.player = player;
	}

	@Override
	public void updateScreen()
	{
		TileEntity tile = this.t.getWorld().getTileEntity(this.t.getPos());
		if(tile instanceof TileLaserTable)
			this.t = (TileLaserTable) tile;
		super.updateScreen();
	}

	@Override
	public void initGui()
	{
		super.initGui();

		GuiTexBakery b = GuiTexBakery.start();
		b.body(0, 0, 176, 166);
		for(int i = 1; i < inventorySlots.inventorySlots.size(); ++i)
			b.slot(inventorySlots.inventorySlots.get(i).xPos - 1, inventorySlots.inventorySlots.get(i).yPos - 1);
		b.slot(119, 30, 24, 24).slot(8, 10, 10, 60);
		tex = b.bake();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mouseX = mouseX;
		this.mouseY = mouseY;

		tex.render(guiLeft, guiTop);
		GL11.glColor4f(1, 1, 1, 1);
		GuiWidgets.drawFurnaceArrow(guiLeft + 90, guiTop + 34, t.recipeEnergy / (float) (t.recipe != null ? t.recipe.FE : 2_147_000_000));
		float energy = t.getEnergyStored() / (float) t.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;

		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);
		String ln = I18n.format(BlocksAR.LASER_TABLE.getTranslationKey() + ".name");
		fontRenderer.drawString(ln, (xSize - fontRenderer.getStringWidth(ln)) / 2, 6, shade, false);

		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		ItemStack mouse = HCNet.getMouseStack(mc.player);

		GlStateManager.enableBlend();
		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generateEnergy(t), mouseX, mouseY);
		}
		GL11.glPopMatrix();
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();

			// Show uses/recipes
			if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}

		super.keyTyped(typedChar, keyCode);
	}
}