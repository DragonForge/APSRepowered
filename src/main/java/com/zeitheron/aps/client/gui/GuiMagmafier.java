package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.APSRepowered;
import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileMagmafier;
import com.zeitheron.aps.cfg.MachineConfig;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerBlastFurnace;
import com.zeitheron.hammercore.client.gui.GuiFluidTank;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiMagmafier extends GuiWTFMojang<ContainerBlastFurnace> implements Runnable
{
	int mouseX, mouseY;
	
	public GuiFluidTank tank;
	private TileMagmafier tile;
	
	public int timeSinceSync = 100, prevProcess;
	
	public GuiMagmafier(EntityPlayer player, TileMagmafier tile)
	{
		super(new ContainerBlastFurnace(player, tile));
		this.tile = tile;
	}
	
	@Override
	public void onGuiClosed()
	{
		tile.onSync = null;
		super.onGuiClosed();
	}
	
	DynGuiTex tx;
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		tank = new GuiFluidTank(guiLeft + 107, guiTop + 14, 16, 58, new FluidTank(tile.lavaTank.getCapacity()).readFromNBT(tile.lavaTank.writeToNBT(new NBTTagCompound())));
		
		GuiTexBakery b = GuiTexBakery.start();
		b.body(0, 0, xSize, ySize);
		for(Slot slot : inventorySlots.inventorySlots)
			b.slot(slot.xPos - 1, slot.yPos - 1);
		b.slot(8, 10, 10, 60).slot(78, 10, 10, 66).slot(92, 10, 10, 66).slot(106, 13, 18, 60);
		tx = b.bake();
	}
	
	@Override
	public void updateScreen()
	{
		++timeSinceSync;
		prevProcess = tile.energyProgress;
		tile.onSync = this;
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TileMagmafier)
			this.tile = (TileMagmafier) tile;
		super.updateScreen();
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;
		
		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);
		
		int y = 0;
		for(String ln : tile.getName().split(" "))
		{
			fontRenderer.drawString(ln, xSize - 48, 8 + y, shade);
			y += fontRenderer.FONT_HEIGHT;
		}
		
		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltips
		
		ItemStack mouse = HCNet.getMouseStack(mc.player);
		
		GlStateManager.enableBlend();
		
		if(tank.isHovered(mouseX, mouseY))
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(tank.x, tank.y, tank.width, tank.height, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
				drawHoveringText(tank.getTooltip(mouseX, mouseY), mouseX, mouseY);
		}
		
		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generateEnergy(tile), mouseX, mouseY);
		}
		
		if(mouseX > guiLeft + 78 && mouseY > guiTop + 10 && mouseX < guiLeft + 88 && mouseY < guiTop + 76)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 79, guiTop + 11, 8, 64, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generate("matter", tile.matter, MachineConfig.MagmafierMatterLimit, "mu"), mouseX, mouseY);
		}
		
		if(mouseX > guiLeft + 92 && mouseY > guiTop + 10 && mouseX < guiLeft + 102 && mouseY < guiTop + 76)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 93, guiTop + 11, 8, 64, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
			{
				int top = MachineConfig.MagmafierEnergyPerOperation;
				int maxUse = Math.min(APSRepowered.getMaxEnergyConsumption(tile.energy, 50), top - tile.energyProgress);
				
				List<String> tip = TooltipHelper.generate("process", tile.energyProgress, MachineConfig.MagmafierEnergyPerOperation, "fe");
				tip.add(TextFormatting.GRAY + I18n.format("gui.hammercore.consumption") + ": " + maxUse + " " + I18n.format("gui.hammercore.fept"));
				drawHoveringText(tip, mouseX, mouseY);
			}
		}
		
		GL11.glPopMatrix();
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		mouseX = i;
		mouseY = j;
		
		tx.render(guiLeft, guiTop);
		
		GL11.glColor4f(1, 1, 1, 1);
		
		// Lava Level Gauge
		tank.tank.setFluid(tile.lavaTank.getFluid());
		tank.render(mouseX, mouseY);
		
		float energy = Math.min((prevProcess + (tile.energyProgress - prevProcess) * f) / (float) MachineConfig.MagmafierEnergyPerOperation, 1F) * 64;
		GuiWidgets.drawEnergy(guiLeft + 93, guiTop + 11 + 64 - energy, 8, energy, EnumPowerAnimation.DOWN);
		
		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/gui/widgets.png");
		energy = tile.matter / (float) MachineConfig.MagmafierMatterLimit * 64;
		RenderUtil.drawTexturedModalRect(guiLeft + 79, guiTop + 75 - energy, 0, 64 - energy, 8, energy);
		
		GL11.glPushMatrix();
		float alpha = (float) Math.sin(Math.toRadians(Math.min((timeSinceSync + f) * 10, 180)));
		GlStateManager.enableBlend();
		GL11.glColor4f(1, 1, 1, alpha * .25F);
		GL11.glTranslated(guiLeft + 53, guiTop + 16, 0);
		GL11.glScaled(16 / 48D, 16 / 48D, 1);
		RenderUtil.drawTexturedModalRect(0, 0, 8, 0, 48, 48);
		GL11.glPopMatrix();
		GL11.glColor4f(1, 1, 1, 1);
		
		energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();
			
			// Show uses/recipes
			if(tank.isHovered(mouseX, mouseY))
			{
				FluidStack fs = tile.lavaTank.getFluid();
				
				if(fs != null)
				{
					FluidStack nfs = new FluidStack(fs.getFluid(), Fluid.BUCKET_VOLUME);
					if(showRecipe.getKeyCode() == keyCode)
						helper.showRecipes(nfs);
					else if(showUses.getKeyCode() == keyCode)
						helper.showUses(nfs);
				}
			} else if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			} else if(mouseX > guiLeft + 78 && mouseY > guiTop + 10 && mouseX < guiLeft + 88 && mouseY < guiTop + 76)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_MATTER_UNIT);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_MATTER_UNIT);
			}
		}
		
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	public void run()
	{
		if(timeSinceSync * 10 >= 180)
			timeSinceSync = 0;
	}
}