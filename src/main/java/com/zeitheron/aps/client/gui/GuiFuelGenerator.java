package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.blocks.tiles.TileFuelGenerator;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerFuelGenerator;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiFuelGenerator extends GuiWTFMojang<ContainerFuelGenerator>
{
	int mouseX, mouseY;
	
	public TileFuelGenerator tile;
	
	public GuiFuelGenerator(EntityPlayer player, TileFuelGenerator tile)
	{
		super(new ContainerFuelGenerator(player, tile));
		this.tile = tile;
	}
	
	DynGuiTex tex;
	
	@Override
	public void updateScreen()
	{
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TileFuelGenerator)
			this.tile = (TileFuelGenerator) tile;
		super.updateScreen();
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		GuiTexBakery bak = GuiTexBakery.start();
		bak.body(0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			bak.slot(s.xPos - 1, s.yPos - 1);
		bak.slot(8, 10, 10, 60);
		tex = bak.bake();
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;
		
		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);
		
		int y = 0;
		for(String ln : tile.getName().split(" "))
		{
			fontRenderer.drawString(ln, 32, 24 + y, shade);
			y += fontRenderer.FONT_HEIGHT;
		}
		
		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltips
		
		ItemStack mouse = HCNet.getMouseStack(mc.player);
		
		GlStateManager.enableBlend();
		
		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
			{
				List<String> tip = TooltipHelper.generateEnergy(tile);
				int emiten = 0;
				gen:
				{
					float factor = Math.min(100, tile.time) / 100F;
					int toEmit = (int) Math.floor(40 * factor);
					if(toEmit == 0)
						break gen;
					emiten = tile.energy.receiveEnergy(toEmit, true);
				}
				tip.add(TextFormatting.DARK_GRAY + I18n.format("gui.hammercore.generation") + ": " + emiten + " " + I18n.format("gui.hammercore.fept"));
				drawHoveringText(tip, mouseX, mouseY);
			}
		} else if(mouseX > guiLeft + 79 && mouseY > guiTop + 24 && mouseX < guiLeft + 98 && mouseY < guiTop + 39)
		{
			boolean tooltiped = false, valid = false, render = mouse.isEmpty();
			
			if(tile.time > 0)
			{
				tooltiped = true;
				valid = true;
			} else
			{
				ItemStack item = tile.items.getStackInSlot(0);
				if(!item.isEmpty() && !tile.isItemValidForSlot(0, item))
				{
					tooltiped = true;
					valid = false;
				}
			}
			
			if(tooltiped)
			{
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				RenderUtil.drawColoredModalRect(guiLeft + 79.5, guiTop + 24, 17, 15, 0xAAFFFFFF);
				GL11.glEnable(GL11.GL_TEXTURE_2D);
			}
			
			if(valid && render)
			{
				List<String> tip = TooltipHelper.generate("burntime", tile.time / 100, tile.maxTime, "bt");
				int emiten = 0;
				gen:
				{
					float factor = Math.min(100, tile.time) / 100F;
					int toEmit = (int) Math.floor(40 * factor);
					if(toEmit == 0)
						break gen;
					emiten = tile.energy.receiveEnergy(toEmit, true);
				}
				tip.add(TextFormatting.DARK_GRAY + I18n.format("gui.hammercore.generation") + ": " + String.format("%,d", emiten));
				tip.add(TextFormatting.DARK_GRAY + "(~ " + String.format("%,d", Math.round(tile.time / 100F * 40F)) + " " + I18n.format("definition.hammercore:fe." + (isShiftKeyDown() ? "long" : "short")) + ")");
				drawHoveringText(tip, mouseX, mouseY);
			} else if(!valid && render)
			{
				List<String> tip = TooltipHelper.emptyTooltipList();
				tip.add(TextFormatting.RED + I18n.format("gui.hammercore.notfuel"));
				drawHoveringText(tip, mouseX, mouseY);
			}
		}
		
		GL11.glPopMatrix();
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		mouseX = i;
		mouseY = j;
		
		tex.render(guiLeft, guiTop);
		
		GL11.glColor4f(1, 1, 1, 1);
		
		float energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
		
		UtilsFX.bindTexture("textures/gui/def_widgets.png");
		int col = GuiTheme.current().slotColor;
		GL11.glColor4f(ColorHelper.getRed(col), ColorHelper.getGreen(col), ColorHelper.getBlue(col), 1);
		RenderUtil.drawTexturedModalRect(guiLeft + 81.5, guiTop + 25, 43, 0, 13, 13);
		
		if(tile.time > 0)
		{
			double k = Math.min(1, (tile.time / 100F) / (float) Math.max(tile.maxTime, 1)) * 13F;
			
			RenderUtil.drawTexturedModalRect(guiLeft + 81.5, guiTop + 24, 14, 0, 14, 14);
			
			GL11.glColor4f(1, 1, 1, 1);
			RenderUtil.drawTexturedModalRect(guiLeft + 80.5, guiTop + 25 + 12 - k, 0, 12 - k, 14, k + 1);
		} else
		{
			ItemStack item = tile.items.getStackInSlot(0);
			if(!item.isEmpty() && !tile.isItemValidForSlot(0, item))
			{
				GL11.glColor4f(1, .5F, .5F, 1);
				RenderUtil.drawTexturedModalRect(guiLeft + 80.5, guiTop + 24, 0, -1, 14, 14);
				GL11.glColor4f(1, 1, 1, 1);
			}
		}
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();
			
			// Show uses/recipes
			if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}
		
		super.keyTyped(typedChar, keyCode);
	}
}