package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.blocks.tiles.TilePoweredFurnace;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerPoweredFurnace;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Loader;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiPoweredFurnace extends GuiWTFMojang<ContainerPoweredFurnace>
{
	int mouseX, mouseY;
	public TilePoweredFurnace tile;
	
	public GuiPoweredFurnace(EntityPlayer player, TilePoweredFurnace tile)
	{
		super(new ContainerPoweredFurnace(player, tile));
		this.tile = tile;
	}
	
	DynGuiTex tex;
	
	@Override
	public void updateScreen()
	{
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TilePoweredFurnace)
			this.tile = (TilePoweredFurnace) tile;
		super.updateScreen();
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		GuiTexBakery bak = GuiTexBakery.start();
		bak.body(0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			bak.slot(s.xPos - 1, s.yPos - 1);
		bak.slot(8, 10, 10, 60).slot(78, 8, 10, 64);
		tex = bak.bake();
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;
		
		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);
		
		int y = 0;
		for(String ln : tile.getName().split(" "))
		{
			fontRenderer.drawString(ln, xSize - 52, 31 + y, shade);
			y += fontRenderer.FONT_HEIGHT;
		}
		
		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltips
		
		ItemStack mouse = HCNet.getMouseStack(mc.player);
		
		GlStateManager.enableBlend();
		
		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generateEnergy(tile), mouseX, mouseY);
		} else if(mouseX > guiLeft + 78 && mouseY > guiTop + 8 && mouseX < guiLeft + 88 && mouseY < guiTop + 72)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 79, guiTop + 9, 8, 62, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			
			if(mouse.isEmpty())
			{
				int max = TilePoweredFurnace.ENERGY_PROGESS;
				List<String> l = TooltipHelper.generate("process", Math.min(tile.energyProgress, max), max, "fe");
				if(Loader.isModLoaded("jei"))
					l.add(TextFormatting.DARK_GRAY + I18n.format("jei.tooltip.show.recipes"));
				drawHoveringText(l, mouseX, mouseY);
			}
		}
		
		GL11.glPopMatrix();
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		mouseX = i;
		mouseY = j;
		
		tex.render(guiLeft, guiTop);
		
		GL11.glColor4f(1, 1, 1, 1);
		
		float energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
		
		energy = tile.energyProgress / (float) TilePoweredFurnace.ENERGY_PROGESS * 62F;
		GuiWidgets.drawEnergy(guiLeft + 79, guiTop + 71 - energy, 8, energy, EnumPowerAnimation.UP);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();
			
			// Show uses/recipes
			if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}
		
		super.keyTyped(typedChar, keyCode);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(mouseX > guiLeft + 78 && mouseY > guiTop + 8 && mouseX < guiLeft + 88 && mouseY < guiTop + 72)
			IJeiHelper.Instance.getJEIModifier().showUses(new ItemStack(BlocksAR.POWERED_FURNACE));
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
}