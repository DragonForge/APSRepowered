package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileGrinder;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerGrinder;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.UtilsFX;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Loader;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiGrinder
		extends GuiWTFMojang<ContainerGrinder>
		implements Runnable
{
	int mouseX, mouseY;
	public TileGrinder tile;

	public int timeSinceSync = 100;

	public DynGuiTex tex;

	public GuiGrinder(EntityPlayer player, TileGrinder grinder)
	{
		super(new ContainerGrinder(player, grinder));
		this.tile = grinder;
	}

	@Override
	public void onGuiClosed()
	{
		tile.onSync = null;
		super.onGuiClosed();
	}

	@Override
	public void updateScreen()
	{
		++timeSinceSync;
		tile.onSync = this;
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TileGrinder)
			this.tile = (TileGrinder) tile;
		super.updateScreen();
	}

	@Override
	public void initGui()
	{
		super.initGui();

		GuiTexBakery bak = GuiTexBakery.start();
		bak.body(0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			bak.slot(s.xPos - 1, s.yPos - 1);
		bak.slot(61, 10, 10, 66).slot(8, 10, 10, 60);
		tex = bak.bake();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;

		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 4, shade);
		fontRenderer.drawString(I18n.format("gui." + InfoAR.MOD_ID + ".grinder.1"), 20, 54, shade, false);
		fontRenderer.drawString(I18n.format("gui." + InfoAR.MOD_ID + ".grinder.2"), 20, 63, shade, false);

		fontRenderer.drawString(I18n.format("gui." + InfoAR.MOD_ID + ".grinder_traces") + ":", 75, 10, shade);
		float i = 0;
		for(String s : tile.getPossibleOutputs())
		{
			int w = fontRenderer.getStringWidth(s);

			float sf = 1F;

			if(75 + w > xSize - 8)
				sf = (xSize - 8F) / (75F + w);

			GL11.glPushMatrix();
			GL11.glTranslatef(75, 20 + i, 0);
			GL11.glScalef(sf, sf, 1);
			fontRenderer.drawString(s, 0, 0, shade);
			GL11.glPopMatrix();
			i += fontRenderer.FONT_HEIGHT * sf;
		}

		// GlStateManager.disableTexture2D();
		//
		// GL11.glLineWidth(1F);
		// GL11.glBegin(GL11.GL_LINE_STRIP);
		// GL11.glVertex2f(64, 72);
		// GL11.glVertex2f(64 + 90, 72);
		// GL11.glEnd();
		//
		// GL11.glLineWidth(2F);
		// GL11.glBegin(GL11.GL_LINE_STRIP);
		// float details = .2F;
		// for(int x = 0; x <= 90 * details; ++x)
		// {
		// float ax = x / details;
		// float y = (float) (Math.sin(Math.toRadians(4 * (ax * 10 +
		// (System.currentTimeMillis() % 3600L) / 10F)))) * 10F;
		// GL11.glVertex2f(ax + 64, 72 - y);
		// }
		// GL11.glEnd();
		// GlStateManager.enableTexture2D();

		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		ItemStack mouse = HCNet.getMouseStack(mc.player);

		GlStateManager.enableBlend();
		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
				drawHoveringText(TooltipHelper.generateEnergy(tile), mouseX, mouseY);
		}
		if(mouseX > guiLeft + 61 && mouseY > guiTop + 10 && mouseX < guiLeft + 71 && mouseY < guiTop + 76)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 62, guiTop + 11, 8, 64, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				int max = tile.currentRecipe != null ? tile.currentRecipe.getEnergyRequired() : 0;
				List<String> l = TooltipHelper.generate("process", Math.min(tile.energyProgress, max), max, "fe");
				if(Loader.isModLoaded("jei"))
					l.add(TextFormatting.DARK_GRAY + I18n.format("jei.tooltip.show.recipes"));
				drawHoveringText(l, mouseX, mouseY);
			}
		}
		GL11.glPopMatrix();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		this.mouseX = mouseX;
		this.mouseY = mouseY;

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		tex.render(guiLeft, guiTop);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		float prog = Math.min(tile.currentRecipe != null ? tile.energyProgress / (float) tile.currentRecipe.getEnergyRequired() : 0F, 1F);
		GuiWidgets.drawEnergy(guiLeft + 62, guiTop + 75 - prog * 66, 8, prog * 66, EnumPowerAnimation.UP);

		UtilsFX.bindTexture(InfoAR.MOD_ID, "textures/gui/widgets.png");
		GL11.glPushMatrix();
		float alpha = (float) Math.sin(Math.toRadians(Math.min((timeSinceSync + partialTicks) * 10, 180)));
		GlStateManager.enableBlend();
		GL11.glColor4f(1, 1, 1, alpha * .25F);
		GL11.glTranslated(guiLeft + 30, guiTop + 16, 0);
		GL11.glScaled(16 / 48D, 16 / 48D, 1);
		RenderUtil.drawTexturedModalRect(0, 0, 8, 0, 48, 48);
		GL11.glPopMatrix();
		GL11.glColor4f(1, 1, 1, 1);

		float energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if(mouseX > guiLeft + 61 && mouseY > guiTop + 10 && mouseX < guiLeft + 71 && mouseY < guiTop + 76)
			IJeiHelper.Instance.getJEIModifier().showUses(new ItemStack(BlocksAR.GRINDER));
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();

			// Show uses/recipes
			if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}

		super.keyTyped(typedChar, keyCode);
	}

	@Override
	public void run()
	{
		if(timeSinceSync * 10 >= 180)
			timeSinceSync = 0;
	}
}