package com.zeitheron.aps.client.gui;

import com.zeitheron.aps.blocks.tiles.TileMagmaticGenerator;
import com.zeitheron.aps.intr.jei.APSRepoweredJEI;
import com.zeitheron.aps.inventory.ContainerMagmaticGenerator;
import com.zeitheron.hammercore.client.gui.GuiFluidTank;
import com.zeitheron.hammercore.client.gui.GuiWTFMojang;
import com.zeitheron.hammercore.client.gui.GuiWidgets;
import com.zeitheron.hammercore.client.gui.GuiWidgets.EnumPowerAnimation;
import com.zeitheron.hammercore.client.utils.RenderUtil;
import com.zeitheron.hammercore.client.utils.TooltipHelper;
import com.zeitheron.hammercore.client.utils.texture.gui.DynGuiTex;
import com.zeitheron.hammercore.client.utils.texture.gui.GuiTexBakery;
import com.zeitheron.hammercore.client.utils.texture.gui.theme.GuiTheme;
import com.zeitheron.hammercore.compat.jei.IJeiHelper;
import com.zeitheron.hammercore.net.HCNet;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.List;

public class GuiMagmaticGenerator
		extends GuiWTFMojang<ContainerMagmaticGenerator>
{
	int mouseX, mouseY;

	public GuiFluidTank tank;
	public TileMagmaticGenerator tile;

	public GuiMagmaticGenerator(EntityPlayer player, TileMagmaticGenerator tile)
	{
		super(new ContainerMagmaticGenerator(player, tile));
		this.tile = tile;
	}

	DynGuiTex tex;

	@Override
	public void initGui()
	{
		super.initGui();

		tank = new GuiFluidTank(guiLeft + 24, guiTop + 11, 16, 58, new FluidTank(tile.fuelTank.getCapacity()).readFromNBT(tile.fuelTank.writeToNBT(new NBTTagCompound())));

		GuiTexBakery bak = GuiTexBakery.start();
		bak.body(0, 0, xSize, ySize);
		for(Slot s : inventorySlots.inventorySlots)
			bak.slot(s.xPos - 1, s.yPos - 1);
		bak.slot(8, 10, 10, 60).slot(23, 10, 18, 60);
		tex = bak.bake();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		int text = GuiTheme.CURRENT_THEME.textColor;
		int shade = GuiTheme.CURRENT_THEME.textShadeColor;

		fontRenderer.drawString(mc.player.inventory.getDisplayName().getFormattedText(), 8, (ySize - 96) + 2, shade);

		int y = 0;
		for(String ln : tile.getName().split(" "))
		{
			fontRenderer.drawString(ln, xSize - 58, 24 + y, shade);
			y += fontRenderer.FONT_HEIGHT;
		}

		GL11.glPushMatrix();
		GL11.glTranslatef(-guiLeft, -guiTop, 200);
		// Draws the tooltips

		ItemStack mouse = HCNet.getMouseStack(mc.player);

		GlStateManager.enableBlend();

		if(tank.isHovered(mouseX, mouseY))
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(tank.x, tank.y, tank.width, tank.height, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				List<String> ls = TooltipHelper.emptyTooltipList();
				ls.addAll(tank.getTooltip(mouseX, mouseY));
				ls.add(TextFormatting.DARK_GRAY.toString() + I18n.format("gui.hammercore.burntime") + ": " + tile.time / 100 + " (~" + String.format("%,d", Math.round(tile.time / 100F * 160F)) + " " + I18n.format("definition.hammercore:fe." + (isShiftKeyDown() ? "long" : "short")) + ")");
				drawHoveringText(ls, mouseX, mouseY);
			}
		}

		if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
		{
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			RenderUtil.drawColoredModalRect(guiLeft + 9, guiTop + 11, 8, 58, 0xAAFFFFFF);
			GL11.glEnable(GL11.GL_TEXTURE_2D);

			if(mouse.isEmpty())
			{
				List<String> tip = TooltipHelper.generateEnergy(tile);
				int emiten = 0;
				gen:
				{
					float factor = Math.min(100, tile.time) / 100F;
					int toEmit = (int) Math.floor(160 * factor);
					if(toEmit == 0)
						break gen;
					emiten = tile.energy.receiveEnergy(toEmit, true);
				}
				tip.add(TextFormatting.DARK_GRAY + I18n.format("gui.hammercore.generation") + ": " + String.format("%,d", emiten) + " " + I18n.format("gui.hammercore.fept"));
				drawHoveringText(tip, mouseX, mouseY);
			}
		}

		GL11.glPopMatrix();
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		mouseX = i;
		mouseY = j;

		tex.render(guiLeft, guiTop);

		GL11.glColor4f(1, 1, 1, 1);

		// Lava Level Gauge
		tank.tank.setFluid(tile.fuelTank.getFluid());
		tank.render(mouseX, mouseY);

		float energy = tile.getEnergyStored() / (float) tile.getMaxEnergyStored() * 58F;
		GuiWidgets.drawEnergy(guiLeft + 9, guiTop + 11 + 58 - energy, 8, energy, EnumPowerAnimation.UP);
	}

	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		IJeiHelper helper = IJeiHelper.Instance.getJEIModifier();
		if(helper.getKeybind_showRecipes() != null)
		{
			KeyBinding showRecipe = (KeyBinding) helper.getKeybind_showRecipes();
			KeyBinding showUses = (KeyBinding) helper.getKeybind_showUses();

			// Show uses/recipes
			if(tank.isHovered(mouseX, mouseY))
			{
				FluidStack fs = tile.fuelTank.getFluid();

				if(fs != null)
				{
					FluidStack nfs = new FluidStack(fs.getFluid(), Fluid.BUCKET_VOLUME);
					if(showRecipe.getKeyCode() == keyCode)
						helper.showRecipes(nfs);
					else if(showUses.getKeyCode() == keyCode)
						helper.showUses(nfs);
				}
			} else if(mouseX > guiLeft + 8 && mouseY > guiTop + 10 && mouseX < guiLeft + 18 && mouseY < guiTop + 70)
			{
				if(showRecipe.getKeyCode() == keyCode)
					helper.showRecipes(APSRepoweredJEI.IG_FORGE_ENERGY);
				else if(showUses.getKeyCode() == keyCode)
					helper.showUses(APSRepoweredJEI.IG_FORGE_ENERGY);
			}
		}

		super.keyTyped(typedChar, keyCode);
	}

	@Override
	public void updateScreen()
	{
		TileEntity tile = this.tile.getWorld().getTileEntity(this.tile.getPos());
		if(tile instanceof TileMagmaticGenerator)
			this.tile = (TileMagmaticGenerator) tile;
		super.updateScreen();
	}
}