package com.zeitheron.aps.util;

import java.util.Collection;
import java.util.Random;

import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class LyingItems
{
	public NonNullList<LocatableStack> merging = NonNullList.create();
	public LocatableStack mergeTar = null;
	public int mergeTime = 0;
	
	public void addItems(Collection<ItemStack> items)
	{
		items.stream().map(LocatableStack::new).forEach(i ->
		{
			i.init();
			merging.add(i);
		});
	}
	
	public void mergeInto(ItemStack output, int timeInTicks)
	{
		if(mergeTar == null)
		{
			mergeTar = new LocatableStack(output);
			mergeTar.x = .5F;
			mergeTar.y = .5F;
			mergeTar.setScaleR(0);
			mergeTime = timeInTicks + 5;
		}
	}
	
	public void tick()
	{
		for(LocatableStack ls : merging)
		{
			ls.prx = ls.x;
			ls.pry = ls.y;
			ls.prrX = ls.rX;
			ls.prrY = ls.rY;
			ls.prScale = ls.scale;
		}
		
		if(mergeTar != null)
		{
			{
				LocatableStack ls = mergeTar;
				ls.prx = ls.x;
				ls.pry = ls.y;
				ls.prrX = ls.rX;
				ls.prrY = ls.rY;
				ls.prScale = ls.scale;
			}
			
			float targX = mergeTar.x;
			float targY = mergeTar.y;
			
			if(mergeTime > 5)
			{
				for(LocatableStack ls : merging)
				{
					float speedx = (targX - ls.x) / mergeTime;
					float speedy = (targY - ls.y) / mergeTime;
					
					ls.x += speedx;
					ls.y += speedy;
				}
				
				mergeTime--;
				if(mergeTime == 5)
					mergeTar.setScaleR(.75F);
			} else if(mergeTime > 0)
			{
				mergeTime--;
				mergeTar.setScale(.75F + (1F - mergeTime / 5F) * .25F);
			} else if(mergeTime > -5)
			{
				mergeTar.setScaleR(1);
				mergeTime--;
				if(mergeTime <= -5)
				{
					mergeTime = 0;
					mergeTar = null;
					merging.clear();
				}
			}
		}
	}
	
	public static class LocatableStack
	{
		public final ItemStack stack;
		public Random rng;
		
		public float scale = 1, prScale = 1;
		public float prx, pry, prrX, prrY;
		public float x, y, rX, rY;
		
		public LocatableStack(ItemStack stack)
		{
			this.stack = stack.copy();
		}
		
		public void init()
		{
			if(rng == null)
				rng = new Random();
			
			x = rng.nextFloat();
			y = rng.nextFloat();
			rX = rng.nextFloat() * 360;
			rY = rng.nextFloat() * 360;
		}
		
		public void setScale(float s)
		{
			scale = s;
		}
		
		public void setScaleR(float s)
		{
			scale = prScale = s;
		}
		
		public float getScale(float partial)
		{
			return prScale + (scale - prScale) * partial;
		}
		
		public float getX(float partial)
		{
			return prx + (x - prx) * partial;
		}
		
		public float getY(float partial)
		{
			return pry + (y - pry) * partial;
		}
	}
}