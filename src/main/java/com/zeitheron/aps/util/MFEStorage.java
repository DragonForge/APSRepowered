package com.zeitheron.aps.util;

import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;

public class MFEStorage extends FEEnergyStorage
{
	public MFEStorage(int capacity, int maxTransfer)
	{
		super(capacity, maxTransfer);
	}
	
	public MFEStorage(int capacity)
	{
		super(capacity);
	}
	
	public MFEStorage(int capacity, int maxReceive, int maxExtract)
	{
		super(capacity, maxReceive, maxExtract);
	}
	
	public void normalizeEnergy()
	{
		this.energy = Math.max(0, Math.min(this.capacity, this.energy));
	}
	
	public void setCapacity(int cap)
	{
		this.capacity = cap;
		normalizeEnergy();
	}
}