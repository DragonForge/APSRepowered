package com.zeitheron.aps.util;

import java.awt.Rectangle;
import java.util.Random;

import com.zeitheron.hammercore.lib.zlib.utils.Vec2D;

import net.minecraft.util.math.MathHelper;

public class RectangleAnimation2D
{
	public final Rectangle bounds;
	
	public float prx, pry;
	public float x, y;
	public float tx, ty;
	public float step = 1;
	
	public RectangleAnimation2D(Rectangle bounds, float step)
	{
		this.bounds = bounds;
		this.step = step;
		
		Random r = new Random();
		prx = x = bounds.x + bounds.width * r.nextFloat();
		pry = y = bounds.y + bounds.height * r.nextFloat();
	}
	
	public float getInterpX(float pt)
	{
		return prx + (x - prx) * pt;
	}
	
	public float getInterpY(float pt)
	{
		return pry + (y - pry) * pt;
	}
	
	public boolean hasReachedTarget()
	{
		Vec2D A = new Vec2D(prx, pry);
		Vec2D C = new Vec2D(tx, ty);
		Vec2D B = new Vec2D(x, y);
		
		double AB = A.distanceTo(B);
		double AC = A.distanceTo(C);
		double CB = B.distanceTo(C);
		
		return Vec2D.isNearlyEqual(AC + CB, AB);
	}
	
	public boolean outOfBounds()
	{
		return !bounds.contains(x, y);
	}
	
	public void update(Random random)
	{
		prx = x;
		pry = y;
		
		Vec2D dir = getNormalizedDirection().scale(step);
		
		x += dir.x;
		y += dir.y;
		
		if(hasReachedTarget() || outOfBounds())
		{
			tx = bounds.x + random.nextFloat() * bounds.width;
			ty = bounds.y + random.nextFloat() * bounds.height;
		}
	}
	
	public Vec2D getNormalizedDirection()
	{
		float x = tx - this.x;
		float y = ty - this.y;
		float f = Math.max(MathHelper.sqrt(x * x + y * y), 0.0001F);
		return new Vec2D(x / f, y / f);
	}
}