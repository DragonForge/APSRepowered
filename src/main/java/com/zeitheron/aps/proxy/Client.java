package com.zeitheron.aps.proxy;

import com.google.common.base.Predicates;
import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.BlockEnergyPipe;
import com.zeitheron.aps.blocks.tiles.TileFluidPump;
import com.zeitheron.aps.blocks.tiles.TileLaserTable;
import com.zeitheron.aps.client.models.BakedPipeModel;
import com.zeitheron.aps.client.tesr.TESRFluidPump;
import com.zeitheron.aps.client.tesr.TESRLaserTable;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.pipes.IPipe;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.hammercore.proxy.RenderProxy_Client;
import com.zeitheron.hammercore.utils.base.Cast;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.BlockFluidBase;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class Client
		extends Common
{
	@Override
	public void preInit()
	{
		ModelLoader.setCustomStateMapper(BlocksAR.HEAVY_WATER, new StateMap.Builder().ignore(BlockFluidBase.LEVEL).build());
		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileLaserTable.class, new TESRLaserTable());
		tesr(BlocksAR.FLUID_PUMP, TileFluidPump.class, new TESRFluidPump());

		{
			for(Block b : ForgeRegistries.BLOCKS.getValuesCollection())
				if(b instanceof BlockEnergyPipe)
				{
					BlockEnergyPipe pipe = (BlockEnergyPipe) b;
					RenderProxy_Client.bakedModelStore.putConstant(pipe.getDefaultState(), new BakedPipeModel<>(pipe, false));
				}
		}

		mapFluid(BlocksAR.HEAVY_WATER);
	}

	@SubscribeEvent
	public void modelBake(ModelBakeEvent e)
	{
		for(Block b : ForgeRegistries.BLOCKS.getValuesCollection())
			if(b instanceof BlockEnergyPipe)
			{
				BlockEnergyPipe pipe = (BlockEnergyPipe) b;
				BakedPipeModel<?> model = new BakedPipeModel<>(pipe, true);
				e.getModelRegistry().putObject(new ModelResourceLocation(pipe.getRegistryName(), "inventory"), model);
			}
	}

	@SubscribeEvent
	public void reloadTextureMap(TextureStitchEvent.Pre e)
	{
		TextureMap map = e.getMap();

		ForgeRegistries.BLOCKS.getValuesCollection()
				.stream()
				.filter(Predicates.instanceOf(IPipe.class))
				.map(b -> Cast.cast(b, IPipe.class))
				.forEach(pipe -> map.registerSprite(pipe.getTex()));
	}

	private static <T extends TileEntity> void tesr(Block block, Class<T> tile, TESR<T> tesr)
	{
		ClientRegistry.bindTileEntitySpecialRenderer(tile, tesr);
		ItemRenderingHandler.INSTANCE.setItemRender(Item.getItemFromBlock(block), tesr);
	}

	private static void mapFluid(BlockFluidBase fluidBlock)
	{
		final Item item = Item.getItemFromBlock(fluidBlock);
		assert item != null;
		ModelBakery.registerItemVariants(item);
		ModelResourceLocation modelResourceLocation = new ModelResourceLocation(InfoAR.MOD_ID + ":fluid", fluidBlock.getFluid().getName());
		ModelLoader.setCustomMeshDefinition(item, stack -> modelResourceLocation);
		ModelLoader.setCustomStateMapper(fluidBlock, new StateMapperBase()
		{
			@Override
			protected ModelResourceLocation getModelResourceLocation(IBlockState state)
			{
				return modelResourceLocation;
			}
		});
	}
}