package com.zeitheron.aps.pipes.energy;

import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;

public class PipeEnergyStorage extends FEEnergyStorage
{
	public PipeEnergyStorage(int capacity, int maxReceive, int maxExtract)
	{
		super(capacity, maxReceive, maxExtract);
	}
	
	public PipeEnergyStorage(int capacity, int maxTransfer)
	{
		super(capacity, maxTransfer);
	}
	
	public PipeEnergyStorage(int capacity)
	{
		super(capacity);
	}
	
	public void setEnergyStored(int energy)
	{
		this.energy = energy;
	}
}