package com.zeitheron.aps.pipes.energy;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.aps.pipes.IPipe;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class PipeEnergyAccessPoint extends FluidEnergyAccessPoint
{
	protected World w;
	protected BlockPos p;
	
	public PipeEnergyAccessPoint(World world, BlockPos pos)
	{
		super(world, pos);
		this.w = world;
		this.p = pos;
	}
	
	@Override
	public List<IEnergyStorage> findFEAcceptors()
	{
		List<IEnergyStorage> list = new ArrayList<>();
		for(EnumFacing face : EnumFacing.VALUES)
		{
			BlockPos apos = p.offset(face);
			TileEntity tile = w.getTileEntity(apos);
			if(tile != null && !(tile instanceof IPipe) && tile.hasCapability(CapabilityEnergy.ENERGY, face.getOpposite()))
				list.add(tile.getCapability(CapabilityEnergy.ENERGY, face.getOpposite()));
		}
		list.removeIf(es -> !es.canReceive());
		return list;
	}
}