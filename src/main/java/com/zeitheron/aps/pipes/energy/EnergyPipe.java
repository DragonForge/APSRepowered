package com.zeitheron.aps.pipes.energy;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileEnergyPipe;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public abstract class EnergyPipe implements IEnergyStorage
{
	protected PipeEnergyStorage buffer;
	public EnergyPipeGrid grid;
	public final TileEnergyPipe tile;
	
	public float delta;
	
	public EnergyPipe(TileEnergyPipe tile)
	{
		this.tile = tile;
	}
	
	public PipeEnergyStorage getBuffer()
	{
		if(buffer == null)
		{
			buffer = new PipeEnergyStorage(getMaxTransfer());
			setReceive(this instanceof ExtractingEnergyPipe);
		}
		return buffer;
	}
	
	public boolean isValid()
	{
		return tile != null && !tile.isInvalid();
	}
	
	public abstract ResourceLocation getPipeTexture();
	
	public abstract int getMaxTransfer();
	
	public void update()
	{
		long start = System.currentTimeMillis();
		linkGrid();
		updateDelta();
		splitEnergy();
		emitEnergy();
		long end = System.currentTimeMillis();
		
		if(end - start >= 10L && InfoAR.MOD_ID.contains("VERSION"))
			System.out.println("Tick for " + tile.getPos() + " took longer than expected (" + (end - start) + " ms)");
	}
	
	public void linkGrid()
	{
		if(grid == null)
		{
			// Try to connect to nearest pipes, if possible
			for(EnumFacing face : EnumFacing.VALUES)
			{
				EnergyPipe p = tile.getRelPipe(face);
				if(p != null && p.grid != null)
				{
					p.grid.addPipe(this);
					grid = p.grid;
					break;
				}
			}
			
			if(grid == null)
				grid = new EnergyPipeGrid(tile.getWorld(), this);
		} else
			for(EnumFacing face : EnumFacing.VALUES)
			{
				EnergyPipe p = tile.getRelPipe(face);
				if(p != null && p.grid != null && p.grid != grid && p.grid.positions.size() >= grid.positions.size())
				{
					EnergyPipeGrid.merge(p.grid, grid);
					break;
				}
			}
	}
	
	protected void emitEnergy()
	{
		int energy = tile.acessPoint.emitEnergy(getBuffer().getEnergyStored());
		delta += energy;
		getBuffer().extractEnergy(energy, false);
	}
	
	protected void updateDelta()
	{
		delta -= delta / 20F;
		if(delta <= .001F)
			delta = 0;
	}
	
	protected void splitEnergy()
	{
		if(grid != null && grid.isMaster(tile.getPos()))
			grid.balance();
	}
	
	public void setEnergy(int energy)
	{
		int pr = getBuffer().getEnergyStored();
		getBuffer().setEnergyStored(energy);
		delta += Math.abs(pr - getBuffer().getEnergyStored());
	}
	
	public void read(NBTTagCompound nbt)
	{
		getBuffer().readFromNBT(nbt);
	}
	
	public void write(NBTTagCompound nbt)
	{
		getBuffer().writeToNBT(nbt);
	}
	
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY;
	}
	
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY ? (T) getBuffer() : null;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return canReceive() ? getBuffer().receiveEnergy(maxReceive, simulate) : 0;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return getBuffer().extractEnergy(maxExtract, simulate);
	}
	
	@Override
	public int getEnergyStored()
	{
		return getBuffer().getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return getBuffer().getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return getBuffer().canExtract();
	}
	
	public boolean receive;
	
	@Override
	public boolean canReceive()
	{
		return receive;
	}
	
	public void setReceive(boolean receive)
	{
		this.receive = receive;
	}
}