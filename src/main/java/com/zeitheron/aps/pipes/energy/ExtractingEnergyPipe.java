package com.zeitheron.aps.pipes.energy;

import com.zeitheron.aps.blocks.tiles.TileEnergyPipe;
import com.zeitheron.aps.pipes.IPipe;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public abstract class ExtractingEnergyPipe extends EnergyPipe
{
	public ExtractingEnergyPipe(TileEnergyPipe tile)
	{
		super(tile);
	}
	
	@Override
	protected void emitEnergy()
	{
		super.emitEnergy();
		
		for(EnumFacing face : EnumFacing.VALUES)
		{
			TileEntity te = tile.getWorld().getTileEntity(tile.getPos().offset(face));
			if(te != null && !(te instanceof IPipe) && te.hasCapability(CapabilityEnergy.ENERGY, face.getOpposite()))
			{
				IEnergyStorage es = te.getCapability(CapabilityEnergy.ENERGY, face.getOpposite());
				if(es != null && es.canExtract())
				{
					int maxExtract = buffer.getMaxEnergyStored() - buffer.getEnergyStored();
					buffer.receiveEnergy(es.extractEnergy(maxExtract, false), false);
				}
			}
		}
	}
}