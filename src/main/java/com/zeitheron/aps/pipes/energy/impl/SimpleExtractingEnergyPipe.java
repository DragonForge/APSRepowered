package com.zeitheron.aps.pipes.energy.impl;

import com.zeitheron.aps.blocks.tiles.TileEnergyPipe;
import com.zeitheron.aps.pipes.energy.ExtractingEnergyPipe;

import net.minecraft.util.ResourceLocation;

public class SimpleExtractingEnergyPipe extends ExtractingEnergyPipe
{
	public final int cap;
	public final ResourceLocation tex;
	
	public SimpleExtractingEnergyPipe(TileEnergyPipe tile, ResourceLocation tex, int capacity)
	{
		super(tile);
		this.tex = tex;
		this.cap = capacity;
	}

	@Override
	public ResourceLocation getPipeTexture()
	{
		return tex;
	}

	@Override
	public int getMaxTransfer()
	{
		return cap;
	}
}