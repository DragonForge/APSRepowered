package com.zeitheron.aps.pipes.energy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.zeitheron.aps.blocks.tiles.TileEnergyPipe;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;

import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;

public class EnergyPipeGrid
{
	public final GridAccessPoint gridAP;
	public final List<BlockPos> positions = new ArrayList<>();
	public EnergyPipe master;
	public World world;
	
	public EnergyPipeGrid parent;
	
	public EnergyPipeGrid(World world, EnergyPipe master)
	{
		gridAP = new GridAccessPoint(world, master.tile.getPos());
		this.world = world;
		this.master = master;
		onMajorGridChange();
	}
	
	public boolean isMaster(BlockPos pos)
	{
		return parent != null ? parent.isMaster(pos) : Objects.equals(pos, this.master.tile.getPos());
	}
	
	public static EnergyPipeGrid merge(EnergyPipeGrid a, EnergyPipeGrid b)
	{
		EnergyPipeGrid newGrid = new EnergyPipeGrid(a.world, a.master);
		
		Stream.concat(a.stream(), b.stream()).forEach(p ->
		{
			p.grid = null;
			newGrid.addPipe(p);
		});
		
		return newGrid;
	}
	
	/**
	 * May merge two grids.
	 */
	public void addPipe(EnergyPipe pipe)
	{
		EnergyPipeGrid grid = pipe.grid != null ? merge(this, pipe.grid) : this;
		grid.positions.add(pipe.tile.getPos());
		pipe.grid = grid;
		onMajorGridChange();
	}
	
	public void setParent(EnergyPipeGrid parent)
	{
		this.parent = parent;
	}
	
	public void onMajorGridChange()
	{
		if(parent != null)
		{
			while(!positions.isEmpty())
			{
				BlockPos p = positions.remove(0);
				EnergyPipe pipe = TileEnergyPipe.getPipe(world, p);
				if(pipe != null)
				{
					pipe.grid = parent;
					if(!parent.positions.contains(p))
						parent.positions.add(p);
				}
			}
			return;
		}
		
		forEachPipe(pipe ->
		{
			if(pipe != null)
				pipe.grid = null;
		});
		
		positions.clear();
		
		positions.add(this.master.tile.getPos());
		
		for(int i = 0; i < positions.size(); ++i)
		{
			EnergyPipe pipe = TileEnergyPipe.getPipe(world, positions.get(i));
			if(pipe == null)
				positions.remove(i);
			else
			{
				pipe.grid = this;
				for(EnumFacing face : EnumFacing.VALUES)
				{
					EnergyPipe ep = pipe.tile.getRelPipe(face);
					if(ep != null && !positions.contains(ep.tile.getPos()))
						positions.add(ep.tile.getPos());
				}
			}
		}
		
		balance();
	}
	
	public void forEachPipe(Consumer<EnergyPipe> consumer)
	{
		stream().forEach(consumer::accept);
	}
	
	public Stream<EnergyPipe> stream()
	{
		return positions.stream().map(pos -> TileEnergyPipe.getPipe(world, pos)).filter(p -> p != null);
	}
	
	public void balance()
	{
		EnExtractionData dat = EnExtractionData.get();
		
		dat.clear();
		
		int fe = 0;
		
		for(int i = 0; i < positions.size(); ++i)
		{
			BlockPos pos = positions.get(i);
			EnergyPipe pipe = TileEnergyPipe.getPipe(world, pos);
			if(pipe != null)
			{
				if(pipe.isValid())
				{
					int en = pipe.getBuffer().getEnergyStored();
					fe += en;
					if(en > 0)
						dat.put(pos, en);
				} else
				{
					positions.remove(i);
					pipe.grid = null;
					onMajorGridChange();
					
					i = 0;
					fe = 0;
				}
			}
		}
		
		int emit = gridAP.emitEnergy(fe);
		
		for(int i = 0; i < positions.size() && emit > 0; ++i)
		{
			BlockPos pos = positions.get(i);
			if(dat.containsKey(pos))
			{
				EnergyPipe pipe = TileEnergyPipe.getPipe(world, pos);
				if(pipe != null && pipe.isValid())
					emit -= pipe.getBuffer().extractEnergy(Math.min(emit, dat.get(pos)), false);
			}
		}
	}
	
	private class GridAccessPoint extends FluidEnergyAccessPoint
	{
		public GridAccessPoint(World world, BlockPos pos)
		{
			super(world, pos);
		}
		
		public PipeEnergyStorage getPipeBuf(World world, BlockPos pos)
		{
			EnergyPipe pipe = TileEnergyPipe.getPipe(world, pos);
			if(pipe != null)
				return pipe.getBuffer();
			return null;
		}
		
		@Override
		public List<IEnergyStorage> findFEAcceptors()
		{
			return positions.stream().map(pos -> getPipeBuf(world, pos)).filter(es -> es != null).collect(Collectors.toList());
		}
		
		@Override
		public List<IFluidHandler> findFLAcceptors(FluidStack fluid)
		{
			return Collections.emptyList();
		}
	}
}