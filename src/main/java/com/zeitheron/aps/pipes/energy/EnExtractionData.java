package com.zeitheron.aps.pipes.energy;

import java.util.HashMap;

import net.minecraft.util.math.BlockPos;

class EnExtractionData extends HashMap<BlockPos, Integer>
{
	static final ThreadLocal<EnExtractionData> DATAS = ThreadLocal.withInitial(EnExtractionData::new);

	public static EnExtractionData get()
	{
		return DATAS.get();
	}
}