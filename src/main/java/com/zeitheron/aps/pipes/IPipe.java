package com.zeitheron.aps.pipes;

import com.zeitheron.aps.client.models.BakedPipeModel;
import com.zeitheron.hammercore.utils.PositionedStateImplementation;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;
import java.util.function.Predicate;

/**
 * An implementation for {@link TileEntity} that wants to be a pipe.
 */
public interface IPipe
{
	Cuboid6 CENTER = new Cuboid6(5 / 16D, 5 / 16D, 5 / 16D, 11 / 16D, 11 / 16D, 11 / 16D);
	Cuboid6 DOWN_CENTER = new Cuboid6(5.5 / 16D, 0, 5.5 / 16D, 10.5 / 16D, 5 / 16D, 10.5 / 16D);
	Cuboid6 CENTER_UP = new Cuboid6(5.5 / 16D, 10.5 / 16D, 5.5 / 16D, 10.5 / 16D, 1, 10.5 / 16D);
	Cuboid6 NORH_CENTER = new Cuboid6(5.5 / 16D, 5.5 / 16D, 0, 10.5 / 16D, 10.5 / 16D, 5 / 16D);
	Cuboid6 CENTER_SOUTH = new Cuboid6(5.5 / 16D, 5.5 / 16D, 10.5 / 16D, 10.5 / 16D, 10.5 / 16D, 1);
	Cuboid6 WEST_CENTER = new Cuboid6(0, 5.5 / 16D, 5.5 / 16D, 5 / 16D, 10.5 / 16D, 10.5 / 16D);
	Cuboid6 CENTER_EAST = new Cuboid6(10.5 / 16D, 5.5 / 16D, 5.5 / 16D, 1, 10.5 / 16D, 10.5 / 16D);

	boolean isConnectedTo(EnumFacing face);

	ResourceLocation getTex();

	BlockPos coordinates();

	World world();

	void kill();

	default void addCuboids(List<Cuboid6> cubes)
	{
		cubes.add(CENTER);
		if(isConnectedTo(EnumFacing.DOWN))
			cubes.add(DOWN_CENTER);
		if(isConnectedTo(EnumFacing.UP))
			cubes.add(CENTER_UP);
		if(isConnectedTo(EnumFacing.NORTH))
			cubes.add(NORH_CENTER);
		if(isConnectedTo(EnumFacing.SOUTH))
			cubes.add(CENTER_SOUTH);
		if(isConnectedTo(EnumFacing.WEST))
			cubes.add(WEST_CENTER);
		if(isConnectedTo(EnumFacing.EAST))
			cubes.add(CENTER_EAST);
	}

	static IPipe get(IBlockAccess world, BlockPos pos)
	{
		TileEntity tile = world.getTileEntity(pos);
		Block blk = world.getBlockState(pos).getBlock();

		if(tile instanceof IPipe) return (IPipe) tile;
		if(blk instanceof IPipe) return (IPipe) blk;

		return null;
	}

	@SideOnly(Side.CLIENT)
	default void generateBakedQuads(List<BakedQuad> quads, EnumFacing side, long rand, IBlockAccess world, BlockPos pos, PositionedStateImplementation state, Predicate<EnumFacing> connetions, TextureAtlasSprite sprite)
	{
		BakedPipeModel.generateQuads(quads, side, rand, world, pos, state, connetions, sprite);
	}
}