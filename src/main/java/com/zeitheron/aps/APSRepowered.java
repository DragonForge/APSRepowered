package com.zeitheron.aps;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.aps.api.GrinderRecipe;
import com.zeitheron.aps.api.ILoadableLoadModule;
import com.zeitheron.aps.api.MeltingRecipe;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.init.FluidsAR;
import com.zeitheron.aps.init.ItemsAR;
import com.zeitheron.aps.init.RecipesAR;
import com.zeitheron.aps.intr.crafttweaker.CraftTweakerCompat;
import com.zeitheron.aps.intr.crafttweaker.core.ICTCompat;
import com.zeitheron.aps.intr.thermalexpansion.ThermalExpansionAR;
import com.zeitheron.aps.proxy.Common;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.internal.SimpleRegistration;
import com.zeitheron.hammercore.mod.ModuleLister;
import com.zeitheron.hammercore.utils.ILoadable;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.UniversalBucket;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLLoadCompleteEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.oredict.OreDictionary;

@Mod(modid = InfoAR.MOD_ID, name = InfoAR.MOD_NAME, version = InfoAR.MOD_VERSION, dependencies = "required-after:hammercore;required-after:hammermetals;after:crafttweaker", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", updateJSON = "https://dccg.herokuapp.com/api/fmluc/291884")
public class APSRepowered
{
	@Instance
	public static APSRepowered instance;
	
	@SidedProxy(clientSide = "com.zeitheron.aps.proxy.Client", serverSide = "com.zeitheron.aps.proxy.Common")
	public static Common proxy;
	
	public static final Logger LOG = LogManager.getLogger(InfoAR.MOD_NAME);
	
	public static final List<ILoadableLoadModule> modules = new ArrayList<>();
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with APSRepowered jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/291884 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put(InfoAR.MOD_ID, "https://minecraft.curseforge.com/projects/291884");
	}
	
	public static final CreativeTabs tab = new CreativeTabs(InfoAR.MOD_ID)
	{
		@Override
		public void displayAllRelevantItems(NonNullList<ItemStack> items)
		{
			UniversalBucket ub = ForgeModContainer.getInstance().universalBucket;
			List<Fluid> fluids = new ArrayList<>();
			
			fluids.add(FluidsAR.HEAVY_WATER);
			
			for(Item item : Item.REGISTRY)
			{
				item.getSubItems(this, items);
				if(item == ub)
					for(Fluid fl : fluids)
						items.add(FluidUtil.getFilledBucket(new FluidStack(fl, Fluid.BUCKET_VOLUME)));
			}
		}
		
		@Override
		public ItemStack createIcon()
		{
			return new ItemStack(ItemsAR.TOKAMAK_PLATING);
		}
	};
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		modules.addAll(ModuleLister.createModules(ILoadableLoadModule.class, "apsmodule", e.getAsmData()));
		
		FluidsAR.init();
		
		SimpleRegistration.registerFieldItemsFrom(ItemsAR.class, InfoAR.MOD_ID, tab);
		SimpleRegistration.registerFieldBlocksFrom(BlocksAR.class, InfoAR.MOD_ID, tab);
		
		OreDictionary.registerOre("gemCoal", new ItemStack(Items.COAL, 1, 0));
		
		proxy.preInit();
		
		modules.forEach(ILoadable::preInit);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
		RecipesAR.INSTANCE.laserTable.call();
		GrinderRecipe.init.call();
		MeltingRecipe.init.call();
		if(Loader.isModLoaded("thermalexpansion"))
			try
			{
				ThermalExpansionAR.register();
			} catch(Throwable err)
			{
			}
		modules.forEach(ILoadable::init);
		ICTCompat ictc = CraftTweakerCompat.compat();
		if(ictc != null)
			ictc.init();
	}
	
	@EventHandler
	public void loadComplete(FMLLoadCompleteEvent e)
	{
		ICTCompat ictc = CraftTweakerCompat.compat();
		if(ictc != null)
			ictc.onLoadComplete();
	}
	
	public static int getMaxEnergyConsumption(IEnergyStorage storage, int maxExtract)
	{
		int capacity = storage.getMaxEnergyStored();
		if(capacity <= 0)
			return 0;
		float fillPercent = storage.getEnergyStored() / (float) capacity;
		return Math.max(Math.round(fillPercent * maxExtract), 1);
	}
}