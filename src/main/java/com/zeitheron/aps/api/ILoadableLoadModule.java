package com.zeitheron.aps.api;

import com.zeitheron.hammercore.mod.ILoadModule;
import com.zeitheron.hammercore.utils.ILoadable;

public interface ILoadableLoadModule extends ILoadModule, ILoadable
{
}