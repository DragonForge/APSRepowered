package com.zeitheron.aps.api;

import java.util.HashMap;
import java.util.Map;

import com.zeitheron.aps.init.FluidsAR;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;

public class TokamakFluidRegistry
{
	public static final Map<Fluid, Integer> TMAP = new HashMap<>();
	static
	{
		addTokamakFuel(FluidRegistry.WATER, 1);
		addTokamakFuel(FluidsAR.HEAVY_WATER, 10);
	}
	
	public static void addTokamakFuel(Fluid fluid, int coef)
	{
		TMAP.put(fluid, coef);
	}
	
	public static int getFuelRate(Fluid fl)
	{
		return TMAP.getOrDefault(fl, 0);
	}
	
	public static boolean canAcceptFluidItem(ItemStack stack)
	{
		FluidStack fs = FluidUtil.getFluidContained(stack);
		return fs != null && TMAP.containsKey(fs.getFluid());
	}
}