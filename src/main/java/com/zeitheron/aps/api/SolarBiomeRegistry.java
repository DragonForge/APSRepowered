package com.zeitheron.aps.api;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import net.minecraft.init.Biomes;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Used to override biomes to apply custom solar generation. Use for hot biomes to make reflectors gen more FE
 */
public class SolarBiomeRegistry
{
	private static final Map<Biome, Float> MAP = new HashMap<>();
	
	static
	{
		registerForExisting(biome ->
		{
			if(biome == Biomes.DESERT || biome == Biomes.DESERT_HILLS || biome == Biomes.MUTATED_DESERT)
				return 2F;
			else if(biome == Biomes.EXTREME_HILLS || biome == Biomes.EXTREME_HILLS_EDGE || biome == Biomes.EXTREME_HILLS_WITH_TREES || biome == Biomes.MUTATED_EXTREME_HILLS || biome == Biomes.MUTATED_EXTREME_HILLS_WITH_TREES)
				return 1.5F;
			else if(biome == Biomes.PLAINS || biome == Biomes.MUTATED_PLAINS)
				return 1.25F;
			else if(biome == Biomes.FOREST || biome == Biomes.FOREST_HILLS || biome == Biomes.BIRCH_FOREST || biome == Biomes.BIRCH_FOREST_HILLS || biome == Biomes.MUTATED_BIRCH_FOREST || biome == Biomes.MUTATED_BIRCH_FOREST_HILLS || biome == Biomes.MUTATED_FOREST || biome == Biomes.MUTATED_ROOFED_FOREST || biome == Biomes.ROOFED_FOREST)
				return .75F;
			else if(biome.isHighHumidity())
				return .75F;
			else if(biome == Biomes.TAIGA || biome == Biomes.TAIGA_HILLS || biome == Biomes.MUTATED_TAIGA)
				return .75F;
			else if(biome.isSnowyBiome())
				return .5F;
			
			return null;
		});
	}
	
	public static void registerForExisting(Function<Biome, Float> func)
	{
		GameRegistry.findRegistry(Biome.class).getValuesCollection().forEach(b ->
		{
			Float f = func.apply(b);
			if(f != null)
				register(f.floatValue(), b);
		});
	}
	
	public static void register(float multiplier, Biome... biomes)
	{
		for(Biome b : biomes)
			MAP.put(b, multiplier);
	}
	
	public static float getMult(Biome bio)
	{
		return MAP.getOrDefault(bio, 1F);
	}
}