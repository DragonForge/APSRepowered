package com.zeitheron.aps.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraftforge.oredict.OreIngredient;

public class MagmafierMatterRegistry
{
	public static final List<TwoTuple<Ingredient, Integer>> recipes = new ArrayList<>();
	
	static
	{
		addMatter("dirt", 2);
		addMatter("cobblestone", 1);
		addMatter("stone", 2);
		addMatter("gravel", 3);
		addMatter("netherrack", 4);
		addMatter("obsidian", 20);
	}
	
	public static void addMatter(ItemStack item, int matter)
	{
		addMatter(Ingredient.fromStacks(item), matter);
	}
	
	public static void addMatter(String od, int matter)
	{
		addMatter(new OreIngredient(od), matter);
	}
	
	public static void addMatter(Ingredient ingred, int matter)
	{
		recipes.add(new TwoTuple<>(ingred, matter));
	}
	
	public static int getMatterIndex(ItemStack item)
	{
		Optional<TwoTuple<Ingredient, Integer>> query = recipes.stream().filter(p -> p.get1().apply(item)).findFirst();
		return query.isPresent() ? recipes.indexOf(query.get()) : -1;
	}
	
	public static int getMatterValue(int index)
	{
		if(index < 0 || index >= recipes.size())
			return 0;
		return recipes.get(index).get2().intValue();
	}
	
	public static void removeMatter(ItemStack item)
	{
		while(true)
		{
			int index = getMatterIndex(item);
			if(index < 0)
				return;
			recipes.remove(index);
		}
	}
}