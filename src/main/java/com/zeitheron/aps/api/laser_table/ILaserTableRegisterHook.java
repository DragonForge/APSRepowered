package com.zeitheron.aps.api.laser_table;

public interface ILaserTableRegisterHook
{
	void registerLaserTable();
}