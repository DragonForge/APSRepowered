package com.zeitheron.aps.api.laser_table;

import com.zeitheron.hammercore.utils.ConsumableItem;
import com.zeitheron.hammercore.utils.ItemStackUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public abstract class LaserTableRecipe
{
	public int FE;
	
	public LaserTableRecipe setFE(int fE)
	{
		FE = fE;
		return this;
	}
	
	public abstract ItemStack getOutput();
	
	public abstract ConsumableItem[] getInputs();
	
	public boolean canOutput(IInventory output)
	{
		ItemStack out = getOutput();
		return output.getStackInSlot(0).isEmpty() || (out.isItemEqual(output.getStackInSlot(0)) && out.getCount() + output.getStackInSlot(0).getCount() <= out.getMaxStackSize());
	}
	
	public boolean matches(IInventory inventory)
	{
		ConsumableItem[] in = getInputs();
		
		/** Can't fit */
		if(in.length > inventory.getSizeInventory())
			return false;
		
		InventoryDummy copy = new InventoryDummy(inventory.getSizeInventory());
		for(int i = 0; i < inventory.getSizeInventory(); ++i)
			copy.setInventorySlotContents(i, inventory.getStackInSlot(i).copy());
		
		for(ConsumableItem ci : in)
			if(!ci.consume(copy))
				return false;
			
		return true;
	}
	
	public boolean craft(IInventory input, IInventory output)
	{
		ConsumableItem[] in = getInputs();
		
		ItemStack out = getOutput();
		
		if(!(output.getStackInSlot(0).isEmpty() || (out.isItemEqual(output.getStackInSlot(0)) && out.getCount() + output.getStackInSlot(0).getCount() <= out.getMaxStackSize())))
			return false;
		
		InventoryDummy copy = new InventoryDummy(input.getSizeInventory());
		for(int i = 0; i < input.getSizeInventory(); ++i)
			copy.setInventorySlotContents(i, input.getStackInSlot(i).copy());
		
		for(ConsumableItem ci : in)
			if(!ci.consume(copy))
				return false;
			
		for(int i = 0; i < input.getSizeInventory(); ++i)
			input.setInventorySlotContents(i, copy.getStackInSlot(i));
		
		if(output.getStackInSlot(0).isEmpty())
			output.setInventorySlotContents(0, out.copy());
		else
			output.getStackInSlot(0).grow(out.getCount());
		
		return true;
	}
	
	public NonNullList<ItemStack> getConsumedItemsFromInputInventory(IInventory input)
	{
		InventoryDummy temp = new InventoryDummy(1);
		NonNullList<ItemStack> stacks = NonNullList.create();
		ConsumableItem[] in = getInputs();
		IntArrayList blk = new IntArrayList();
		for(ConsumableItem it : in)
			for(int i = 0; i < input.getSizeInventory(); ++i)
			{
				if(blk.contains(i))
					continue;
				ItemStack found = ItemStackUtil.compact(input, input.getStackInSlot(i), blk);
				temp.setInventorySlotContents(0, found);
				if(it.canConsume(temp))
					stacks.add(found);
			}
		return stacks;
	}
}