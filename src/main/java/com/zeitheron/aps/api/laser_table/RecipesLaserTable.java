package com.zeitheron.aps.api.laser_table;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.utils.ConsumableItem;

import net.minecraft.block.Block;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class RecipesLaserTable
{
	public static final List<LaserTableRecipe> RECIPES = new ArrayList<>();
	
	@Nullable
	public static LaserTableRecipe search(IInventory inv, @Nullable IInventory out)
	{
		int s = RECIPES.size();
		for(int i = 0; i < s; ++i)
		{
			LaserTableRecipe r = RECIPES.get(i);
			if(r.matches(inv) && (out == null || r.canOutput(out)))
				return r;
		}
		return null;
	}
	
	public static void add(Item output, int energy, ConsumableItem... ingredients)
	{
		add(new ItemStack(output), energy, ingredients);
	}
	
	public static void add(Block output, int energy, ConsumableItem... ingredients)
	{
		add(new ItemStack(output), energy, ingredients);
	}
	
	public static void add(ItemStack output, int energy, ConsumableItem... ingredients)
	{
		RECIPES.add(new LaserTableRecipe()
		{
			@Override
			public ItemStack getOutput()
			{
				return output;
			}
			
			@Override
			public ConsumableItem[] getInputs()
			{
				return ingredients;
			}
		}.setFE(energy));
	}
}