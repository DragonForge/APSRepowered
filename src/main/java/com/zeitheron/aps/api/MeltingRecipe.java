package com.zeitheron.aps.api;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.zeitheron.hammercore.utils.OnetimeCaller;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

public class MeltingRecipe
{
	public static final Map<Item, Function<ItemStack, FluidStack>> MELTING_RECIPES = new HashMap<>();
	public static final int BUCKET = Fluid.BUCKET_VOLUME;
	
	public static final OnetimeCaller init = new OnetimeCaller(MeltingRecipe::$init);
	
	private static void $init()
	{
		addLavaMelting(new ItemStack(Blocks.STONE), BUCKET);
		addLavaMelting(new ItemStack(Blocks.COBBLESTONE), BUCKET);
		addLavaMelting(new ItemStack(Blocks.GRAVEL), BUCKET / 2);
		addLavaMelting(new ItemStack(Blocks.NETHERRACK), BUCKET);
		addLavaMelting(new ItemStack(Blocks.MAGMA), BUCKET);
		addLavaMelting(new ItemStack(Blocks.OBSIDIAN), BUCKET);
		addLavaMelting(new ItemStack(Blocks.SOUL_SAND), BUCKET / 2);
	}
	
	public static void addMelting(ItemStack stack, Fluid fluid, int amount)
	{
		addCustomMelting(stack.getItem(), s -> stack.isItemEqual(s) ? new FluidStack(fluid, amount) : null);
	}
	
	public static void addLavaMelting(ItemStack stack, int amount)
	{
		addCustomMelting(stack.getItem(), s -> stack.isItemEqual(s) ? new FluidStack(FluidRegistry.LAVA, amount) : null);
	}
	
	public static void addWaterMelting(ItemStack stack, int amount)
	{
		addCustomMelting(stack.getItem(), s -> stack.isItemEqual(s) ? new FluidStack(FluidRegistry.WATER, amount) : null);
	}
	
	public static void addCustomMelting(Item item, Function<ItemStack, FluidStack> fluid)
	{
		MELTING_RECIPES.put(item, fluid);
	}
}