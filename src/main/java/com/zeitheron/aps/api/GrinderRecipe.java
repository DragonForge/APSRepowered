package com.zeitheron.aps.api;

import java.util.Arrays;
import java.util.LinkedList;

import com.zeitheron.hammercore.utils.OnetimeCaller;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.OreIngredient;

public class GrinderRecipe
{
	public static final LinkedList<GrinderRecipe> GRINDER_RECIPES = new LinkedList<GrinderRecipe>();
	
	public static final OnetimeCaller init = new OnetimeCaller(GrinderRecipe::$init);
	
	private static void $init()
	{
		new GrinderRecipe(new ItemStack(Blocks.GRAVEL), new ItemStack(Items.FLINT), 100, 100).add();
		
		new GrinderRecipe(new ItemStack(Blocks.SANDSTONE), new ItemStack(Blocks.SAND, 4), 100, 200).add();
		new GrinderRecipe(new ItemStack(Blocks.BRICK_BLOCK), new ItemStack(Items.BRICK, 4), 100, 300).add();
		new GrinderRecipe(new OreIngredient("plankWood"), 1, new ItemStack(Items.STICK, 5), 100, 200).add();
		new GrinderRecipe(new ItemStack(Blocks.WOOL, 1, OreDictionary.WILDCARD_VALUE), new ItemStack[] { new ItemStack(Items.STRING), new ItemStack(Items.STRING, 2), new ItemStack(Items.STRING, 4) }, new float[] { 25, 25, 25 }, 200).add();
		new GrinderRecipe(new OreIngredient("dirt"), 1, new ItemStack[] { new ItemStack(Blocks.COBBLESTONE), new ItemStack(Blocks.STONE), new ItemStack(Items.COAL), new ItemStack(MetalRegistry.getMetal("iron").get(EnumItemMetalPart.NUGGET)) }, new float[] { 20, 4, 1F, 1.5F }, 200).add();
		new GrinderRecipe(new OreIngredient("cobblestone"), 1, new ItemStack[] { new ItemStack(Blocks.GRAVEL), new ItemStack(Items.COAL), new ItemStack(MetalRegistry.getMetal("iron").get(EnumItemMetalPart.DUST)), new ItemStack(Items.REDSTONE) }, new float[] { 75, 2.5f, 2, 0.5f }, 300).add();
		new GrinderRecipe(new OreIngredient("stone"), 1, new ItemStack[] { new ItemStack(Blocks.GRAVEL), new ItemStack(MetalRegistry.getMetal("iron").get(EnumItemMetalPart.DUST)), new ItemStack(Items.REDSTONE) }, new float[] { 50, 2, 0.5f }, 300).add();
		
		new GrinderRecipe(new ItemStack(Items.FLINT), new ItemStack[] { new ItemStack(Blocks.SAND), new ItemStack(Items.CLAY_BALL), new ItemStack(Items.GUNPOWDER) }, new float[] { 60, 37, 3 }, 300).add();
		
		new GrinderRecipe(new ItemStack(Blocks.MAGMA), new ItemStack[] { new ItemStack(Items.MAGMA_CREAM, 2), new ItemStack(Items.MAGMA_CREAM, 1), new ItemStack(Items.MAGMA_CREAM, 1) }, new float[] { 100F, 50F, 25F }, 500).add();
		new GrinderRecipe(new ItemStack(Blocks.GLOWSTONE), new ItemStack(Items.GLOWSTONE_DUST, 4), 100F, 500).add();
		new GrinderRecipe(new ItemStack(Blocks.SEA_LANTERN), new ItemStack[] { new ItemStack(Items.PRISMARINE_CRYSTALS, 3), new ItemStack(Items.PRISMARINE_CRYSTALS, 2), new ItemStack(Items.PRISMARINE_SHARD, 3), new ItemStack(Items.PRISMARINE_SHARD, 1) }, new float[] { 100F, 75F, 100F, 75F }, 500).add();
		new GrinderRecipe(new OreIngredient("blockGlass"), 1, new ItemStack(Blocks.SAND), 100F, 100).add();
		
		// Add ore processing
		for(String ore : OreDictionary.getOreNames())
		{
			String metal = null;
			if(ore.startsWith("ore"))
				metal = ore.substring(3);
			if(ore.startsWith("gem"))
				metal = ore.substring(3);
			if(ore.startsWith("ingot"))
				metal = ore.substring(5);
			if(ore.startsWith("plate"))
				metal = ore.substring(5);
			if(metal == null || OreDictionary.getOres("dust" + metal).isEmpty() || OreDictionary.getOres(ore).isEmpty())
				continue;
			
			ItemStack out = ItemStack.EMPTY;
			
			MetalData md = MetalRegistry.getMetal(metal);
			if(md != null && md.has(EnumItemMetalPart.DUST))
				out = new ItemStack(md.get(EnumItemMetalPart.DUST));
			else
				out = OreDictionary.getOres("dust" + metal).get(0);
			
			if(out.isEmpty())
				continue;
			
			out.setCount(out.getCount() * (ore.startsWith("ore") ? 2 : 1));
			new GrinderRecipe(new OreIngredient(ore), 1, out, 100, 600).add();
		}
	}
	
	public GrinderRecipe add()
	{
		if(!GRINDER_RECIPES.contains(this))
			GRINDER_RECIPES.add(this);
		return this;
	}
	
	public GrinderRecipe remove()
	{
		if(GRINDER_RECIPES.contains(this))
			GRINDER_RECIPES.remove(this);
		return this;
	}
	
	public Ingredient ingredient;
	public int amount;
	
	public ItemStack[] outputs;
	public float[] chances;
	public int energy;
	
	public GrinderRecipe(ItemStack input, ItemStack[] outputs, float[] chances, int fe)
	{
		ingredient = Ingredient.fromStacks(input);
		amount = input.getCount();
		
		this.outputs = outputs;
		if(chances.length != outputs.length)
		{
			String s1 = ", Inputs: ";
			for(ItemStack s : outputs)
				s1 += s.getTranslationKey();
			
			s1 += ", Chances: ";
			
			for(float f : chances)
				s1 += f + ":";
			
			new Exception("GrinderRecipe: outputs array and chance array not same length! \n Info:\n" + input.getTranslationKey() + s1).printStackTrace();
		}
		this.chances = chances;
		energy = fe;
	}
	
	public GrinderRecipe(ItemStack input, ItemStack[] outputs, float chance, int fe)
	{
		ingredient = Ingredient.fromStacks(input);
		amount = input.getCount();
		
		this.outputs = outputs;
		chances = new float[outputs.length];
		Arrays.fill(chances, chance);
		energy = fe;
	}
	
	public GrinderRecipe(ItemStack input, ItemStack output, float chance, int fe)
	{
		ingredient = Ingredient.fromStacks(input);
		amount = input.getCount();
		
		outputs = new ItemStack[] { output };
		chances = new float[] { chance };
		energy = fe;
	}
	
	public GrinderRecipe(Ingredient input, int count, ItemStack[] outputs, float[] chances, int fe)
	{
		ingredient = input;
		amount = count;
		
		this.outputs = outputs;
		if(chances.length != outputs.length)
		{
			String s1 = ", Inputs: ";
			for(ItemStack s : outputs)
				s1 += s.getTranslationKey();
			
			s1 += ", Chances: ";
			
			for(float f : chances)
				s1 += f + ":";
			
			new Exception("GrinderRecipe: outputs array and chance array not same length! \n Info:\n" + input + s1).printStackTrace();
		}
		this.chances = chances;
		energy = fe;
	}
	
	public GrinderRecipe(Ingredient input, int count, ItemStack[] outputs, float chance, int fe)
	{
		ingredient = input;
		amount = count;
		
		this.outputs = outputs;
		chances = new float[outputs.length];
		Arrays.fill(chances, chance);
		energy = fe;
	}
	
	public GrinderRecipe(Ingredient input, int count, ItemStack output, float chance, int fe)
	{
		ingredient = input;
		amount = count;
		
		outputs = new ItemStack[] { output };
		chances = new float[] { chance };
		energy = fe;
	}
	
	public boolean isIngredient(ItemStack i)
	{
		if(i.isEmpty())
			return false;
		else
			return ingredient.apply(i);
	}
	
	public boolean isProduct(ItemStack i)
	{
		if(i.isEmpty())
			return false;
		for(ItemStack output : outputs)
			if(output.isItemEqual(i))
				return true;
		return false;
	}
	
	public Ingredient getIngredient()
	{
		return ingredient;
	}
	
	public int getEnergyRequired()
	{
		return energy;
	}
	
	public ItemStack[] getAllOutputs()
	{
		return outputs;
	}
	
	public ItemStack getRandomOutput(World world)
	{
		float r = world.rand.nextFloat() * 100F;
		float c = 0;
		for(int i = 0; i < outputs.length; i++)
			if(outputs.length > 1)
			{
				if(chances.length > 1)
				{
					if(r < c + chances[i])
						return outputs[i].copy();
					else
						c += chances[i];
				} else
				{
					if(r < c + chances[0])
						return outputs[i].copy();
					else
						c += chances[0];
				}
			} else
				return outputs[0].copy();
		return ItemStack.EMPTY;
	}
	
	public String[] getPossibleOutputs()
	{
		ItemStack[] outs = getAllOutputs();
		String[] Output = new String[outs.length];
		for(int i = 0; i < outs.length; i++)
			Output[i] = outs[i].getCount() + "x " + outs[i].getDisplayName() + " (" + chances[i] + "%)";
		return Output;
	}
}