package com.zeitheron.aps.blocks;

import com.zeitheron.aps.blocks.tiles.TileLaserTable;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.WorldUtil;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockLaserTable
		extends Block
		implements ITileBlock<TileLaserTable>, ITileEntityProvider
{
	public static final PropertyBool LOWER = PropertyBool.create("lower");

	public BlockLaserTable()
	{
		super(Material.IRON);
		MinecraftForge.EVENT_BUS.register(this);
		setSoundType(SoundType.METAL);
		setHardness(4F);
		setHarvestLevel("pickaxe", 1);
		setTranslationKey("laser_table");
	}

	@Override
	public Class<TileLaserTable> getTileClass()
	{
		return TileLaserTable.class;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return meta < 4 ? new TileLaserTable() : null;
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		return getDefaultState()
				.withProperty(EnumRotation.ROTATION, EnumRotation.values()[meta % 4])
				.withProperty(LOWER, meta < 4);
	}

	@Override
	public int getMetaFromState(IBlockState state)
	{
		return state.getValue(EnumRotation.ROTATION).ordinal() + (state.getValue(LOWER) ? 0 : 4);
	}

	@Override
	protected BlockStateContainer createBlockState()
	{
		return new BlockStateContainer(this, EnumRotation.ROTATION, LOWER);
	}

	@Override
	public boolean canPlaceBlockAt(World worldIn, BlockPos pos)
	{
		return super.canPlaceBlockAt(worldIn, pos) && super.canPlaceBlockAt(worldIn, pos.up());
	}

	@Override
	public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer, EnumHand hand)
	{
		EnumRotation rot = EnumRotation.valueOf(placer.getHorizontalFacing().getOpposite().name());
		world.setBlockState(pos.up(), getDefaultState().withProperty(LOWER, false).withProperty(EnumRotation.ROTATION, rot));
		return getDefaultState().withProperty(LOWER, true).withProperty(EnumRotation.ROTATION, rot);
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		if(!state.getValue(LOWER) && worldIn.getBlockState(pos.down()).getBlock() == this)
			worldIn.destroyBlock(pos.down(), true);
		if(state.getValue(LOWER) && worldIn.getBlockState(pos.up()).getBlock() == this)
			worldIn.destroyBlock(pos.up(), true);
		super.breakBlock(worldIn, pos, state);
	}

	@Override
	public void getDrops(NonNullList<ItemStack> drops, IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		if(state.getValue(LOWER))
			super.getDrops(drops, world, pos, state, fortune);
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		if(!state.getValue(LOWER))
			return onBlockActivated(worldIn, pos.down(), worldIn.getBlockState(pos.down()), playerIn, hand, facing, hitX, hitY, hitZ);
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
}