package com.zeitheron.aps.blocks;

import com.zeitheron.aps.blocks.tiles.TileFuelGenerator;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockEnableable;
import com.zeitheron.hammercore.internal.blocks.base.IBlockHorizontal;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockFuelGenerator extends BlockDeviceHC<TileFuelGenerator> implements IBlockHorizontal, IBlockEnableable
{
	public BlockFuelGenerator()
	{
		super(Material.IRON, TileFuelGenerator.class, "fuel_generator");
		setHardness(3F);
		setSoundType(SoundType.METAL);
	}

	@Override
	public boolean enableableDefault()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}
	
	@Override
	protected void updateState(World worldIn, BlockPos pos, IBlockState state)
	{
	}
}