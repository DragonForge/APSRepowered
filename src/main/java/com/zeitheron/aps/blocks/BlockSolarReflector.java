package com.zeitheron.aps.blocks;

import java.util.Random;

import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.blocks.tiles.TileSolarReflector;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockEnableable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockSolarReflector extends BlockDeviceHC<TileSolarReflector> implements IBlockEnableable
{
	public BlockSolarReflector()
	{
		super(Material.IRON, TileSolarReflector.class, "solar_reflector");
		setSoundType(SoundType.METAL);
		setHardness(3);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		TileSolarReflector tile = WorldUtil.cast(worldIn.getTileEntity(pos), TileSolarReflector.class);
		// Draw beam? NAAAH, I'm too lazy to do that
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileMachine m = WorldUtil.cast(worldIn.getTileEntity(pos), TileMachine.class);
		if(m != null)
			m.destroy();
		super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	protected void updateState(World worldIn, BlockPos pos, IBlockState state)
	{
	}
}