package com.zeitheron.aps.blocks;

import java.lang.reflect.Field;

import com.zeitheron.hammercore.internal.blocks.IItemBlock;
import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class BlockBurntGlass extends Block implements IRegisterListener, IItemBlock
{
	public BlockBurntGlass()
	{
		super(Material.ROCK);
		setTranslationKey("burnt_glass");
		setSoundType(SoundType.GLASS);
		setHarvestLevel("pickaxe", 1);
		setHardness(3F);
	}
	
	@Override
	public void onRegistered()
	{
		OreDictionary.registerOre("blockGlassHardened", this);
	}
	
	@Override
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}
	
	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		Block block = blockAccess.getBlockState(pos.offset(side)).getBlock();
		return block == this ? false : super.shouldSideBeRendered(blockState, blockAccess, pos, side);
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	public final ItemBlockBurntGlass item = new ItemBlockBurntGlass();
	
	@Override
	public ItemBlock getItemBlock()
	{
		return item;
	}
	
	public class ItemBlockBurntGlass extends ItemBlock
	{
		public ItemBlockBurntGlass()
		{
			super(BlockBurntGlass.this);
		}
		
		@Override
		public Entity createEntity(World world, Entity entity, ItemStack itemstack)
		{
			Field isImmuneToFire = Entity.class.getDeclaredFields()[54];
			isImmuneToFire.setAccessible(true);
			try
			{
				isImmuneToFire.setBoolean(entity, true);
			} catch(IllegalArgumentException | IllegalAccessException e)
			{
			}
			return null;
		}
		
		@Override
		public boolean hasCustomEntity(ItemStack stack)
		{
			return true;
		}
	}
}