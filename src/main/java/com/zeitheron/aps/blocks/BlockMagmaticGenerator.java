package com.zeitheron.aps.blocks;

import com.zeitheron.aps.blocks.tiles.TileMagmaticGenerator;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockEnableable;
import com.zeitheron.hammercore.internal.blocks.base.IBlockHorizontal;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidUtil;

public class BlockMagmaticGenerator extends BlockDeviceHC<TileMagmaticGenerator> implements IBlockHorizontal, IBlockEnableable
{
	public BlockMagmaticGenerator()
	{
		super(Material.IRON, TileMagmaticGenerator.class, "magmatic_generator");
		setHardness(3F);
		setSoundType(SoundType.METAL);
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileMagmaticGenerator tbf = WorldUtil.cast(worldIn.getTileEntity(pos), TileMagmaticGenerator.class);
		if(tbf != null)
		{
			ItemStack held = playerIn.getHeldItem(hand);
			FluidActionResult far = FluidUtil.tryEmptyContainer(held, tbf, tbf.fuelTank.getCapacity(), playerIn, true);
			if(far.isSuccess())
			{
				if(!playerIn.isCreative())
					playerIn.setHeldItem(hand, far.getResult());
				HCNet.swingArm(playerIn, hand);
				return true;
			}
		}
		
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}
	
	@Override
	protected void updateState(World worldIn, BlockPos pos, IBlockState state)
	{
	}
}