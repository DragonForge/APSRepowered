package com.zeitheron.aps.blocks.solar;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.zeitheron.aps.api.SolarBiomeRegistry;
import com.zeitheron.aps.blocks.tiles.TileSolarCollector;
import com.zeitheron.aps.blocks.tiles.TileSolarReflector;
import com.zeitheron.aps.cfg.SolarConfig;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

public class SolarManager
{
	public final LinkedList<TileSolarReflector> Reflectors = new LinkedList<TileSolarReflector>();
	
	World w = null;
	public TileSolarCollector collector = null;
	BlockPos pos;
	
	public SolarManager(TileSolarCollector collector)
	{
		this.collector = collector;
	}
	
	public void worldSetup(World W, TileSolarCollector c)
	{
		if(w == null)
		{
			w = W;
			pos = c.getPos();
			collector = c;
		}
	}
	
	public void addReflector(TileSolarReflector Reflector)
	{
		if(!Reflectors.contains(Reflector))
			Reflectors.add(Reflector);
		Reflector.manager = this;
		collector.sendChangesToNearby();
		collector.setTooltipDirty(true);
	}
	
	public void remReflector(TileSolarReflector Reflector)
	{
		Reflectors.remove(Reflector);
		collector.setTooltipDirty(true);
	}
	
	public int getLightInput()
	{
		int Light = 0;
		List<TileSolarReflector> rem = null;
		for(TileSolarReflector ref : Reflectors)
		{
			if(ref.isInvalid() || ref.manager != this)
			{
				if(rem == null)
					rem = new ArrayList<>();
				rem.add(ref);
				continue;
			}
			
			Light += ref.getLightAmount();
		}
		if(rem != null)
		{
			Reflectors.removeAll(rem);
			collector.setTooltipDirty(true);
		}
		return (int) (Light * getTimeLightMult(w));
	}
	
	public float getPowerOutput()
	{
		return getLightInput() * SolarConfig.SolarBaseGen;
	}
	
	public static float getTimeLightMult(World w)
	{
		float base = w.isDaytime() ? 1 : 0;
		if(w.isThundering())
			base *= .6F;
		else if(w.isRaining())
			base *= .8F;
		return base;
	}
	
	public static float getBiomeLightMult(Biome biome)
	{
		return SolarBiomeRegistry.getMult(biome);
	}
	
	public void kill()
	{
		Reflectors.forEach(r -> r.assignManager(null));
	}
}