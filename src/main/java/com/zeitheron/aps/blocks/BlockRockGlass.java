package com.zeitheron.aps.blocks;

import com.zeitheron.aps.init.BlocksAR;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockRockGlass extends Block
{
	public BlockRockGlass()
	{
		super(Material.ROCK);
		setTranslationKey("rock_glass");
		setHardness(4F);
		setHarvestLevel("pickaxe", 1);
	}
	
	protected boolean tryTouchLava(World worldIn, BlockPos pos, IBlockState state)
	{
		for(EnumFacing enumfacing : EnumFacing.values())
			if(enumfacing != EnumFacing.DOWN && worldIn.getBlockState(pos.offset(enumfacing)).getMaterial() == Material.LAVA)
			{
				worldIn.setBlockState(pos, BlocksAR.BURNT_GLASS.getDefaultState(), 3);
				return true;
			}
		return false;
	}
	
	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
	{
		if(!tryTouchLava(worldIn, pos, state))
			super.neighborChanged(state, worldIn, pos, blockIn, fromPos);
	}
	
	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state)
	{
		if(!tryTouchLava(worldIn, pos, state))
			super.onBlockAdded(worldIn, pos, state);
	}
}