package com.zeitheron.aps.blocks.heavy_water;

import com.zeitheron.aps.InfoAR;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

public class FluidHeavyWater extends Fluid
{
	public FluidHeavyWater()
	{
		super("heavy_water", new ResourceLocation(InfoAR.MOD_ID, "blocks/heavy_water_still"), new ResourceLocation(InfoAR.MOD_ID, "blocks/heavy_water_flow"));
		setUnlocalizedName(InfoAR.MOD_ID + ":heavy_water");
		viscosity = 4000;
	}
	
	@Override
	public boolean doesVaporize(FluidStack fluidStack)
	{
		return false;
	}
}