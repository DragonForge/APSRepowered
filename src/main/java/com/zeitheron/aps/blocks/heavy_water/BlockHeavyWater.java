package com.zeitheron.aps.blocks.heavy_water;

import com.zeitheron.aps.init.FluidsAR;

import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.fluids.BlockFluidClassic;

public class BlockHeavyWater extends BlockFluidClassic
{
	public BlockHeavyWater()
	{
		super(FluidsAR.HEAVY_WATER, Material.WATER);
		setTranslationKey("heavy_water");
	}
	
	@Override
	public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
	{
	}
}