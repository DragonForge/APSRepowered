package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.APSRepowered;
import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.api.GrinderRecipe;
import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.client.gui.GuiGrinder;
import com.zeitheron.aps.inventory.ContainerGrinder;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.ItemStackUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileGrinder extends TileMachine implements ITooltipProviderHC, IEnergyStorage, IInventory
{
	public final InventoryDummy inventory = new InventoryDummy(1);
	public final FEEnergyStorage buffer = new FEEnergyStorage(20_000);
	
	public int energyProgress;
	
	public GrinderRecipe currentRecipe;
	
	public Runnable onSync;
	
	@Override
	public void tick()
	{
		if(isRecipeUpdateNecessary())
			updateRecipe();
		if(atTickRate(20))
			setMachineActive(currentRecipe != null);
		
		if(world.getRedstonePowerFromNeighbors(pos) > 0)
			return;
		
		if(atTickRate(400))
			sendChangesToNearby();
		
		boolean changed = false;
		
		if(canGrind() && !world.isRemote)
		{
			int top = currentRecipe.getEnergyRequired();
			int maxUse = Math.min(APSRepowered.getMaxEnergyConsumption(buffer, Math.max(1, Math.min(40, top / 40))), top - energyProgress);
			energyProgress += buffer.extractEnergy(maxUse, false);
			
			changed = true;
			
			if(energyProgress >= top)
			{
				energyProgress -= top;
				grindItem();
				sendChangesToNearby();
			}
		}
		
		if(changed && atTickRate(60))
			sendChangesToNearby();
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiGrinder(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerGrinder(player, this);
	}
	
	public String[] getPossibleOutputs()
	{
		if(currentRecipe == null)
			return new String[0];
		return currentRecipe.getPossibleOutputs();
	}
	
	public void updateRecipe()
	{
		currentRecipe = null;
		if(inventory.getStackInSlot(0).isEmpty())
			return;
		for(GrinderRecipe r : GrinderRecipe.GRINDER_RECIPES)
		{
			if(r.isIngredient(inventory.getStackInSlot(0)))
			{
				currentRecipe = r;
				break;
			}
		}
	}
	
	public boolean isRecipeUpdateNecessary()
	{
		return currentRecipe == null || !currentRecipe.isIngredient(inventory.getStackInSlot(0));
	}
	
	private boolean canGrind()
	{
		return !isRecipeUpdateNecessary();
	}
	
	public void grindItem()
	{
		ItemStack in = inventory.getStackInSlot(0);
		if(in.isEmpty())
			return;
			
		// int co = getRNG().nextInt(4) + 1;
		// for(int i = 0; i < co; ++i)
		// HCNet.spawnParticle(world, EnumParticleTypes.ITEM_CRACK, pos.getX() +
		// 5 / 16F + getRNG().nextFloat() * 3 / 8, pos.getY() + 1, pos.getZ() +
		// 1 / 16D + getRNG().nextFloat() * 3 / 8, 0, .1, 0,
		// Item.getIdFromItem(in.getItem()), in.getItemDamage());
		
		if(in.getItem().hasContainerItem(in))
			inventory.setInventorySlotContents(0, in.getItem().getContainerItem(in));
		else
			in.shrink(currentRecipe.amount);
		
		for(int i = 0; i < currentRecipe.outputs.length; ++i)
		{
			ItemStack out = currentRecipe.outputs[i];
			if(getRNG().nextInt(100000) / 1000F <= currentRecipe.chances[i])
				ItemStackUtil.ejectOrDrop(out.copy(), this);
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", inventory.writeToNBT(new NBTTagCompound()));
		nbt.setInteger("EnergyProgress", energyProgress);
		buffer.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		inventory.readFromNBT(nbt.getCompoundTag("Items"));
		energyProgress = nbt.getInteger("EnergyProgress");
		buffer.readFromNBT(nbt);
		setTooltipDirty(true);
		
		if(onSync != null)
			onSync.run();
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		int e = buffer.receiveEnergy(Math.min(500, maxReceive), simulate);
		if(e > 0)
			sendChangesToNearby();
		return e;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return buffer.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return buffer.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	private boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));
		
		String[] pos = getPossibleOutputs();
		if(pos.length > 0)
		{
			tip.newLine();
			tip.append(new TranslationTooltipInfo("gui." + InfoAR.MOD_ID + ".grinder_traces"));
			tip.append(new StringTooltipInfo(":"));
			for(String s : getPossibleOutputs())
			{
				tip.newLine();
				tip.append(new StringTooltipInfo(s));
			}
		}
	}
	
	@Override
	public String getName()
	{
		return "GRINDER";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public int getSizeInventory()
	{
		return inventory.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return inventory.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return inventory.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		setTooltipDirty(true);
		return inventory.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		setTooltipDirty(true);
		return inventory.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		inventory.setInventorySlotContents(index, stack);
		setTooltipDirty(true);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return inventory.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return inventory.isUsableByPlayer(player, pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return GrinderRecipe.GRINDER_RECIPES.stream().anyMatch(r -> r.isIngredient(stack));
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		inventory.clear();
	}
}