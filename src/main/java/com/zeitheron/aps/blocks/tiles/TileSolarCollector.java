package com.zeitheron.aps.blocks.tiles;

import java.util.Map;

import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.blocks.solar.SolarManager;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.PositionedSearching;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileSolarCollector extends TileMachine implements IEnergyStorage, ITooltipProviderHC
{
	public SolarManager Manager;
	
	int powerRefreshCounter = 0;
	int powerRefreshDelay = 90;
	
	int PowerOut;
	
	boolean Active;
	
	PositionedSearching<TileSolarReflector> reflectorSearch;
	
	public PositionedSearching<TileSolarReflector> getReflectorSearch()
	{
		if(reflectorSearch == null)
		{
			reflectorSearch = new PositionedSearching<>(pos -> WorldUtil.cast(world.getTileEntity(pos), TileSolarReflector.class), tile -> !tile.isInvalid() && (tile.manager == null || tile.manager == Manager || tile.manager.Reflectors.isEmpty()), TileSolarReflector.class);
			reflectorSearch.setCenter(pos.down(3));
			reflectorSearch.setRadius(12, 4, 12);
		}
		return reflectorSearch;
	}
	
	public TileSolarCollector()
	{
		Manager = new SolarManager(this);
		Active = true;
	}
	
	public FluidEnergyAccessPoint feSrc;
	
	@Override
	public void tick()
	{
		Active = world.getRedstonePowerFromNeighbors(pos) <= 0;
		
		Manager.worldSetup(world, this);
		int s = getReflectorSearch().located.size();
		getReflectorSearch().update(16);
		if(s != getReflectorSearch().located.size())
			getReflectorSearch().located.forEach(Manager::addReflector);
		
		float extracted = Manager.getPowerOutput();
		
		if(world.isRemote)
			return;
		
		if(feSrc == null)
			feSrc = FluidEnergyAccessPoint.create(world, pos);
		
		if(PowerOut != extracted || ticksExisted == 20)
		{
			PowerOut = (int) extracted;
			sendChangesToNearby();
		}
		
		if(Active)
			feSrc.emitEnergy((int) extracted);
	}
	
	public Vec3d getBeamFocus(float X, float Z)
	{
		int xCoord = pos.getX(), yCoord = pos.getY(), zCoord = pos.getZ();
		
		X -= xCoord;
		Z -= zCoord;
		
		if(X >= 0)
		{
			if(Math.abs(X) >= Math.abs(Z))
				return new Vec3d(xCoord + 1.0f, yCoord + 0.5f, zCoord + 0.5f);
			else if(Z >= 0)
				return new Vec3d(xCoord + 0.5f, yCoord + 0.5f, zCoord + 1.0f);
			else
				return new Vec3d(xCoord + 0.5f, yCoord + 0.5f, zCoord + 0.0f);
		} else
		{
			if(Math.abs(X) >= Math.abs(Z))
				return new Vec3d(xCoord + 0.0f, yCoord + 0.5f, zCoord + 0.5f);
			else if(Z >= 0)
				return new Vec3d(xCoord + 0.5f, yCoord + 0.5f, zCoord + 1.0f);
			else
				return new Vec3d(xCoord + 0.5f, yCoord + 0.5f, zCoord + 0.0f);
		}
	}
	
	@Override
	public void destroy()
	{
		Manager.kill();
		Manager = null;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("PowerOut", PowerOut);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		PowerOut = nbt.getInteger("PowerOut");
		ticksExisted = 0;
	}
	
	@Override
	public void addProperties(Map<String, Object> properties, RayTraceResult trace)
	{
		properties.put("active", Active);
		properties.put("generation", PowerOut);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return 0;
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return PowerOut;
	}
	
	@Override
	public boolean canExtract()
	{
		return true;
	}
	
	@Override
	public boolean canReceive()
	{
		return false;
	}
	
	private boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("gui.hammercore.generation"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", (int) PowerOut)).appendColor(TextFormatting.GOLD));
		tip.append(new TranslationTooltipInfo("gui.hammercore.fept"));
	}
}