package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.BlockEnergyPipe;
import com.zeitheron.aps.pipes.IPipe;
import com.zeitheron.aps.pipes.energy.EnergyPipe;
import com.zeitheron.aps.pipes.energy.PipeEnergyAccessPoint;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.WorldUtil;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEnergyPipe
		extends TileSyncableTickable
		implements IPipe, ITooltipProviderHC
{
	public EnergyPipe pipe;
	public BlockEnergyPipe block;
	public PipeEnergyAccessPoint acessPoint;

	public boolean[] connects = new boolean[6];

	public TileEnergyPipe(BlockEnergyPipe block)
	{
		this.block = block;
	}

	public TileEnergyPipe()
	{
	}

	@Override
	public void tick()
	{
		if(block == null)
		{
			block = WorldUtil.cast(world.getBlockState(pos).getBlock(), BlockEnergyPipe.class);
			if(block == null)
			{
				world.removeTileEntity(pos);
				return;
			}
		}

		if(atTickRate(5)) updateConnections();

		if(acessPoint == null)
			acessPoint = new PipeEnergyAccessPoint(getWorld(), getPos());

		if(pipe == null)
			pipe = block.pipe.apply(this);

		pipe.update();
	}

	public void updateConnections()
	{
		boolean changed = false;
		for(EnumFacing f : EnumFacing.VALUES)
		{
			TileEntity te = world.getTileEntity(pos.offset(f));
			boolean c = te instanceof TileEnergyPipe;
			if(!c && te != null && te.hasCapability(CapabilityEnergy.ENERGY, f.getOpposite()))
			{
				IEnergyStorage st = te.getCapability(CapabilityEnergy.ENERGY, f.getOpposite());
				c = st != null;
			}
			if(connects[f.ordinal()] != c) changed = true;
			connects[f.ordinal()] = c;
		}
		if(changed)
		{
			IBlockState state;
			world.notifyBlockUpdate(pos, state = getWorld().getBlockState(pos), state, 3);
		}
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return super.hasCapability(capability, facing) || (pipe != null && pipe.hasCapability(capability, facing));
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return (pipe != null && pipe.hasCapability(capability, facing)) ? pipe.getCapability(capability, facing) : super.getCapability(capability, facing);
	}

	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setString("Block", block.getRegistryName().toString());
		if(pipe == null)
			return;
		NBTTagCompound n = new NBTTagCompound();
		pipe.write(n);
		nbt.setTag("Pipe", n);
	}

	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		block = WorldUtil.cast(GameRegistry.findRegistry(Block.class).getValue(new ResourceLocation(nbt.getString("Block"))), BlockEnergyPipe.class);
		if(block != null)
		{
			pipe = block.pipe.apply(this);
			pipe.read(nbt.getCompoundTag("Pipe"));
		}
	}

	public EnergyPipe getRelPipe(EnumFacing to)
	{
		return getPipe(world, pos.offset(to));
	}

	public static EnergyPipe getPipe(World world, BlockPos pos)
	{
		TileEnergyPipe tep = WorldUtil.cast(world.getTileEntity(pos), TileEnergyPipe.class);
		return tep != null ? tep.pipe : null;
	}

	@Override
	public boolean isConnectedTo(EnumFacing face)
	{
		return connects[face.ordinal()];
	}

	@Override
	public ResourceLocation getTex()
	{
		return pipe != null ? pipe.getPipeTexture() : null;
	}

	@Override
	public void kill()
	{
		world.removeTileEntity(pos);
		if(pipe != null && pipe.grid != null)
			pipe.grid.onMajorGridChange();
	}

	private boolean tdirty;

	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}

	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));

		tip.newLine();
		tip.append(new TranslationTooltipInfo("tooltip." + InfoAR.MOD_ID + ".pipe_type"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new TranslationTooltipInfo("tooltip." + InfoAR.MOD_ID + ".pipe_type_" + (block.extraction ? "ex" : "tr")));

		tip.newLine();
		tip.append(new TranslationTooltipInfo("tooltip." + InfoAR.MOD_ID + ".transfer"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", block.transfer)).appendColor(TextFormatting.LIGHT_PURPLE));
		tip.append(new TranslationTooltipInfo("gui.hammercore.fept"));
	}

	@Override
	public BlockPos coordinates()
	{
		return getPos();
	}

	@Override
	public World world()
	{
		return getWorld();
	}
}