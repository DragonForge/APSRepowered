package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.APSRepowered;
import com.zeitheron.aps.api.MagmafierMatterRegistry;
import com.zeitheron.aps.cfg.MachineConfig;
import com.zeitheron.aps.client.gui.GuiMagmafier;
import com.zeitheron.aps.inventory.ContainerBlastFurnace;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileMagmafier extends TileSyncableTickable implements ISidedInventory, IEnergyStorage, IFluidHandler
{
	public final FEEnergyStorage energy = new FEEnergyStorage(20_000);
	public final InventoryDummy inventory = new InventoryDummy(1);
	public final FluidTank lavaTank = new FluidTank(10 * Fluid.BUCKET_VOLUME);
	public int matter, energyProgress;
	
	public FluidEnergyAccessPoint hotspot;
	
	public Runnable onSync;
	
	@Override
	public void tick()
	{
		if(hotspot == null)
			hotspot = new FluidEnergyAccessPoint(getWorld(), getPos());
		
		if(world.getRedstonePowerFromNeighbors(pos) > 0)
			return;
		
		if(atTickRate(10) && !inventory.getStackInSlot(0).isEmpty())
			addMatter(inventory.getStackInSlot(0));
		
		if(atTickRate(400))
			sendChangesToNearby();
		
		boolean changed = false;
		if(matter > 0)
		{
			int top = MachineConfig.MagmafierEnergyPerOperation;
			int maxUse = Math.min(APSRepowered.getMaxEnergyConsumption(energy, 50), top - energyProgress);
			energyProgress += energy.extractEnergy(maxUse, false);
			if(maxUse > 0)
				changed = true;
			if(energyProgress >= top)
			{
				FluidStack lava = new FluidStack(FluidRegistry.LAVA, Fluid.BUCKET_VOLUME / 20);
				if(lavaTank.fill(lava, false) == lava.amount)
				{
					lavaTank.fill(lava, true);
					energyProgress -= top;
					matter--;
					changed = true;
				}
			}
		}
		
		if(lavaTank.getFluidAmount() > 0 && hotspot.emitFluid(lavaTank.getFluid()) > 0)
			changed = true;
		
		if(changed && atTickRate(80))
			sendChangesToNearby();
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiMagmafier(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerBlastFurnace(player, this);
	}
	
	public boolean addMatter(ItemStack stack)
	{
		int value = MagmafierMatterRegistry.getMatterValue(MagmafierMatterRegistry.getMatterIndex(stack));
		if(value <= 0)
			return false;
		int taken = 0;
		while(!stack.isEmpty())
		{
			if(matter + value > MachineConfig.MagmafierMatterLimit)
				break;
			matter += value;
			stack.shrink(1);
			sendChangesToNearby();
		}
		return taken > 0;
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Matter", matter);
		nbt.setInteger("EnergyProgress", energyProgress);
		energy.writeToNBT(nbt);
		nbt.setTag("Fluids", lavaTank.writeToNBT(new NBTTagCompound()));
		nbt.setTag("Items", inventory.writeToNBT(new NBTTagCompound()));
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		matter = nbt.getInteger("Matter");
		energyProgress = nbt.getInteger("EnergyProgress");
		energy.readFromNBT(nbt);
		lavaTank.readFromNBT(nbt.getCompoundTag("Fluids"));
		inventory.readFromNBT(nbt.getCompoundTag("Items"));
		
		if(onSync != null)
			onSync.run();
	}
	
	@Override
	public String getName()
	{
		return "AGC-MS BLAST FURNACE";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return true;
	}
	
	@Override
	public int getSizeInventory()
	{
		return inventory.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return inventory.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return inventory.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return inventory.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return inventory.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		inventory.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return inventory.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return inventory.isUsableByPlayer(player, getPos());
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return index == 0 && MagmafierMatterRegistry.getMatterValue(MagmafierMatterRegistry.getMatterIndex(stack)) > 0;
	}
	
	@Override
	public int getField(int id)
	{
		return id == 0 ? matter : 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
		if(id == 0)
			matter = Math.max(0, Math.min(value, MachineConfig.MagmafierMatterLimit));
	}
	
	@Override
	public int getFieldCount()
	{
		return 1;
	}
	
	@Override
	public void clear()
	{
		inventory.clear();
	}
	
	static final int[] slots = { 0 };
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return slots;
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return isItemValidForSlot(index, itemStackIn);
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return false;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		int rf = energy.receiveEnergy(Math.min(500, maxReceive), simulate);
		if(rf > 0)
			sendChangesToNearby();
		return rf;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return energy.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return energy.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY ? (T) this : capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ? (T) this : super.getCapability(capability, facing);
	}
	
	@Override
	public IFluidTankProperties[] getTankProperties()
	{
		return lavaTank.getTankProperties();
	}
	
	@Override
	public int fill(FluidStack resource, boolean doFill)
	{
		return 0;
	}
	
	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain)
	{
		return lavaTank.drain(resource, doDrain);
	}
	
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain)
	{
		return lavaTank.drain(maxDrain, doDrain);
	}
}