package com.zeitheron.aps.blocks.tiles;

import java.util.ArrayList;
import java.util.List;

import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.EnumRotation;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileFluidPump extends TileSyncableTickable implements IEnergyStorage, IFluidHandler
{
	public final FluidTank internalTank = new FluidTank(Fluid.BUCKET_VOLUME * 2); // Holds 2 buckets in the buffer
	public final FEEnergyStorage buffer = new FEEnergyStorage(20_000);
	public FluidEnergyAccessPoint flSrc;
	
	public Fluid lastPumpedFluid;
	List<Long> consumablePositions = new ArrayList<>();
	
	public boolean canAcceptFluid()
	{
		return internalTank.getFluidAmount() == 0;
	}
	
	public EnumFacing getFront()
	{
		return WorldUtil.getFacing(getWorld().getBlockState(getPos()));
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
			return;
		
		if(flSrc == null)
			flSrc = FluidEnergyAccessPoint.create(world, pos);
		
		if(internalTank.getFluid() != null && internalTank.getFluid().amount == 0)
			internalTank.setFluid(null);
		
		if(atTickRate(10) && buffer.extractEnergy(100, true) == 100 && canAcceptFluid())
		{
			if(consumablePositions.isEmpty())
				consumablePositions.add(pos.down().toLong());
			
			try
			{
				int index = getRNG().nextInt(consumablePositions.size());
				BlockPos pos = BlockPos.fromLong(consumablePositions.remove(index));
				buffer.extractEnergy(100, false);
				
				for(EnumFacing f : EnumFacing.VALUES)
				{
					BlockPos npos = pos.offset(f);
					IBlockState state = world.getBlockState(npos);
					
					IFluidHandler handler = FluidUtil.getFluidHandler(world, npos, null);
					if(handler != null && world.getTileEntity(npos) == null)
					{
						FluidStack fs = handler.drain(Fluid.BUCKET_VOLUME, false);
						if(fs != null && fs.amount > 0 && consumablePositions.size() < 16 * 1024) // Limit to 16K positions!
							consumablePositions.add(npos.toLong());
					}
				}
				
				IBlockState state = world.getBlockState(pos);
				Block blo = state.getBlock();
				
				IFluidHandler handler = FluidUtil.getFluidHandler(world, pos, null);
				if(handler != null)
				{
					FluidStack fs = handler.drain(Fluid.BUCKET_VOLUME, false);
					
					if(fs != null)
					{
						int fill = internalTank.fill(fs.copy(), false);
						
						if(fill == Fluid.BUCKET_VOLUME)
						{
							handler.drain(Fluid.BUCKET_VOLUME, true);
							internalTank.fill(fs.copy(), true);
							
							if(fs.getFluid() != lastPumpedFluid)
							{
								lastPumpedFluid = fs.getFluid();
								sync();
							}
							
							return;
						}
					}
				}
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
		}
		
		if(atTickRate(2))
		{
			FluidStack stack = internalTank.getFluid();
			if(stack != null && stack.amount > 0)
			{
				int sent = flSrc.emitFluid(stack.copy());
				stack.amount -= sent;
			}
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Tank", internalTank.writeToNBT(new NBTTagCompound()));
		if(lastPumpedFluid != null)
			nbt.setString("LastFluid", FluidRegistry.getFluidName(lastPumpedFluid));
		buffer.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		if(nbt.hasKey("LastFluid"))
			lastPumpedFluid = FluidRegistry.getFluid(nbt.getString("LastFluid"));
		internalTank.readFromNBT(nbt.getCompoundTag("Tank"));
		buffer.readFromNBT(nbt);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public IFluidTankProperties[] getTankProperties()
	{
		return internalTank.getTankProperties();
	}
	
	@Override
	public int fill(FluidStack resource, boolean doFill)
	{
		return 0;
	}
	
	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain)
	{
		return internalTank.drain(resource, doDrain);
	}
	
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain)
	{
		return internalTank.drain(maxDrain, doDrain);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return buffer.receiveEnergy(maxReceive, simulate);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return buffer.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return buffer.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
}