package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.blocks.solar.SolarManager;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;

public class TileSolarReflector extends TileMachine
{
	boolean isConnected = false;
	public SolarManager manager = null;
	
	boolean Setup = false;
	float BiomePowerMult = 1.0f;
	
	public Vec3d beamFocus = Vec3d.ZERO;
	boolean beamActive = false;
	
	/* desert = 2.0 hell = 1.5 sky = 1.5 hills = 1.5 plains = 1.25 ocean = 1.0
	 * river = 1.0 mushroomIsland = 1.0 mushroomIslandShore = 1.0 forest = 0.75
	 * taiga = 0.75 swampland = 0.75 frozenOcean = 0.5 frozenRiver = 0.5
	 * icePlains = 0.25 iceMountains = 0.25 */
	
	public TileSolarReflector()
	{
	}
	
	void setup()
	{
		BiomePowerMult = SolarManager.getBiomeLightMult(world.getBiome(pos));
		
		Setup = true;
	}
	
	@Override
	public void tick()
	{
		if(world.isRemote)
			return;
		if(!Setup)
			setup();
		
		if(manager == null)
		{
			/* if(beamActive) { if(module_Solar.ShowBeams) {
			 * worldObj.setEntityDead(beam); beam = null; } beamActive = false;
			 * setTexture(1, 64); worldObj.markBlockAsNeedsUpdate(xCoord,
			 * yCoord, zCoord); } */
			
			if(beamActive)
			{
				/* if(module_Solar.ShowBeams) { worldObj.setEntityDead(beam);
				 * beam = null; } */
				beamActive = false;
			}
		} else
		{
			if(getIsExposed() && world.isDaytime() && manager.collector.Active)
			{
				if(!beamActive)
				{
					/* if(module_Solar.ShowBeams) { beam = new
					 * EntityLaser(worldObj); beam.setTexture(tex);
					 * beam.setPositions(xCoord + 0.5f, yCoord + 1.0f, zCoord +
					 * 0.5f, beamFocus[0], beamFocus[1], beamFocus[2]);
					 * worldObj.joinEntityInSurroundings(beam); } */
					beamActive = true;
				}
			} else if(beamActive)
			{
				/* if(module_Solar.ShowBeams) { worldObj.setEntityDead(beam);
				 * beam = null; } */
				beamActive = false;
			}
		}
		
		if(beamActive != isMachineActive())
			setMachineActive(beamActive);
	}
	
	public void assignManager(SolarManager Manager)
	{
		manager = Manager;
		
		if(manager == null)
			beamFocus = Vec3d.ZERO;
		else
			beamFocus = manager.collector.getBeamFocus(getPos().getX(), getPos().getZ());
		
		if(Manager != null)
			manager.addReflector(this);
	}
	
	public boolean getIsExposed()
	{
		return world.canBlockSeeSky(pos.up());
	}
	
	public float getLightAmount()
	{
		if(!getIsExposed())
			return 0.0f; // returns 0 if the reflector is covered
		return BiomePowerMult;
	}
	
	@Override
	public void destroy()
	{
		if(manager != null)
			manager.remReflector(this);
		invalidate();
		/* if(beam != null) { beam.setDead(); beam = null; } */
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
	}
}