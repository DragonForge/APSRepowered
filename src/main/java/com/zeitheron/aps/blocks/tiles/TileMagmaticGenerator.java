package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.api.TokamakFluidRegistry;
import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.cfg.MachineConfig;
import com.zeitheron.aps.client.gui.GuiMagmaticGenerator;
import com.zeitheron.aps.inventory.ContainerMagmaticGenerator;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.IFluidTankProperties;

public class TileMagmaticGenerator extends TileMachine implements IFluidHandler, IEnergyStorage, IInventory
{
	public final FluidTank fuelTank = new FluidTank(Fluid.BUCKET_VOLUME * 10);
	public final FEEnergyStorage energy = new FEEnergyStorage(20000);
	public final InventoryDummy items = new InventoryDummy(1);
	public int time = 0;
	public FluidEnergyAccessPoint hotspot;
	
	public Fluid renderFluid;
	
	@Override
	public void tick()
	{
		if(hotspot == null)
			hotspot = new FluidEnergyAccessPoint(getWorld(), getPos());
		
		if(atTickRate(10) && !world.isRemote)
			setMachineActive(renderFluid != null && time > 0);
		
		if(atTickRate(20))
		{
			ItemStack iis = items.getStackInSlot(0);
			if(!iis.isEmpty())
			{
				if(getBurnTime(FluidUtil.getFluidContained(iis)) > 0)
				{
					IFluidHandlerItem it = FluidUtil.getFluidHandler(iis);
					FluidStack fls = FluidUtil.getFluidContained(iis);
					
					Fluid fl = fls.getFluid();
					
					int amt = fls.amount;
					int fs = fill(fls.copy(), false);
					
					if(fs >= Fluid.BUCKET_VOLUME)
					{
						int fill = Math.min(amt, fs);
						it.drain(new FluidStack(fl, fill), true);
						fill(new FluidStack(fl, fill), true);
						items.setInventorySlotContents(0, it.getContainer().copy());
					}
				}
			}
		}
		
		if(time <= 0 && atTickRate(5))
		{
			int time = getBurnTime(fuelTank.getFluid());
			if(time > 0)
			{
				renderFluid = fuelTank.getFluid().getFluid();
				
				int maxDrain = MachineConfig.MagmaticGeneratorFluidConsumption;
				FluidStack ds = fuelTank.drain(maxDrain, true);
				int drained = ds != null ? ds.amount : 0;
				
				float factor = drained / (float) maxDrain;
				
				this.time = Math.round(time * 100 * factor);
				sendChangesToNearby();
			}
		}
		
		boolean changed = false;
		
		gen: if(time > 0)
		{
			float factor = Math.min(100, time) / 100F;
			int toEmit = (int) Math.floor(160 * factor);
			if(toEmit == 0)
				break gen;
			int emitted = energy.receiveEnergy(toEmit, false);
			float extractFactor = Math.max(emitted, 1) / (float) toEmit;
			int reduceBT = (int) Math.ceil(extractFactor * 100);
			time = Math.max(0, time - reduceBT);
			changed = true;
		}
		
		if(energy.getEnergyStored() > 0)
			changed |= energy.extractEnergy(hotspot.emitEnergy(energy.getEnergyStored()), false) > 0;
		
		if(changed && atTickRate(60))
			sendChangesToNearby();
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Fluids", fuelTank.writeToNBT(new NBTTagCompound()));
		nbt.setTag("Items", items.writeToNBT(new NBTTagCompound()));
		energy.writeToNBT(nbt);
		nbt.setInteger("BurnTime", time);
		if(renderFluid != null)
			nbt.setString("RenderFluid", renderFluid.getName());
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		fuelTank.readFromNBT(nbt.getCompoundTag("Fluids"));
		items.readFromNBT(nbt.getCompoundTag("Items"));
		energy.readFromNBT(nbt);
		time = nbt.getInteger("BurnTime");
		
		if(nbt.hasKey("RenderFluid"))
			renderFluid = FluidRegistry.getFluid(nbt.getString("RenderFluid"));
		else
			renderFluid = null;
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiMagmaticGenerator(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerMagmaticGenerator(player, this);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ? (T) this : super.getCapability(capability, facing);
	}
	
	@Override
	public IFluidTankProperties[] getTankProperties()
	{
		return fuelTank.getTankProperties();
	}
	
	@Override
	public int fill(FluidStack resource, boolean doFill)
	{
		if(getBurnTime(resource) > 0)
		{
			int i = fuelTank.fill(resource, doFill);
			if(i > 0)
				sendChangesToNearby();
			return i;
		}
		return 0;
	}
	
	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain)
	{
		return null;
	}
	
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain)
	{
		return null;
	}
	
	public static int getBurnTime(FluidStack fluid)
	{
		if(canBurn(fluid))
		{
			int tC = fluid.getFluid().getTemperature(fluid) - 273;
			int extra = tC - 1000;
			return 200 + extra / 2;
		}
		
		return 0;
	}
	
	public static boolean canBurn(FluidStack fluid)
	{
		return fluid != null && fluid.getFluid().getTemperature(fluid) - 273 > 1000;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return energy.extractEnergy(maxExtract, simulate);
	}
	
	@Override
	public int getEnergyStored()
	{
		return energy.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return energy.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return true;
	}
	
	@Override
	public boolean canReceive()
	{
		return false;
	}
	
	@Override
	public String getName()
	{
		return "AGC-ME MAGMATIC GEN";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return true;
	}
	
	@Override
	public int getSizeInventory()
	{
		return items.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return items.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return items.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return items.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return items.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		items.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 1;
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return items.isUsableByPlayer(player, getPos());
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return getBurnTime(FluidUtil.getFluidContained(stack)) > 0;
	}
	
	@Override
	public int getField(int id)
	{
		return id == 0 ? time : 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
		if(id == 0)
			time = Math.max(0, value);
	}
	
	@Override
	public int getFieldCount()
	{
		return 1;
	}
	
	@Override
	public void clear()
	{
		items.clear();
	}
	
	public EnumFacing getFront()
	{
		return WorldUtil.getFacing(getWorld().getBlockState(getPos()));
	}
}