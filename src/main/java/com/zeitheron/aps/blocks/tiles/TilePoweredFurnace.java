package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.APSRepowered;
import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.client.gui.GuiPoweredFurnace;
import com.zeitheron.aps.inventory.ContainerPoweredFurnace;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

public class TilePoweredFurnace extends TileMachine implements IEnergyStorage, ISidedInventory
{
	public static final int ENERGY_PROGESS = 2_000;
	
	public final FEEnergyStorage energy = new FEEnergyStorage(20000);
	public final InventoryDummy items = new InventoryDummy(2);
	public int energyProgress;
	
	@Override
	public void tick()
	{
		if(atTickRate(20))
			setMachineActive(canSmelt());
		
		if(world.getRedstonePowerFromNeighbors(pos) > 0)
			return;
		
		if(atTickRate(400))
			sendChangesToNearby();
		
		boolean changed = false;
		
		if(canSmelt() && !world.isRemote)
		{
			int maxUse = Math.min(APSRepowered.getMaxEnergyConsumption(energy, Math.max(1, Math.min(40, ENERGY_PROGESS / 40))), ENERGY_PROGESS - energyProgress);
			energyProgress += energy.extractEnergy(maxUse, false);
			
			changed = true;
			
			if(energyProgress >= ENERGY_PROGESS && !world.isRemote)
			{
				energyProgress -= ENERGY_PROGESS;
				smeltItem();
				sendChangesToNearby();
			}
		}
		
		if(changed && atTickRate(60))
			sendChangesToNearby();
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiPoweredFurnace(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerPoweredFurnace(player, this);
	}
	
	public boolean canSmelt()
	{
		if(items.getStackInSlot(0).isEmpty())
			return false;
		else
		{
			ItemStack itemstack = FurnaceRecipes.instance().getSmeltingResult(items.getStackInSlot(0));
			
			if(itemstack.isEmpty())
				return false;
			else
			{
				ItemStack itemstack1 = items.getStackInSlot(1);
				
				if(itemstack1.isEmpty())
					return true;
				else if(!itemstack1.isItemEqual(itemstack))
					return false;
				else if(itemstack1.getCount() + itemstack.getCount() <= this.getInventoryStackLimit() && itemstack1.getCount() + itemstack.getCount() <= itemstack1.getMaxStackSize())
					return true;
				else
					return itemstack1.getCount() + itemstack.getCount() <= itemstack.getMaxStackSize();
			}
		}
	}
	
	public void smeltItem()
	{
		if(canSmelt())
		{
			ItemStack itemstack = items.getStackInSlot(0);
			ItemStack itemstack1 = FurnaceRecipes.instance().getSmeltingResult(itemstack);
			ItemStack itemstack2 = items.getStackInSlot(1);
			
			if(itemstack2.isEmpty())
				items.setInventorySlotContents(1, itemstack1.copy());
			else if(itemstack2.getItem() == itemstack1.getItem())
				itemstack2.grow(itemstack1.getCount());
			
			itemstack.shrink(1);
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", items.writeToNBT(new NBTTagCompound()));
		energy.writeToNBT(nbt);
		nbt.setInteger("EnergyProgress", energyProgress);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		items.readFromNBT(nbt.getCompoundTag("Items"));
		energy.readFromNBT(nbt);
		energyProgress = nbt.getInteger("EnergyProgress");
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		int e = energy.receiveEnergy(Math.min(500, maxReceive), simulate);
		if(e > 0)
			sendChangesToNearby();
		return e;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return energy.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return energy.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	@Override
	public String getName()
	{
		return "AGC-PF POWERED FURNACE";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return true;
	}
	
	@Override
	public int getSizeInventory()
	{
		return items.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return items.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return items.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return items.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return items.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		items.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return items.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return items.isUsableByPlayer(player, getPos());
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return index == 0 && !FurnaceRecipes.instance().getSmeltingResult(stack).isEmpty();
	}
	
	@Override
	public int getField(int id)
	{
		return id == 0 ? energyProgress : 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
		if(id == 0)
			energyProgress = Math.max(0, value);
	}
	
	@Override
	public int getFieldCount()
	{
		return 1;
	}
	
	@Override
	public void clear()
	{
		items.clear();
	}
	
	public static final int[] SLOTS = { 0, 1 };
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return SLOTS;
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return index == 0 && isItemValidForSlot(index, itemStackIn);
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return index == 1;
	}
}