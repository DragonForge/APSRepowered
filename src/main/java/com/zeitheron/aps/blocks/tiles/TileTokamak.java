package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.api.TokamakFluidRegistry;
import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.cfg.TokamakConfig;
import com.zeitheron.aps.client.gui.GuiTokamak;
import com.zeitheron.aps.inventory.ContainerTokamak;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltip;
import com.zeitheron.hammercore.tile.tooltip.own.ITooltipProviderHC;
import com.zeitheron.hammercore.tile.tooltip.own.inf.ItemStackTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.StringTooltipInfo;
import com.zeitheron.hammercore.tile.tooltip.own.inf.TranslationTooltipInfo;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileTokamak extends TileMachine implements IInventory, IFluidHandler, IEnergyStorage, ITooltipProviderHC
{
	public final InventoryDummy inventory = new InventoryDummy(1);
	public final FluidTank fuelTank = new FluidTank(10 * Fluid.BUCKET_VOLUME);
	public FEEnergyStorage buffer = new FEEnergyStorage(TokamakConfig.TokamakMaxEnergyReceive);
	public FluidEnergyAccessPoint feSrc;
	
	int TokamakFusionTemp = (int) (TokamakConfig.TokamakMaxTemp * TokamakConfig.TokamakFusionFraction);
	
	public int TokamakIdlingBurnDelay = 0;
	
	public int TokamakTemp;
	public int BurnTimeRemaining;
	public boolean Idling;
	
	public float PowerIn;
	public float PowerOut;
	
	public int fusionTime;
	
	@Override
	public void tick()
	{
		if(atTickRate(40))
			sendChangesToNearby();
		
		if(feSrc == null)
			feSrc = FluidEnergyAccessPoint.create(world, pos);
		
		float i = PowerIn, o = PowerOut;
		
		PowerIn = 0;
		PowerOut = 0;
		
		Idling = world.getRedstonePowerFromNeighbors(pos) > 0 || !isMachineActive();
		
		ItemStack itemInInventory = inventory.getStackInSlot(0);
		
		if(!itemInInventory.isEmpty())
		{
			if(TokamakFluidRegistry.canAcceptFluidItem(itemInInventory))
			{
				IFluidHandlerItem it = FluidUtil.getFluidHandler(itemInInventory);
				FluidStack fls = FluidUtil.getFluidContained(itemInInventory);
				
				Fluid fl = fls.getFluid();
				
				int amt = fls.amount;
				int fs = fill(fls.copy(), false);
				
				if(fs >= Fluid.BUCKET_VOLUME)
				{
					int fill = Math.min(amt, fs);
					it.drain(new FluidStack(fl, fill), true);
					fill(new FluidStack(fl, fill), true);
					inventory.setInventorySlotContents(0, it.getContainer().copy());
				}
			}
		}
		
		boolean works = false;
		
		// If the tokamak is hot enough,
		if(TokamakTemp >= TokamakFusionTemp)
		{
			if(buffer.getEnergyStored() > 0)
				buffer.readFromNBT(new NBTTagCompound());
			
			works = true;
			
			// if it's still burning some fuel
			if(BurnTimeRemaining > 0)
			{
				// go burn that fuel some more
				Burn();
				if(!Idling)
				{
					// and decrease the burn time accordingly
					BurnTimeRemaining--;
					TokamakIdlingBurnDelay = 0;
				} else if(TokamakIdlingBurnDelay == 0)
				{
					BurnTimeRemaining--;
					TokamakIdlingBurnDelay = 10;
				} else
					TokamakIdlingBurnDelay--;
			} else // otherwise
			{
				// if there's enough fuel
				if(fuelTank.getFluidAmount() >= (Fluid.BUCKET_VOLUME * (TokamakConfig.TokamakWaterUse / 100F)))
				{
					// use up some more
					FluidStack fs = fuelTank.drain((int) (Fluid.BUCKET_VOLUME * (TokamakConfig.TokamakWaterUse / 100F)), true);
					// and fill up the burn time
					BurnTimeRemaining = fs != null ? TokamakFluidRegistry.getFuelRate(fs.getFluid()) : 1;
					Burn();
				}
			}
			
			if(Idling)
			{
				PowerIn = buffer.extractEnergy(TokamakConfig.TokamakMaxEnergyReceive / 4, false);
				TokamakTemp += PowerIn * TokamakConfig.TokamakEnergyToHeatingScalar;
			}
			
			if(fusionTime >= 115 && (fusionTime - 115) % 39 == 0)
			{
				if(world.isRemote && !Idling)
					getLocation().playSound(InfoAR.MOD_ID + ":tokamak.loop", .1F, 1F, SoundCategory.BLOCKS);
				fusionTime = 115;
			}
			
			++fusionTime;
		} else // or if it's not hot enough,
		{
			PowerIn = buffer.extractEnergy(TokamakConfig.TokamakMaxEnergyReceive, false);
			
			if(fusionTime > 0)
			{
				if(world.isRemote)
					getLocation().playSound(InfoAR.MOD_ID + ":tokamak.stop", .13F, 1F, SoundCategory.BLOCKS);
				fusionTime = 1;
			}
			
			// receive energy and add the equivalent heat to the tokamak
			TokamakTemp += PowerIn * TokamakConfig.TokamakEnergyToHeatingScalar;
			
			if(TokamakTemp >= TokamakFusionTemp)
			{
				if(!world.isRemote)
					getLocation().playSound(InfoAR.MOD_ID + ":tokamak.start", .1F, 1F, SoundCategory.BLOCKS);
				fusionTime = 1;
			}
		}
		
		// then apply the natural reactor cooling
		TokamakTemp -= TokamakConfig.TokamakCoolRate * ((float) TokamakTemp / (float) TokamakConfig.TokamakMaxTemp);
		
		// make sure the temperature is in range
		if(TokamakTemp > TokamakConfig.TokamakMaxTemp)
			TokamakTemp = TokamakConfig.TokamakMaxTemp;
		else if(TokamakTemp < 0)
			TokamakTemp = 0;
		
		if(world.isRemote)
		{
			PowerIn = i;
			PowerOut = o;
		} else if((o != PowerOut || i != PowerIn) && atTickRate(10))
			sendChangesToNearby();
		
		if(isMachineActive() != works)
			setMachineActive(works);
	}
	
	void Burn()
	{
		if(!Idling)
		{
			// then work out the current energy gradient
			float EnergyGradient = (float) (TokamakTemp - TokamakFusionTemp) / (float) (TokamakConfig.TokamakMaxTemp - TokamakFusionTemp);
			
			float ex = EnergyGradient * (float) Math.sqrt(TokamakConfig.TokamakGenLimit);
			
			// get how much energy we can give
			float extracted = (float) ex * ex;
			
			/* RawEnergy = TokamakTemp - TokamakFusionTemp MaxEnergy =
			 * TokamakMaxTemp - TokamakFusionTemp Energy = (RawEnergy/MaxEnergy)
			 * * FusionMaxEnergyGen Result, 0 energy generation at
			 * TokamakFusionTemp, FusionMaxEnergyGen (1000) energy generation at
			 * TokamakMaxTemp */
			if(extracted > 0) // if we can give some
				feSrc.emitEnergy((int) (PowerOut = extracted));
			
			// then heat the tokamak accordingly
			TokamakTemp += (EnergyGradient * TokamakConfig.TokamakHeatGenLimit) + (TokamakConfig.TokamakCoolRate * ((float) TokamakTemp / (float) TokamakConfig.TokamakMaxTemp)) + 1;
		} else
			TokamakTemp += TokamakConfig.TokamakCoolRate * TokamakConfig.TokamakFusionFraction;
		// Result, temp levels at TokamakFusionTemp, temp increases by
		// FusionMaxHeatGen at TokamakMaxTemp
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiTokamak(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerTokamak(player, this);
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", inventory.writeToNBT(new NBTTagCompound()));
		nbt.setTag("Tank", fuelTank.writeToNBT(new NBTTagCompound()));
		nbt.setInteger("BurnRemaining", BurnTimeRemaining);
		nbt.setInteger("Temperature", TokamakTemp);
		buffer.writeToNBT(nbt);
		nbt.setFloat("PowerOut", PowerOut);
		nbt.setFloat("PowerIn", PowerIn);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		inventory.readFromNBT(nbt.getCompoundTag("Items"));
		fuelTank.readFromNBT(nbt.getCompoundTag("Tank"));
		BurnTimeRemaining = nbt.getInteger("BurnRemaining");
		TokamakTemp = nbt.getInteger("Temperature");
		buffer.readFromNBT(nbt);
		PowerOut = nbt.getFloat("PowerOut");
		PowerIn = nbt.getFloat("PowerIn");
		
		setTooltipDirty(true);
	}
	
	@Override
	public String getName()
	{
		return "TOKAMAK";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return false;
	}
	
	@Override
	public int getSizeInventory()
	{
		return inventory.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return inventory.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return inventory.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return inventory.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return inventory.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		inventory.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 1;
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return inventory.isUsableByPlayer(player, pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return TokamakFluidRegistry.canAcceptFluidItem(stack);
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		inventory.clear();
	}
	
	@Override
	public IFluidTankProperties[] getTankProperties()
	{
		return fuelTank.getTankProperties();
	}
	
	@Override
	public int fill(FluidStack resource, boolean doFill)
	{
		return resource != null && TokamakFluidRegistry.getFuelRate(resource.getFluid()) > 0 ? fuelTank.fill(resource, doFill) : 0;
	}
	
	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain)
	{
		return null;
	}
	
	@Override
	public FluidStack drain(int maxDrain, boolean doDrain)
	{
		return null;
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return canReceive() ? buffer.receiveEnergy(maxReceive, simulate) : 0;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return buffer.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return buffer.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return TokamakTemp < TokamakFusionTemp;
	}
	
	public float getScaledLiquidQuantity(int MaxLevel)
	{
		return ((float) fuelTank.getFluidAmount() / (float) fuelTank.getCapacity()) * MaxLevel;
	}
	
	public float getScaledTemp(int MaxLevel)
	{
		return ((float) TokamakTemp / (float) TokamakConfig.TokamakMaxTemp) * MaxLevel;
	}
	
	public float getScaledFusionTemp(int MaxLevel)
	{
		return ((float) TokamakFusionTemp / (float) TokamakConfig.TokamakMaxTemp) * MaxLevel;
	}
	
	public float getScaledPower(boolean InputOutput, int MaxLevel)
	{
		if(InputOutput)
			return (PowerOut / TokamakConfig.TokamakGenLimit) * MaxLevel;
		else
			return (PowerIn / TokamakConfig.TokamakMaxEnergyReceive) * MaxLevel;
	}
	
	public boolean isFusing()
	{
		return TokamakTemp > TokamakFusionTemp && fuelTank.getFluidAmount() > 0;
	}
	
	public static int getFEProduced(int heatPercent, Fluid fluid, int amount)
	{
		int time = (int) (Fluid.BUCKET_VOLUME * (TokamakConfig.TokamakWaterUse / 100F));
		int rate = TokamakFluidRegistry.getFuelRate(fluid);
		if(heatPercent < 100 * TokamakConfig.TokamakFusionFraction || amount < time || rate == 0)
			return 0;
		int TokamakTemp = (int) (TokamakConfig.TokamakMaxTemp * (heatPercent / 100F));
		int times = amount / time;
		int TokamakFusionTemp = (int) (TokamakConfig.TokamakMaxTemp * TokamakConfig.TokamakFusionFraction);
		float EnergyGradient = (float) (TokamakTemp - TokamakFusionTemp) / (float) (TokamakConfig.TokamakMaxTemp - TokamakFusionTemp);
		float ex = EnergyGradient * (float) Math.sqrt(TokamakConfig.TokamakGenLimit);
		int extracted = (int) (ex * ex);
		return extracted * rate * times;
	}
	
	private boolean tdirty;
	
	@Override
	public boolean isTooltipDirty()
	{
		return tdirty;
	}
	
	@Override
	public void setTooltipDirty(boolean dirty)
	{
		tdirty = dirty;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ITooltip tip)
	{
		tip.append(new ItemStackTooltipInfo(new ItemStack(getBlockType()), 16, 16));
		tip.append(new TranslationTooltipInfo(getBlockType().getTranslationKey() + ".name"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("gui." + InfoAR.MOD_ID + ".tokamak.5"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", (int) PowerIn)).appendColor(TextFormatting.BLUE));
		tip.append(new TranslationTooltipInfo("gui.hammercore.fept"));
		
		tip.newLine();
		tip.append(new TranslationTooltipInfo("gui." + InfoAR.MOD_ID + ".tokamak.6"));
		tip.append(new StringTooltipInfo(": "));
		tip.append(new StringTooltipInfo(String.format("%,d ", (int) PowerOut)).appendColor(TextFormatting.GOLD));
		tip.append(new TranslationTooltipInfo("gui.hammercore.fept"));
	}
}