package com.zeitheron.aps.blocks.tiles;

import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.client.gui.GuiFuelGenerator;
import com.zeitheron.aps.inventory.ContainerFuelGenerator;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.utils.FluidEnergyAccessPoint;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidUtil;

public class TileFuelGenerator extends TileMachine implements IInventory, IEnergyStorage
{
	public final FEEnergyStorage energy = new FEEnergyStorage(20000);
	public final InventoryDummy items = new InventoryDummy(1);
	public FluidEnergyAccessPoint hotspot;
	public int time = 0, maxTime = 0;
	
	@Override
	public void tick()
	{
		if(hotspot == null)
			hotspot = new FluidEnergyAccessPoint(getWorld(), getPos());
		
		if(atTickRate(10) && !world.isRemote)
			setMachineActive(time > 0);
		
		if(time <= 0 && atTickRate(20))
		{
			ItemStack item = items.getStackInSlot(0);
			if(!item.isEmpty())
			{
				int b = TileEntityFurnace.getItemBurnTime(item);
				if(b > 0 && FluidUtil.getFluidContained(item) == null)
				{
					maxTime = b;
					time += maxTime * 100;
					item.shrink(1);
					sendChangesToNearby();
				}
			}
		}
		
		boolean changed = false;
		
		gen: if(time > 0)
		{
			float factor = Math.min(100, time) / 100F;
			int toEmit = (int) Math.floor(40 * factor);
			if(toEmit == 0)
				break gen;
			int emitted = energy.receiveEnergy(toEmit, false);
			float extractFactor = Math.max(emitted, 1) / (float) toEmit;
			int reduceBT = (int) Math.ceil(extractFactor * 100);
			time = Math.max(0, time - reduceBT);
			changed = true;
		}
		
		if(energy.getEnergyStored() > 0)
			changed |= energy.extractEnergy(hotspot.emitEnergy(energy.getEnergyStored()), false) > 0;
		
		if(changed && atTickRate(60))
			sendChangesToNearby();
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("Items", items.writeToNBT(new NBTTagCompound()));
		energy.writeToNBT(nbt);
		nbt.setInteger("BurnTime", time);
		nbt.setInteger("MaxBurnTime", maxTime);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		items.readFromNBT(nbt.getCompoundTag("Items"));
		energy.readFromNBT(nbt);
		time = nbt.getInteger("BurnTime");
		maxTime = nbt.getInteger("MaxBurnTime");
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiFuelGenerator(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerFuelGenerator(player, this);
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY || super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		return capability == CapabilityEnergy.ENERGY ? (T) this : super.getCapability(capability, facing);
	}
	
	@Override
	public String getName()
	{
		return "AGC-SE SOLID GEN";
	}
	
	@Override
	public boolean hasCustomName()
	{
		return true;
	}
	
	@Override
	public int getSizeInventory()
	{
		return items.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return items.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return items.getStackInSlot(index);
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return items.decrStackSize(index, count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return items.removeStackFromSlot(index);
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		items.setInventorySlotContents(index, stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return items.getInventoryStackLimit();
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return items.isUsableByPlayer(player, getPos());
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return TileEntityFurnace.getItemBurnTime(stack) > 0 && FluidUtil.getFluidContained(stack) == null;
	}
	
	@Override
	public int getField(int id)
	{
		return id == 0 ? time : 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
		if(id == 0)
			time = Math.max(0, value);
	}
	
	@Override
	public int getFieldCount()
	{
		return 1;
	}
	
	@Override
	public void clear()
	{
		items.clear();
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return energy.extractEnergy(maxExtract, simulate);
	}
	
	@Override
	public int getEnergyStored()
	{
		return energy.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return energy.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return true;
	}
	
	@Override
	public boolean canReceive()
	{
		return false;
	}
}