package com.zeitheron.aps.blocks.tiles;

import java.awt.Rectangle;

import com.zeitheron.aps.api.laser_table.LaserTableRecipe;
import com.zeitheron.aps.api.laser_table.RecipesLaserTable;
import com.zeitheron.aps.blocks.BlockLaserTable;
import com.zeitheron.aps.client.gui.GuiLaserTable;
import com.zeitheron.aps.init.BlocksAR;
import com.zeitheron.aps.inventory.ContainerLaserTable;
import com.zeitheron.aps.util.LyingItems;
import com.zeitheron.aps.util.RectangleAnimation2D;
import com.zeitheron.hammercore.internal.capabilities.FEEnergyStorage;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.tileentity.TileEntityItemStackRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileLaserTable extends TileSyncableTickable implements IEnergyStorage, ISidedInventory
{
	public FEEnergyStorage buffer = new FEEnergyStorage(100_000, 20_000, 100_000);
	public final InventoryDummy input = new InventoryDummy(9);
	public final InventoryDummy output = new InventoryDummy(1);
	public final int[] slots = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	public LaserTableRecipe recipe;
	public int recipeEnergy;
	
	public boolean crafting = false;
	public final RectangleAnimation2D laserHeadPos = new RectangleAnimation2D(new Rectangle(4, 4, 8, 8), .1F);
	
	{
		output.validSlots = (i, stack) -> false;
	}
	
	public LyingItems renderable = new LyingItems();
	
	@Override
	public void tick()
	{
		int prevEE = recipeEnergy;
		
		if(atTickRate(4))
		{
			IBlockState state = world.getBlockState(pos);
			if(state.getBlock() == BlocksAR.LASER_TABLE && !state.getValue(BlockLaserTable.LOWER))
				world.removeTileEntity(pos);
		}
		
		if(atTickRate(10) && renderable.mergeTar == null)
		{
			LaserTableRecipe pre = recipe;
			recipe = RecipesLaserTable.search(input, output);
			if(recipe != null && recipe != pre)
			{
				renderable.merging.clear();
				renderable.addItems(input.inventory);
			}
			sync();
		}
		
		if(recipe != null)
		{
			recipeEnergy += buffer.extractEnergy((int) Math.ceil(recipe.FE / 200F), false);
			if(recipeEnergy >= recipe.FE && recipe.craft(input, output))
			{
				recipeEnergy -= recipe.FE;
				recipe = null;
				renderable.mergeInto(output.getStackInSlot(0), 10);
				double step = laserHeadPos.bounds.getWidth() / 5;
				for(int j = 0; j <= 5; ++j)
					for(int k = 0; k <= 5; ++k)
						if(getRNG().nextInt(10) == 0)
							HCNet.spawnParticle(world, EnumParticleTypes.FLAME, pos.getX() + (step * j + laserHeadPos.bounds.x) / 16F, pos.getY() + 1.05, pos.getZ() + (step * k + laserHeadPos.bounds.y) / 16F, 0, 0, 0);
			}
		} else if(RecipesLaserTable.search(input, output) == null)
			renderable.merging.clear();
		
		crafting = prevEE != recipeEnergy;
		
		renderable.tick();
		if(world.isRemote)
		{
			laserHeadPos.step = crafting && recipe != null ? .25F : 0F;
			laserHeadPos.update(getRNG());
			if(atTickRate(5) && crafting)
				HCNet.spawnParticle(world, EnumParticleTypes.SMOKE_NORMAL, pos.getX() + laserHeadPos.x / 16, pos.getY() + 1.05, pos.getZ() + laserHeadPos.y / 16, 0, 0, 0);
		}
	}
	
	@Override
	public void writeNBT(NBTTagCompound nbt)
	{
		nbt.setTag("ItemsIn", input.writeToNBT(new NBTTagCompound()));
		nbt.setTag("ItemsOut", output.writeToNBT(new NBTTagCompound()));
		nbt.setInteger("RecipeEnergy", recipeEnergy);
		buffer.writeToNBT(nbt);
	}
	
	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		input.readFromNBT(nbt.getCompoundTag("ItemsIn"));
		output.readFromNBT(nbt.getCompoundTag("ItemsOut"));
		recipeEnergy = nbt.getInteger("RecipeEnergy");
		buffer.readFromNBT(nbt);
	}
	
	public void drop()
	{
		input.drop(world, pos);
		output.drop(world, pos);
	}
	
	@Override
	public boolean hasGui()
	{
		return true;
	}
	
	@Override
	public Object getClientGuiElement(EntityPlayer player)
	{
		return new GuiLaserTable(player, this);
	}
	
	@Override
	public Object getServerGuiElement(EntityPlayer player)
	{
		return new ContainerLaserTable(player, this);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getRenderBoundingBox()
	{
		return new net.minecraft.util.math.AxisAlignedBB(getPos().add(-1, 0, -1), getPos().add(1, 2, 1));
	}
	
	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return true;
		return super.hasCapability(capability, facing);
	}
	
	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if(capability == CapabilityEnergy.ENERGY)
			return (T) this;
		return super.getCapability(capability, facing);
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate)
	{
		return buffer.receiveEnergy(Math.min(500, maxReceive), simulate);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate)
	{
		return 0;
	}
	
	@Override
	public int getEnergyStored()
	{
		return buffer.getEnergyStored();
	}
	
	@Override
	public int getMaxEnergyStored()
	{
		return buffer.getMaxEnergyStored();
	}
	
	@Override
	public boolean canExtract()
	{
		return false;
	}
	
	@Override
	public boolean canReceive()
	{
		return true;
	}
	
	private IInventory geti(int slot)
	{
		return slot < output.getSizeInventory() ? output : input;
	}
	
	private int geta(int slot)
	{
		return slot < output.getSizeInventory() ? slot : slot - output.getSizeInventory();
	}
	
	@Override
	public int getSizeInventory()
	{
		return input.getSizeInventory() + output.getSizeInventory();
	}
	
	@Override
	public boolean isEmpty()
	{
		return input.isEmpty() && output.isEmpty();
	}
	
	@Override
	public ItemStack getStackInSlot(int index)
	{
		return geti(index).getStackInSlot(geta(index));
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count)
	{
		return geti(index).decrStackSize(geta(index), count);
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index)
	{
		return geti(index).removeStackFromSlot(geta(index));
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		geti(index).setInventorySlotContents(geta(index), stack);
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player)
	{
		return input.isUsableByPlayer(player, pos);
	}
	
	@Override
	public void openInventory(EntityPlayer player)
	{
	}
	
	@Override
	public void closeInventory(EntityPlayer player)
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack)
	{
		return geti(index) == output ? false : true;
	}
	
	@Override
	public int getField(int id)
	{
		return 0;
	}
	
	@Override
	public void setField(int id, int value)
	{
	}
	
	@Override
	public int getFieldCount()
	{
		return 0;
	}
	
	@Override
	public void clear()
	{
		input.clear();
		output.clear();
	}
	
	@Override
	public String getName()
	{
		return getClass().getSimpleName();
	}
	
	@Override
	public ITextComponent getDisplayName()
	{
		return new TextComponentString(getName());
	}
	
	@Override
	public boolean hasCustomName()
	{
		return true;
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side)
	{
		return slots;
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction)
	{
		return geti(index) == input;
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction)
	{
		return geti(index) == output;
	}
}