package com.zeitheron.aps.blocks;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.tiles.TileEnergyPipe;
import com.zeitheron.aps.pipes.IPipe;
import com.zeitheron.aps.pipes.energy.EnergyPipe;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.api.blocks.INoBlockstate;
import com.zeitheron.hammercore.api.inconnect.InConnectAPI;
import com.zeitheron.hammercore.api.mhb.BlockTraceable;
import com.zeitheron.hammercore.api.mhb.ICubeManager;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.base.Cast;
import com.zeitheron.hammercore.utils.math.vec.Cuboid6;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class BlockEnergyPipe
		extends BlockTraceable
		implements ITileBlock<TileEnergyPipe>, IPipe, ITileEntityProvider, ICubeManager, INoBlockstate
{
	public final Function<TileEnergyPipe, EnergyPipe> pipe;
	public final boolean extraction;
	public final int transfer;
	public final String name;

	public BlockEnergyPipe(String name, boolean extractionPipe, int transfer, Function<TileEnergyPipe, EnergyPipe> pipe)
	{
		super(Material.ROCK);
		setTranslationKey("pipe_energy_" + name);
		setHarvestLevel("pickaxe", 0);
		setHardness(0);
		this.name = name;
		this.pipe = pipe;
		this.extraction = extractionPipe;
		this.transfer = transfer;
	}

	@Override
	public IBlockState getExtendedState(IBlockState state, IBlockAccess world, BlockPos pos)
	{
		return InConnectAPI.makeExtendedPositionedState(world, pos, state);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, World world, List<String> tooltip, ITooltipFlag advanced)
	{
		tooltip.add(I18n.format("tooltip." + InfoAR.MOD_ID + ".pipe_type") + ": " + I18n.format("tooltip." + InfoAR.MOD_ID + ".pipe_type_" + (extraction ? "ex" : "tr")));
		tooltip.add(I18n.format("gui.hammercore.fept") + ": " + String.format("%,d", transfer));
	}

	@Override
	public boolean canHarvestBlock(IBlockAccess world, BlockPos pos, EntityPlayer player)
	{
		return true;
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		IPipe pip = WorldUtil.cast(worldIn.getTileEntity(pos), IPipe.class);
		if(pip != null)
			pip.kill();
		super.breakBlock(worldIn, pos, state);
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileEnergyPipe(this);
	}

	@Override
	public Class<TileEnergyPipe> getTileClass()
	{
		return TileEnergyPipe.class;
	}

	@Override
	public boolean isConnectedTo(EnumFacing face)
	{
		return face.getAxis() == Axis.Y;
	}

	@Override
	public ResourceLocation getTex()
	{
		return new ResourceLocation(InfoAR.MOD_ID, "pipes/pipe_energy_" + name);
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullBlock(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}

	@Override
	public void kill()
	{
	}

	@Override
	public BlockFaceShape getBlockFaceShape(IBlockAccess worldIn, IBlockState state, BlockPos pos, EnumFacing face)
	{
		return BlockFaceShape.UNDEFINED;
	}

	@Override
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}

	@Override
	public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
	{
		return true;
	}

	@Override
	public EnumBlockRenderType getRenderType(IBlockState state)
	{
		return EnumBlockRenderType.MODEL;
	}

	@Override
	public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos)
	{
		Cast.optionally(worldIn.getTileEntity(pos), TileEnergyPipe.class).ifPresent(pipe -> pipe.updateConnections());
	}

	private static final ThreadLocal<List<Cuboid6>> cubeLists = ThreadLocal.withInitial(ArrayList::new);

	@Override
	public Cuboid6[] getCuboids(World world, BlockPos pos, IBlockState state)
	{
		IPipe pipe = WorldUtil.cast(world.getTileEntity(pos), IPipe.class);
		if(pipe != null)
		{
			List<Cuboid6> c = cubeLists.get();
			c.clear();
			pipe.addCuboids(c);
			return c.toArray(new Cuboid6[c.size()]);
		}
		return new Cuboid6[0];
	}

	@Override
	public BlockPos coordinates()
	{
		return BlockPos.ORIGIN;
	}

	@Override
	public World world()
	{
		return null;
	}
}