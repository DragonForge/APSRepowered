package com.zeitheron.aps.blocks;

import com.zeitheron.aps.blocks.tiles.TileMagmafier;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockHorizontal;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidUtil;

public class BlockMagmafier extends BlockDeviceHC<TileMagmafier> implements IBlockHorizontal
{
	public BlockMagmafier()
	{
		super(Material.IRON, TileMagmafier.class, "magmafier");
		setSoundType(SoundType.METAL);
		setHardness(3F);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileMagmafier tbf = WorldUtil.cast(worldIn.getTileEntity(pos), TileMagmafier.class);
		if(tbf != null)
		{
			FluidActionResult far = FluidUtil.tryFillContainer(playerIn.getHeldItem(hand), tbf.lavaTank, tbf.lavaTank.getFluidAmount(), playerIn, true);
			if(far.isSuccess())
			{
				if(!playerIn.isCreative())
					playerIn.setHeldItem(hand, far.getResult());
				HCNet.swingArm(playerIn, hand);
				return true;
			}
		}
		
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}
}