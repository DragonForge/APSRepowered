package com.zeitheron.aps.blocks;

import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.blocks.tiles.TileSolarCollector;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockSolarCollector extends Block implements ITileEntityProvider, ITileBlock<TileSolarCollector>
{
	public BlockSolarCollector()
	{
		super(Material.IRON);
		setSoundType(SoundType.METAL);
		setTranslationKey("solar_collector");
		setHardness(3);
	}
	
	@Override
	public Class<TileSolarCollector> getTileClass()
	{
		return TileSolarCollector.class;
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta)
	{
		return new TileSolarCollector();
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileMachine m = WorldUtil.cast(worldIn.getTileEntity(pos), TileMachine.class);
		if(m != null)
			m.destroy();
		super.breakBlock(worldIn, pos, state);
	}
}