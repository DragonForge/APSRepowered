package com.zeitheron.aps.blocks;

import com.zeitheron.aps.blocks.tiles.TileFluidPump;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockHorizontal;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidUtil;

public class BlockFluidPump extends BlockDeviceHC<TileFluidPump> implements IBlockHorizontal
{
	public BlockFluidPump()
	{
		super(Material.IRON, TileFluidPump.class, "fluid_pump");
		setHardness(3F);
		setSoundType(SoundType.METAL);
	}
	
	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}
	
	@Override
	public BlockRenderLayer getRenderLayer()
	{
		return BlockRenderLayer.CUTOUT;
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileFluidPump tbf = WorldUtil.cast(worldIn.getTileEntity(pos), TileFluidPump.class);
		if(tbf != null)
		{
			FluidActionResult far = FluidUtil.tryFillContainer(playerIn.getHeldItem(hand), tbf.internalTank, tbf.internalTank.getFluidAmount(), playerIn, true);
			if(far.isSuccess())
			{
				if(!playerIn.isCreative())
					playerIn.setHeldItem(hand, far.getResult());
				HCNet.swingArm(playerIn, hand);
				return true;
			}
		}
		
		return false;
	}
}