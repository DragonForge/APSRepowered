package com.zeitheron.aps.blocks;

import com.zeitheron.aps.api.TokamakFluidRegistry;
import com.zeitheron.aps.blocks.api.TileMachine;
import com.zeitheron.aps.blocks.tiles.TileMagmafier;
import com.zeitheron.aps.blocks.tiles.TileTokamak;
import com.zeitheron.hammercore.api.ITileBlock;
import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.internal.blocks.base.BlockDeviceHC;
import com.zeitheron.hammercore.internal.blocks.base.IBlockEnableable;
import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.tile.TileSyncable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidActionResult;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;

public class BlockTokamak extends BlockDeviceHC<TileTokamak> implements IBlockEnableable
{
	public BlockTokamak()
	{
		super(Material.IRON, TileTokamak.class, "tokamak");
		setHardness(3F);
		setSoundType(SoundType.METAL);
	}
	
	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
	{
		TileTokamak tbf = WorldUtil.cast(worldIn.getTileEntity(pos), TileTokamak.class);
		if(tbf != null)
		{
			ItemStack held = playerIn.getHeldItem(hand);
			if(TokamakFluidRegistry.canAcceptFluidItem(held))
			{
				FluidActionResult far = FluidUtil.tryEmptyContainer(held, tbf.fuelTank, tbf.fuelTank.getCapacity(), playerIn, true);
				if(far.isSuccess())
				{
					if(!playerIn.isCreative())
						playerIn.setHeldItem(hand, far.getResult());
					HCNet.swingArm(playerIn, hand);
					return true;
				}
			}
		}
		
		GuiManager.openGui(playerIn, WorldUtil.cast(worldIn.getTileEntity(pos), TileSyncable.class));
		return true;
	}
	
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
		TileTokamak tile = WorldUtil.cast(worldIn.getTileEntity(pos), TileTokamak.class);
		if(tile != null)
			tile.inventory.drop(worldIn, pos);
		super.breakBlock(worldIn, pos, state);
	}
	
	@Override
	protected void updateState(World worldIn, BlockPos pos, IBlockState state)
	{
	}
}