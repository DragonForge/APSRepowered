package com.zeitheron.aps.blocks.api;

import com.zeitheron.hammercore.internal.blocks.base.IBlockEnableable;
import com.zeitheron.hammercore.tile.TileSyncableTickable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class TileMachine extends TileSyncableTickable
{
	public static boolean isActive(IBlockState machine)
	{
		if(machine.getPropertyKeys().contains(IBlockEnableable.ENABLED))
			return machine.getValue(IBlockEnableable.ENABLED);
		return false;
	}
	
	public static boolean isActive(World world, BlockPos pos)
	{
		return isActive(world.getBlockState(pos));
	}
	
	public boolean isMachineActive()
	{
		return isActive(world, pos);
	}
	
	public void setMachineActive(boolean active)
	{
		IBlockState state = world.getBlockState(pos);
		if(state.getProperties().containsKey(IBlockEnableable.ENABLED))
		{
			IBlockState nstate = state.withProperty(IBlockEnableable.ENABLED, active);
			if(state.equals(nstate))
				return;
			world.setBlockState(pos, nstate, 1 | 2);
			validate();
			world.setTileEntity(pos, this);
			validate();
			sendChangesToNearby();
		}
	}
	
	public void destroy()
	{
		
	}
}