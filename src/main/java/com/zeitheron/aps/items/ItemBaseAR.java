package com.zeitheron.aps.items;

import com.zeitheron.hammercore.utils.IRegisterListener;

import net.minecraft.item.Item;
import net.minecraftforge.oredict.OreDictionary;

public class ItemBaseAR extends Item implements IRegisterListener
{
	private final String od;
	
	public ItemBaseAR(String name)
	{
		this(name, 64);
	}
	
	public ItemBaseAR(String name, int stackSize)
	{
		this(name, stackSize, null);
	}
	
	public ItemBaseAR(String name, String od)
	{
		this(name, 64, od);
	}
	
	public ItemBaseAR(String name, int stackSize, String od)
	{
		setTranslationKey(name);
		setMaxStackSize(stackSize);
		this.od = od;
	}

	@Override
	public void onRegistered()
	{
		if(od != null)
			OreDictionary.registerOre(od, this);
	}
}