package com.zeitheron.aps.init;

import javax.annotation.Nullable;

import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.blocks.BlockMagmafier;
import com.zeitheron.aps.blocks.BlockMagmaticGenerator;
import com.zeitheron.aps.blocks.BlockPoweredFurnace;
import com.zeitheron.aps.blocks.BlockBurntGlass;
import com.zeitheron.aps.blocks.BlockEnergyPipe;
import com.zeitheron.aps.blocks.BlockFluidPump;
import com.zeitheron.aps.blocks.BlockFuelGenerator;
import com.zeitheron.aps.blocks.BlockGrinder;
import com.zeitheron.aps.blocks.BlockLaserTable;
import com.zeitheron.aps.blocks.BlockRockGlass;
import com.zeitheron.aps.blocks.BlockSolarCollector;
import com.zeitheron.aps.blocks.BlockSolarReflector;
import com.zeitheron.aps.blocks.BlockTokamak;
import com.zeitheron.aps.blocks.heavy_water.BlockHeavyWater;
import com.zeitheron.aps.cfg.MachineConfig;
import com.zeitheron.aps.cfg.SolarConfig;
import com.zeitheron.aps.cfg.TokamakConfig;
import com.zeitheron.aps.pipes.energy.impl.SimpleEnergyPipe;
import com.zeitheron.aps.pipes.energy.impl.SimpleExtractingEnergyPipe;

import com.zeitheron.hammercore.annotations.RegisterIf;
import net.minecraft.util.ResourceLocation;

public class BlocksAR
{
	@Nullable
	public static final BlockTokamak TOKAMAK;
	@Nullable
	public static final BlockFluidPump FLUID_PUMP;
	@Nullable
	public static final BlockSolarReflector SOLAR_REFLECTOR;
	@Nullable
	public static final BlockSolarCollector SOLAR_COLLECTOR;
	@Nullable
	public static final BlockGrinder GRINDER;
	@Nullable
	public static final BlockMagmafier MAGMAFIER;
	@Nullable
	public static final BlockPoweredFurnace POWERED_FURNACE;

	public static final BlockLaserTable LASER_TABLE = new BlockLaserTable();
	public static final BlockHeavyWater HEAVY_WATER = new BlockHeavyWater();
	public static final BlockMagmaticGenerator MAGMATIC_GENERATOR = new BlockMagmaticGenerator();
	public static final BlockFuelGenerator FUEL_GENERATOR = new BlockFuelGenerator();
	public static final BlockRockGlass ROCK_GLASS = new BlockRockGlass();
	public static final BlockBurntGlass BURNT_GLASS = new BlockBurntGlass();
	
	public static final BlockEnergyPipe PIPE_ENERGY_WOOD = new BlockEnergyPipe("wood", true, 400, tep -> new SimpleExtractingEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_wood.png"), 400));
	public static final BlockEnergyPipe PIPE_ENERGY_TIN = new BlockEnergyPipe("tin", false, 400, tep -> new SimpleEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_tin.png"), 400));
	
	public static final BlockEnergyPipe PIPE_ENERGY_LAPIS = new BlockEnergyPipe("lapis", true, 4_000, tep -> new SimpleExtractingEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_lapis.png"), 4_000));
	public static final BlockEnergyPipe PIPE_ENERGY_IRON = new BlockEnergyPipe("iron", false, 4_000, tep -> new SimpleEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_iron.png"), 4_000));
	public static final BlockEnergyPipe PIPE_ENERGY_SILVER = new BlockEnergyPipe("silver", false, 10_000, tep -> new SimpleEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_silver.png"), 10_000));
	public static final BlockEnergyPipe PIPE_ENERGY_GOLD = new BlockEnergyPipe("gold", false, 15_000, tep -> new SimpleEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_gold.png"), 15_000));
	
	public static final BlockEnergyPipe PIPE_ENERGY_PLATINIRIDIUM = new BlockEnergyPipe("platiniridium", true, 150_000, tep -> new SimpleExtractingEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_platiniridium.png"), 150_000));
	public static final BlockEnergyPipe PIPE_ENERGY_IRIDIUM = new BlockEnergyPipe("iridium", false, 100_000, tep -> new SimpleEnergyPipe(tep, new ResourceLocation(InfoAR.MOD_ID, "textures/pipe/pipe_energy_iridium.png"), 100_000));
	
	static
	{
		TOKAMAK = TokamakConfig.TokamakEnabled ? new BlockTokamak() : null;
		SOLAR_REFLECTOR = SolarConfig.SolarEnabled ? new BlockSolarReflector() : null;
		SOLAR_COLLECTOR = SolarConfig.SolarEnabled ? new BlockSolarCollector() : null;
		POWERED_FURNACE = MachineConfig.FurnaceEnabled ? new BlockPoweredFurnace() : null;
		GRINDER = MachineConfig.GrinderEnabled ? new BlockGrinder() : null;
		FLUID_PUMP = MachineConfig.PumpEnabled ? new BlockFluidPump() : null;
		MAGMAFIER = MachineConfig.MagmafierEnabled ? new BlockMagmafier() : null;
	}
}