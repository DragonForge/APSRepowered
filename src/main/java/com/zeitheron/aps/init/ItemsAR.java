package com.zeitheron.aps.init;

import com.zeitheron.aps.items.ItemBaseAR;

public class ItemsAR
{
	public static final ItemBaseAR //
	TOKAMAK_PLATING = new ItemBaseAR("tokamak_plating"), //
	        TOKAMAK_CHAMBER = new ItemBaseAR("tokamak_chamber"), //
	        CONTROL_MODULE = new ItemBaseAR("control_module"), //
	        DEUTERIUM_EXTRACTOR = new ItemBaseAR("deuterium_extractor"), //
	        INPUT_MODULE = new ItemBaseAR("input_module"), //
	        OUTPUT_MODULE = new ItemBaseAR("output_module"), //
	        DIAMOND_GEAR = new ItemBaseAR("diamond_gear", "gearDiamond"), //
	        EMERALD_GEAR = new ItemBaseAR("emerald_gear", "gearEmerald"), //
	        RIBBON_CABLE = new ItemBaseAR("ribbon_cable", "cableRibbon"), //
	        REDSTONE_CHIP = new ItemBaseAR("redstone_chip"), //
	        IRON_CHIP = new ItemBaseAR("iron_chip"), //
	        GOLD_CHIP = new ItemBaseAR("gold_chip"), //
	        DIAMOND_CHIP = new ItemBaseAR("diamond_chip"), //
	        EMERALD_CHIP = new ItemBaseAR("emerald_chip"), //
	        REDSTONE_POWER_CORE = new ItemBaseAR("redstone_power_core"), //
	        IRON_POWER_CORE = new ItemBaseAR("iron_power_core"), //
	        GOLD_POWER_CORE = new ItemBaseAR("gold_power_core"), //
	        DIAMOND_POWER_CORE = new ItemBaseAR("diamond_power_core"), //
	        EMERALD_POWER_CORE = new ItemBaseAR("emerald_power_core");
}