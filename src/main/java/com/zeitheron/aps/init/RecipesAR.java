package com.zeitheron.aps.init;

import com.google.common.base.Predicates;
import com.zeitheron.aps.InfoAR;
import com.zeitheron.aps.api.laser_table.ILaserTableRegisterHook;
import com.zeitheron.aps.api.laser_table.RecipesLaserTable;
import com.zeitheron.hammercore.utils.ConsumableItem;
import com.zeitheron.hammercore.utils.OnetimeCaller;
import com.zeitheron.hammercore.utils.base.Cast;
import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@RegisterRecipes(modid = InfoAR.MOD_ID)
public class RecipesAR
		extends RecipeRegistry
{
	public static final List<RecipeRegistry> subs = new ArrayList<>();

	public static RecipesAR INSTANCE;

	{
		INSTANCE = this;
	}

	public final OnetimeCaller laserTable = new OnetimeCaller(this::$laserTable);

	public void $laserTable()
	{
		RecipesLaserTable.add(ItemsAR.REDSTONE_CHIP, 3000, ConsumableItem.of(8, Items.REDSTONE), ConsumableItem.of(2, ItemsAR.RIBBON_CABLE));
		RecipesLaserTable.add(ItemsAR.IRON_CHIP, 4000, ConsumableItem.of(1, Items.IRON_INGOT), ConsumableItem.of(1, ItemsAR.REDSTONE_CHIP));
		RecipesLaserTable.add(ItemsAR.GOLD_CHIP, 5000, ConsumableItem.of(1, Items.GOLD_INGOT), ConsumableItem.of(1, ItemsAR.IRON_CHIP));
		RecipesLaserTable.add(ItemsAR.DIAMOND_CHIP, 6000, ConsumableItem.of(1, Items.DIAMOND), ConsumableItem.of(1, ItemsAR.GOLD_CHIP));
		RecipesLaserTable.add(ItemsAR.EMERALD_CHIP, 7000, ConsumableItem.of(1, Items.EMERALD), ConsumableItem.of(1, ItemsAR.DIAMOND_CHIP));

		subs.stream().map(c -> Cast.cast(c, ILaserTableRegisterHook.class)).filter(Predicates.notNull()).forEach(ILaserTableRegisterHook::registerLaserTable);
	}

	@Override
	public void crafting()
	{
		shaped(ItemsAR.DIAMOND_GEAR, " d ", "dmd", " d ", 'd', "gemDiamond", 'm', "ingotIron");
		shaped(ItemsAR.DIAMOND_GEAR, " d ", "dmd", " d ", 'd', "gemDiamond", 'm', "ingotCopper");
		shaped(ItemsAR.EMERALD_GEAR, " d ", "dmd", " d ", 'd', "gemEmerald", 'm', "ingotIron");
		shaped(ItemsAR.EMERALD_GEAR, " d ", "dmd", " d ", 'd', "gemEmerald", 'm', "ingotCopper");
		shaped(ItemsAR.TOKAMAK_PLATING, "dio", "dio", "dio", 'd', "gemDiamond", 'i', "plateIron", 'o', "obsidian");
		shaped(ItemsAR.TOKAMAK_CHAMBER, "ipi", "pcp", "ipi", 'i', "ingotIron", 'p', ItemsAR.TOKAMAK_PLATING, 'c', "casingChrome");
		shaped(ItemsAR.CONTROL_MODULE, "iri", "rcr", "iri", 'i', "ingotIron", 'r', Items.REPEATER, 'c', ItemsAR.DIAMOND_POWER_CORE);
		shaped(ItemsAR.REDSTONE_POWER_CORE, "iri", "gRg", "iri", 'i', "ingotIron", 'g', "gearTin", 'r', "dustRedstone", 'R', ItemsAR.REDSTONE_CHIP);
		shaped(ItemsAR.IRON_POWER_CORE, "iri", "gRg", "iri", 'i', "ingotIron", 'g', "gearIron", 'r', "dustRedstone", 'R', ItemsAR.IRON_CHIP);
		shaped(ItemsAR.GOLD_POWER_CORE, "iri", "gRg", "iri", 'i', "ingotIron", 'g', "gearGold", 'r', "dustRedstone", 'R', ItemsAR.GOLD_CHIP);
		shaped(ItemsAR.DIAMOND_POWER_CORE, "iri", "gRg", "iri", 'i', "ingotIron", 'g', "gearDiamond", 'r', "dustRedstone", 'R', ItemsAR.DIAMOND_CHIP);
		shaped(ItemsAR.EMERALD_POWER_CORE, "iri", "gRg", "iri", 'i', "ingotIron", 'g', "gearEmerald", 'r', "dustRedstone", 'R', ItemsAR.EMERALD_CHIP);
		shaped(ItemsAR.DEUTERIUM_EXTRACTOR, "ClC", "ere", "clc", 'C', ItemsAR.GOLD_POWER_CORE, 'l', "plateIridium", 'e', "gearTitanium", 'r', "casingTungsten", 'c', BlocksAR.PIPE_ENERGY_GOLD);
		shaped(ItemsAR.INPUT_MODULE, "DcD", "Rcl", "Rcl", 'D', "gemDiamond", 'c', BlocksAR.PIPE_ENERGY_GOLD, 'R', "dustRedstone", 'l', "plateIridium");
		shaped(ItemsAR.OUTPUT_MODULE, "Rcd", "Rcd", "DwD", 'R', "dustRedstone", 'c', BlocksAR.PIPE_ENERGY_GOLD, 'd', "gearDiamond", 'w', BlocksAR.PIPE_ENERGY_WOOD, 'D', "gemDiamond");
		shapeless(new ItemStack(ItemsAR.RIBBON_CABLE, 4), "stickCopper", "blockWool");

		if(BlocksAR.TOKAMAK != null)
			shaped(BlocksAR.TOKAMAK, "oOo", "CTD", "oIo", 'o', "obsidian", 'O', ItemsAR.OUTPUT_MODULE, 'C', ItemsAR.CONTROL_MODULE, 'T', ItemsAR.TOKAMAK_CHAMBER, 'D', ItemsAR.DEUTERIUM_EXTRACTOR, 'I', ItemsAR.INPUT_MODULE);
		if(BlocksAR.GRINDER != null)
			shaped(BlocksAR.GRINDER, "ggg", "pcp", "pip", 'c', "casingAluminum", 'g', "gearTitanium", 'i', ItemsAR.GOLD_POWER_CORE, 'p', "plateAluminum");
		if(BlocksAR.FLUID_PUMP != null)
			shaped(BlocksAR.FLUID_PUMP, "pop", "pcp", "bib", 'c', "casingAluminum", 'i', ItemsAR.IRON_POWER_CORE, 'b', Items.BUCKET, 'p', "plateAluminum", 'o', ItemsAR.OUTPUT_MODULE);
		if(BlocksAR.SOLAR_COLLECTOR != null)
			shaped(BlocksAR.SOLAR_COLLECTOR, "ggg", "gcg", "pop", 'g', "blockGlassHardened", 'c', ItemsAR.CONTROL_MODULE, 'p', "plateAluminum", 'o', ItemsAR.EMERALD_POWER_CORE);
		if(BlocksAR.SOLAR_REFLECTOR != null)
			shaped(BlocksAR.SOLAR_REFLECTOR, "ggg", "rcr", "ppp", 'g', "blockGlassHardened", 'r', "stickAluminum", 'p', "plateAluminum", 'c', "dustCoal");
		if(BlocksAR.MAGMAFIER != null)
			shaped(BlocksAR.MAGMAFIER, "ogo", "dcd", "opo", 'o', "obsidian", 'c', "casingIron", 'p', ItemsAR.GOLD_POWER_CORE, 'd', ItemsAR.DIAMOND_POWER_CORE, 'g', "gearAluminum");
		if(BlocksAR.POWERED_FURNACE != null)
			shaped(BlocksAR.POWERED_FURNACE, "aia", "fsf", "afa", 's', "casingSteel", 'f', Blocks.FURNACE, 'i', "gearInvar", 'a', "plateAluminum");
		shaped(BlocksAR.FUEL_GENERATOR, "aca", "cic", "afa", 'i', "casingIron", 'f', Blocks.FURNACE, 'c', "coilCopper", 'a', "plateAluminum");
		shaped(BlocksAR.LASER_TABLE, "ppp", "aca", "aia", 'i', "coilChrome", 'a', "plateAluminum", 'c', "casingTitanium", 'p', "plateTitanium");
		shaped(BlocksAR.MAGMATIC_GENERATOR, "pep", "ete", "pbp", 'p', "plateInvar", 'e', "coilElectrum", 't', "casingInvar", 'b', Items.BUCKET);

		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_WOOD, 2), "s s", "grg", "s s", 's', "stickWood", 'g', "blockGlass", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_TIN, 4), "s s", "grg", "s s", 's', "ingotTin", 'g', "blockGlass", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_LAPIS, 8), "s s", "grg", "s s", 's', "gemLapis", 'g', "blockGlass", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_IRON, 8), "s s", "grg", "s s", 's', "ingotIron", 'g', "blockGlass", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_SILVER, 8), "s s", "grg", "s s", 's', "ingotSilver", 'g', "blockGlass", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_GOLD, 8), "s s", "grg", "s s", 's', "ingotGold", 'g', "blockGlass", 'r', "dustRedstone");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_PLATINIRIDIUM, 16), "s s", "grg", "s s", 's', "ingotPlatiniridium", 'g', "blockGlassHardened", 'r', "gemLapis");
		shaped(new ItemStack(BlocksAR.PIPE_ENERGY_IRIDIUM, 16), "s s", "grg", "s s", 's', "ingotIridium", 'g', "blockGlassHardened", 'r', "gemLapis");

		shaped(new ItemStack(BlocksAR.ROCK_GLASS, 4), "gs", "sg", 'g', "blockGlass", 's', "stone");
		shaped(new ItemStack(BlocksAR.ROCK_GLASS, 4), "sg", "gs", 'g', "blockGlass", 's', "stone");
	}

	@Override
	public Collection<IRecipe> collect()
	{
		HashSet<IRecipe> recipes = (HashSet<IRecipe>) super.collect();
		subs.stream().map(RecipeRegistry::collect).forEach(recipes::addAll);
		return recipes;
	}

	protected Block block(String regName)
	{
		return GameRegistry.findRegistry(Block.class).getValue(new ResourceLocation(regName));
	}

	protected Item item(String regName)
	{
		return GameRegistry.findRegistry(Item.class).getValue(new ResourceLocation(regName));
	}

	protected Block block(String mod, String regName)
	{
		return GameRegistry.findRegistry(Block.class).getValue(new ResourceLocation(mod, regName));
	}

	protected Item item(String mod, String regName)
	{
		return GameRegistry.findRegistry(Item.class).getValue(new ResourceLocation(mod, regName));
	}

	@Override
	public void smelting()
	{

	}

	@Override
	protected String getMod()
	{
		return InfoAR.MOD_ID;
	}

	@Override
	protected IRecipe recipe(IRecipe recipe)
	{
		if(recipe.getRegistryName() == null)
			recipe = recipe.setRegistryName(new ResourceLocation("hammercore", "recipes." + getMod() + ":ar." + recipes.size()));
		recipes.add(recipe);
		return recipe;
	}
}