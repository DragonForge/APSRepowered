package com.zeitheron.aps.init;

import com.zeitheron.aps.blocks.heavy_water.FluidHeavyWater;

import net.minecraftforge.fluids.FluidRegistry;

public class FluidsAR
{
	public static final FluidHeavyWater HEAVY_WATER = new FluidHeavyWater();
	
	public static void init()
	{
		FluidRegistry.registerFluid(HEAVY_WATER);
		FluidRegistry.addBucketForFluid(HEAVY_WATER);
	}
}